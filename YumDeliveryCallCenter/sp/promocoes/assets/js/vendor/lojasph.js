﻿// Inicializa o mapa
var brasil = new google.maps.LatLng(-14.2392976,-53.1805017);
var mapOptions = {
	zoom: 2,
	center: brasil,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	mapTypeControl: false
};

var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
var addressField = document.getElementById('search_address');
var infowindow = new google.maps.InfoWindow(); 
var geocoder = new google.maps.Geocoder();
var marker, i, getCidade, getEstado, getNovaCidade, novaCidade;
var listaLoja = []; 

getNovaCidade = 1;
novaCidade = "";

//Limpar Lojas
function limparMapa() {
	for (var i = 0; i < listaLoja.length; i++ ) {
		listaLoja[i].setMap(null);
	}
	
	listaLoja.length = 0;
	$(".store-list").html(null);
}

//Adiciona novas lojas
function addLojas(buscaUF, buscaCidade){
	$.ajax({
		type: "GET",
		url: "assets/csv/lojasph.asp?chUF="+buscaUF+"&chCidade="+buscaCidade,
		dataType: "xml",
		success: function(xml) {
			for (var i = 0; i < listaLoja.length; i++ ) {
				listaLoja[i].setMap(null);
			}
			
			listaLoja.length = 0;
			$(".store-list").html(null);
			
			var bounds = new google.maps.LatLngBounds();
			
			console.log(xml);
			
			$(xml).find('loja').each(function(){
				var id = $(this).find('IdFranqueado').text();
				var nome = $(this).find('NomeFantasia').text();
				var endereco = $(this).find('Endereco').text();
				var cidade = $(this).find('Cidade').text();
				var uf = $(this).find('UF').text();
				var telefone = $(this).find('Telefone').text();
				var horarioAtendimento = $(this).find('HORARIOATENDIMENTO').text();
				var cafe = $(this).find('Cafe').text();
				var	express = $(this).find('Express').text();
				var	delivery = $(this).find('Delivery').text();
				var	restaurante = $(this).find('Restaurante').text();
				var	balcaoviagem = $(this).find('BalcaoViagem').text();
				var	estacionamento = $(this).find('Estacionamento').text();
				var cadeirante = $(this).find('Cadeirante').text();
						
				var category = [];
				var classeMarker = "";
					
					if(cafe){category[0]="Cafe"; classeMarker = classeMarker + " Cafe"}else{category[0]="";}
					if(express){category[1]="Express"; classeMarker = classeMarker + " Express"}else{category[1]="";}
					if(delivery){category[2]="Delivery"; classeMarker = classeMarker + " Delivery"}else{category[2]="";}
					if(restaurante){category[3]="Restaurante"; classeMarker = classeMarker + " Restaurante"}else{category[3]="";}
					if(balcaoviagem){category[4]="BalcaoViagem"; classeMarker = classeMarker + " BalcaoViagem"}else{category[4]="";}
					if(estacionamento){category[5]="Estacionamento"; classeMarker = classeMarker + " Estacionamento"}else{category[5]="";}
					if(cadeirante){category[6]="Cadeirante"; classeMarker = classeMarker + " Cadeirante"}else{category[6]="";}
				
				var estacionamento = $(this).find('Estacionamento').text();
				var cadeirante = $(this).find('Cadeirante').text();
				var longitude = $(this).find('Longitude').text();
				var latitude = $(this).find('Latitude').text();
	
				var pos = new google.maps.LatLng(latitude, longitude);
				
				bounds.extend(pos);
				
				marker = new google.maps.Marker({
					title:	nome,
					position: pos,
					category: category,
					icon: "/assets/img/marker.png",
					city: cidade,
					state: estado,
					map: map
				});
				
				var detalhesLoja;
				
				if(nome){detalhesLoja = '<div class="loc-name">'+nome+'</div>';}
				if(endereco){detalhesLoja = detalhesLoja + '<div class="loc-addr">'+endereco+'</div>';}
				if(horarioAtendimento){detalhesLoja = detalhesLoja + '<div class="time-addr"><span>'+horarioAtendimento+'</span></div>';}
				if(telefone){detalhesLoja = detalhesLoja + '<div class="tel-addr"><span>'+telefone+'</span></div>';}		
/* 				if(atendimento){detalhesLoja = detalhesLoja + '<div class="atendimento-addr"><ul>'+listaAtendimento+'</ul></div>';} */
				if(cadeirante){detalhesLoja = detalhesLoja + '<div class="cadeirante-addr"><span>Serviço para cadeirante</span></div>';}
				if(estacionamento){detalhesLoja = detalhesLoja + '<div class="estacionamento-addr"><span>Estacionamento</span></div>';}
				
			    (function(marker){
			        google.maps.event.addDomListener($('<li class="store all'+classeMarker+'"/>').html(detalhesLoja).appendTo('.store-list')[0],'click',function(){
			            google.maps.event.trigger(marker,'click',{});
			        });
			    })(marker);
			    
			    listaLoja.push(marker);
			    
				google.maps.event.addListener(marker, 'click', (function(marker, i) {
					return function() {
						infowindow.setContent(marker.title);
						infowindow.open(map, marker);
					}
				})(marker, i));
				
				for(i=0;i<listaLoja.length;i++) {
				   bounds.extend(listaLoja[i].getPosition());
				}
				
				map.fitBounds(bounds);			
			});
		}
	});
}

//Select de estados
$("#estado").change(function(){
	var estadoSelect = $(this).val();
	$('#cidade option').not(":first").remove();

	if(estadoSelect === ""){
		$("#cidade").attr("disabled","").html('<option value="" selected="selected">Selecione uma Cidade</option>');

		limparMapa();
		addLojas(estadoSelect,"");
	}else{
		$("#cidade").attr('style','background: url("assets/img/ajax-loader.gif") #fff no-repeat center center;');

		$.ajax({
			type: "GET",
			url: "assets/csv/lojasph.asp?chUF="+estadoSelect+"&chCidade=",
			dataType: "xml",
			success: function(xml) {
				var cidadeSelect = "";
				var options = [];

				$(xml).find('loja').each(function(){
					var cidadeSelect = $(this).find("Cidade").text();

					if (cidadeSelect) {
						if (options.indexOf(cidadeSelect) == -1) {
							options.push(cidadeSelect);
							options.sort();
							cidadeSelect = '';
						}
					}
				});
				
				var addOptions = function () {
					var r = $.Deferred();
					
					for(var item in options){
					  $('<option value="'+options[item]+'">'+options[item]+'</option>').appendTo('#cidade');
					}
	
					r.resolve();
					
					return r;
				};
	
				var removeAttrs = function () {
					$("#cidade").removeAttr("disabled");
					$("#cidade").removeAttr("style");
					
					if(getNovaCidade == 1){
						$("#cidade").val(novaCidade).prop("selected", true).change();
					}
					
					limparMapa();
					addLojas(estadoSelect,"");
				};
				
				addOptions().done(removeAttrs);
			}
		});
	}
});


//Select de cidades
$("#cidade").change(function(){
	var cidadeSelect = $(this).val();
	var estadoSelect = $("#estado").val();

	limparMapa();
	addLojas(estadoSelect,cidadeSelect);
});
					

//Filtro de lojas
$(".feature-filter").find("input:radio").on('change', function(){
	var categoriaLoja = $(this).val();
	
	for (i = 0; i < listaLoja.length; i++) {
		for (j = 0; j < 7; j++){
			if((listaLoja[i].category[j] === categoriaLoja) || (categoriaLoja === "all")){
				listaLoja[i].setVisible(true);
				
				$(".store").css("display","none");
				$("."+categoriaLoja).css("display","block");
				
				break;
			}else{
				listaLoja[i].setVisible(false);
			}
		}
	}
});

//ADD class marcação
$( ".feature-filter label" ).click(function() {
    $('.feature-filter label').removeAttr("style");
    $(this).attr("style","color: #fff !important; background: #868686 !important;");
});

/* Busca customizada */					    
function search() {
	if(addressField.value){
	
		$("#estado").val("").prop("selected", true).change();
	
	    geocoder.geocode(
	        {'address': addressField.value + ' - Brasil'}, 
	        function(results, status) { 
	            if (status == google.maps.GeocoderStatus.OK) { 

	            	var region, city, country;

	            	$('.error-msg').remove();

					for (var i=0; i<results[0].address_components.length; i++){
						if (results[0].address_components[i].types[0] == "administrative_area_level_1") {
							region = results[0].address_components[i];

							if ($("#estado option[value='"+region.short_name+"']").length == 0 ){
								$('.error-msg').remove();
								$('<div class="error-msg"><i class="fa fa-times-circle"></i> Desculpa, ainda não temos nenhuma Franquia da Pizza Hut no seu Estado.</div>').appendTo('.store-alert');
							}
						}
						if (results[0].address_components[i].types[0] == "locality") {
							city = results[0].address_components[i];
						}
						if (results[0].address_components[i].types[0] == "country") {
							country = results[0].address_components[i];
						}
					}

					for (i = 0; i < listaLoja.length; i++) {
					    if((listaLoja[i].state == region.short_name) || (listaLoja[i].city == city.short_name)){
					    	limparMapa();
					    	addLojas(region.short_name,city.short_name);
					    	
						    map.setCenter(results[0].geometry.location);
						    map.fitBounds(results[0].geometry.viewport);
					    	
					    	trocaMapa();
					    	
					    	break;
					    }
					}
	            } 
	            else { 
	            	$('.error-msg').remove();
	            	$('<div class="error-msg"><i class="fa fa-times-circle"></i> Endereço incorreto.</div>').appendTo('.store-alert');
	            } 
	        }
	    );
	};
};

//Troca mapa de estados pelo google maps
function trocaMapa(){
	$('#map-brasil').css("display","none");
	$('.busca-lojas').css("height","auto");
}

/* Inicializa o mapa */
var className;

$(function($){
  $('#map-brasil').cssMap({
    'size': 660,
    'tooltips' : "visible", 
    'loadingText' : 'Carregando ...',
    'onClick': function(e){
      className = e.attr('class').split(' ')[0];
	  	switch(className) {
		    case 'br4':
		        className = 'Amazonas';
		        break;
		    case 'br6':
		        className = 'Ceará';
		        break;
		    case 'br7':
		        className = 'Distrito Federal';
		        break;
		    case 'br9':
		        className = 'Goiás';
		        break;
		    case 'br10':
		        className = 'Maranhão';
		        break;
		    case 'br11':
		        className = 'Mato Grosso';
		        break;
            case 'br12':
		        className = 'Mato Grosso do Sul';
		        break;
            case 'br13':
		        className = 'Minas Gerais';
		        break;
		    case 'br14':
		        className = 'Pará';
		        break;
		    case 'br15':
		        className = 'Paraíba';
		        break;
		    case 'br4':
		        className = 'Amazonas';
		        break;
		    case 'br16':
		        className = 'Paraná';
		        break;
		    case 'br18':
		        className = 'Piauí';
		        break;
		    case 'br19':
		        className = 'Rio de Janeiro';
		        break;
		    case 'br21':
		        className = 'Rio Grande do Sul';
		        break;
		    case 'br24':
		        className = 'Santa Catarina';
		        break;
		    case 'br25':
		        className = 'São Paulo';
		        break;
		    default:
		        className = 'Brasil';
		}
		
		limparMapa();
		$("#estado").find('option:contains("'+className+'")').prop("selected", true).change();
		trocaMapa();
		
		var tabValue = $(this).attr('href');
		var withoutHash = tabValue.substr(0,tabValue.indexOf('#'));
     }
   });
});

//Inicializa a busca de lojas
//eRest = Estado
//cRest = Cidade
//fRest = Franquia
function restaurantes(eRest,cRest){
	//$("#estado").val(eRest).prop("selected", true).change();
	limparMapa();
	addLojas(eRest, cRest);
}