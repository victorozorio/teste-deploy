﻿//Scripts js

//Custom Select input style, base on Codrops tutorial
;( function( window ) {

	'use strict';

	/**
	 * based on from https://github.com/inuyaksa/jquery.nicescroll/blob/master/jquery.nicescroll.js
	 */
	function hasParent( e, p ) {
		if (!e) return false;
		var el = e.target||e.srcElement||e||false;
		while (el && el != p) {
			el = el.parentNode||false;
		}
		return (el!==false);
	};

	/**
	 * extend obj function
	 */
	function extend( a, b ) {
		for( var key in b ) {
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	/**
	 * SelectFx function
	 */
	function SelectFx( el, options ) {
		this.el = el;
		this.options = extend( {}, this.options );
		extend( this.options, options );
		this._init();
	}

	/**
	 * SelectFx options
	 */
	SelectFx.prototype.options = {
		// if true all the links will open in a new tab.
		// if we want to be redirected when we click an option, we need to define a data-link attr on the option of the native select element
		newTab : true,
		// when opening the select element, the default placeholder (if any) is shown
		stickyPlaceholder : true,
		// callback when changing the value
		onChange : function( val ) { return false; }
	}

	/**
	 * init function
	 * initialize and cache some vars
	 */
	SelectFx.prototype._init = function() {
		// check if we are using a placeholder for the native select box
		// we assume the placeholder is disabled and selected by default
		var selectedOpt = this.el.querySelector( 'option[selected]' );
		this.hasDefaultPlaceholder = selectedOpt && selectedOpt.disabled;

		// get selected option (either the first option with attr selected or just the first option)
		this.selectedOpt = selectedOpt || this.el.querySelector( 'option' );

		// create structure
		this._createSelectEl();

		// all options
		this.selOpts = [].slice.call( this.selEl.querySelectorAll( 'li[data-option]' ) );

		// total options
		this.selOptsCount = this.selOpts.length;

		// current index
		this.current = this.selOpts.indexOf( this.selEl.querySelector( 'li.cs-selected' ) ) || -1;

		// placeholder elem
		this.selPlaceholder = this.selEl.querySelector( 'span.cs-placeholder' );

		// init events
		this._initEvents();
	}

	/**
	 * creates the structure for the select element
	 */
	SelectFx.prototype._createSelectEl = function() {
		var self = this, options = '', createOptionHTML = function(el) {
			var optclass = '', classes = '', link = '';

			if( el.selectedOpt && !this.foundSelected && !this.hasDefaultPlaceholder ) {
				classes += 'cs-selected ';
				this.foundSelected = true;
			}
			// extra classes
			if( el.getAttribute( 'data-class' ) ) {
				classes += el.getAttribute( 'data-class' );
			}
			// link options
			if( el.getAttribute( 'data-link' ) ) {
				link = 'data-link=' + el.getAttribute( 'data-link' );
			}

			if( classes !== '' ) {
				optclass = 'class="' + classes + '" ';
			}

			return '<li ' + optclass + link + ' data-option data-value="' + el.value + '"><span>' + el.textContent + '</span></li>';
		};

		[].slice.call( this.el.children ).forEach( function(el) {
			if( el.disabled ) { return; }

			var tag = el.tagName.toLowerCase();

			if( tag === 'option' ) {
				options += createOptionHTML(el);
			}
			else if( tag === 'optgroup' ) {
				options += '<li class="cs-optgroup"><span>' + el.label + '</span><ul>';
				[].slice.call( el.children ).forEach( function(opt) {
					options += createOptionHTML(opt);
				} );
				options += '</ul></li>';
			}
		} );

		var opts_el = '<div class="cs-options"><ul>' + options + '</ul></div>';
		this.selEl = document.createElement( 'div' );
		this.selEl.className = this.el.className;
		this.selEl.tabIndex = this.el.tabIndex;
		this.selEl.innerHTML = '<span class="cs-placeholder">' + this.selectedOpt.textContent + '</span>' + opts_el;
		this.el.parentNode.appendChild( this.selEl );
		this.selEl.appendChild( this.el );
	}

	/**
	 * initialize the events
	 */
	SelectFx.prototype._initEvents = function() {
		var self = this;

		// open/close select
		this.selPlaceholder.addEventListener( 'click', function() {
			self._toggleSelect();
		} );

		// clicking the options
		this.selOpts.forEach( function(opt, idx) {
			opt.addEventListener( 'click', function() {
				self.current = idx;
				self._changeOption();
				// close select elem
				self._toggleSelect();
			} );
		} );

		// close the select element if the target it´s not the select element or one of its descendants..
		document.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( self._isOpen() && target !== self.selEl && !hasParent( target, self.selEl ) ) {
				self._toggleSelect();
			}
		} );

		// keyboard navigation events
		this.selEl.addEventListener( 'keydown', function( ev ) {
			var keyCode = ev.keyCode || ev.which;

			switch (keyCode) {
				// up key
				case 38:
					ev.preventDefault();
					self._navigateOpts('prev');
					break;
				// down key
				case 40:
					ev.preventDefault();
					self._navigateOpts('next');
					break;
				// space key
				case 32:
					ev.preventDefault();
					if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
						self._changeOption();
					}
					self._toggleSelect();
					break;
				// enter key
				case 13:
					ev.preventDefault();
					if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
						self._changeOption();
						self._toggleSelect();
					}
					break;
				// esc key
				case 27:
					ev.preventDefault();
					if( self._isOpen() ) {
						self._toggleSelect();
					}
					break;
			}
		} );
	}

	/**
	 * navigate with up/dpwn keys
	 */
	SelectFx.prototype._navigateOpts = function(dir) {
		if( !this._isOpen() ) {
			this._toggleSelect();
		}

		var tmpcurrent = typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ? this.preSelCurrent : this.current;

		if( dir === 'prev' && tmpcurrent > 0 || dir === 'next' && tmpcurrent < this.selOptsCount - 1 ) {
			// save pre selected current - if we click on option, or press enter, or press space this is going to be the index of the current option
			this.preSelCurrent = dir === 'next' ? tmpcurrent + 1 : tmpcurrent - 1;
			// remove focus class if any..
			this._removeFocus();
			// add class focus - track which option we are navigating
			classie.add( this.selOpts[this.preSelCurrent], 'cs-focus' );
		}
	}

	/**
	 * open/close select
	 * when opened show the default placeholder if any
	 */
	SelectFx.prototype._toggleSelect = function() {
		// remove focus class if any..
		this._removeFocus();

		if( this._isOpen() ) {
			if( this.current !== -1 ) {
				// update placeholder text
				this.selPlaceholder.textContent = this.selOpts[ this.current ].textContent;
			}
			classie.remove( this.selEl, 'cs-active' );
		}
		else {
			if( this.hasDefaultPlaceholder && this.options.stickyPlaceholder ) {
				// everytime we open we wanna see the default placeholder text
				this.selPlaceholder.textContent = this.selectedOpt.textContent;
			}
			classie.add( this.selEl, 'cs-active' );
		}
	}

	/**
	 * change option - the new value is set
	 */
	SelectFx.prototype._changeOption = function() {
		// if pre selected current (if we navigate with the keyboard)...
		if( typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ) {
			this.current = this.preSelCurrent;
			this.preSelCurrent = -1;
		}

		// current option
		var opt = this.selOpts[ this.current ];

		// update current selected value
		this.selPlaceholder.textContent = opt.textContent;

		// change native select element´s value
		this.el.value = opt.getAttribute( 'data-value' );

		// remove class cs-selected from old selected option and add it to current selected option
		var oldOpt = this.selEl.querySelector( 'li.cs-selected' );
		if( oldOpt ) {
			classie.remove( oldOpt, 'cs-selected' );
		}
		classie.add( opt, 'cs-selected' );

		// if there´s a link defined
		if( opt.getAttribute( 'data-link' ) ) {
			// open in new tab?
			if( this.options.newTab ) {
				window.open( opt.getAttribute( 'data-link' ), '_blank' );
			}
			else {
				window.location = opt.getAttribute( 'data-link' );
			}
		}

		// callback
		this.options.onChange( this.el.value );
	}

	/**
	 * returns true if select element is opened
	 */
	SelectFx.prototype._isOpen = function(opt) {
		return classie.has( this.selEl, 'cs-active' );
	}

	/**
	 * removes the focus class from the option
	 */
	SelectFx.prototype._removeFocus = function(opt) {
		var focusEl = this.selEl.querySelector( 'li.cs-focus' )
		if( focusEl ) {
			classie.remove( focusEl, 'cs-focus' );
		}
	}

	/**
	 * add to global namespace
	 */
	window.SelectFx = SelectFx;

} )( window );

//Mask form script
(function(global){
    var $ = global.jQuery || false;

    var addEvent = function(evnt, elem, func) {
        if (elem.addEventListener)  // W3C DOM
            elem.addEventListener(evnt,func,false);
        else if (elem.attachEvent) { // IE DOM
            elem.attachEvent("on"+evnt, func);
        }
        else {
            elem[evnt] = func;
        }
    };

    var VanillaMask = function(input, options) {
        var that = this;

        if (!input || !options) {
            return;
        }

        if ($ && input instanceof $) {
            this.jquery = true;
            this.$input = input;
            this.input = input.get(0);
        } else {
            this.jquery = false;
            this.input = input;
        }
        this.options = options;
        this.placeholderChar = /[0-9a-zA-Z]/;
        this.masks = options.masks;
        this.render();
        addEvent('keyup', this.input, function(event){
            var code = event.keyCode || event.which;
            if(code != 37 && code != 39){
                that.render();
            }
        });
    };

    VanillaMask.prototype = {
        _getMaskPartial: function(mask, length){
            var resultMask = "";
            var placeholderCount = 0;
            for(var i=0; i< mask.length; i++) {
                resultMask += mask.charAt(i);
                if(mask.charAt(i).match(this.placeholderChar)) {
                    placeholderCount++;
                }

                if(placeholderCount == length){
                    break;
                }
            }

            return resultMask;
        },

        _getLargerMask: function() {
            if (this.masks.length > 1) {
                return this.masks[0].length > this.masks[1].length ? this.masks[0] : this.masks[1];
            } else {
                return this.masks[0];
            }
        },

        _getLowerMask: function() {
            if (this.masks.length > 1) {
                return this.masks[0].length < this.masks[1].length ? this.masks[0] : this.masks[1];
            } else {
                return this.masks[0];
            }
        },

        _getMaskPositions: function(mask){
            var placeholders = mask.replace(/\D/g, '');
            return placeholders.length;
        },

        _getMask: function(value) {
            if(this.masks.length === 1) {
                return this.masks[0];
            } else {
                var largeMask = this._getLargerMask();
                var lowerMask = this._getLowerMask();
                var largeMaskPos = this._getMaskPositions(largeMask);
                return value.length < largeMaskPos ? lowerMask : largeMask;
            }
        },

        render: function() {
            var valOriginal = this.input.value.replace(/\D/g, '');
            var mask = this._getMask(valOriginal);
            var maskPartial = this._getMaskPartial(mask, valOriginal.length);
            var originalPos = 0;
            var finalValue = "";

            for(var i=0; i<maskPartial.length; i++) {
                if(maskPartial.charAt(i).match(this.placeholderChar)) {
                    finalValue += valOriginal.charAt(originalPos);
                    originalPos++;
                } else {
                    finalValue += maskPartial.charAt(i);
                }
            }

            if(valOriginal === "") {
                finalValue = "";
            }

            if (this._getMaskPositions(mask) === valOriginal.length) {
                var args = [valOriginal, this.input, mask];
                if (typeof this.options.onComplete === 'function') {
                    this.options.onComplete.apply(this, args);
                }

                if (this.jquery) {
                    this.$input.trigger('complete.VanillaMask', args);
                }
            }

            this.input.value = finalValue;
        }
    };

    if ($) {
        $.fn.VanillaMask = function( method ) {
            var args = arguments;

            return this.each(function() {
                if ( !$.data(this, 'VanillaMask') ) {
                    $.data(this, 'VanillaMask', new VanillaMask($(this), method));
                    return;
                }

                var api = $.data(this, 'VanillaMask');

                if ( typeof method === 'string' && method.charAt(0) !== '_' && api[ method ] ) {
                    api[ method ].apply( api, Array.prototype.slice.call( args, 1 ) );
                } else {
                    $.error( 'Method ' +  method + ' does not exist on jQuery.VanillaMask' );
                }
            });
        };
    }

    global.VanillaMask = VanillaMask;
})(window);

//call scripts
$(document).ready( function(){

	//Tabs processo
	(function ($) {

		$('.process ul.process-steps').addClass('active').find('> li:eq(0)').addClass('current');

		$('.process ul.process-steps li a').click(function (g) {
			var tab = $(this).closest('.process'),
				index = $(this).closest('li').index();

			tab.find('ul.process-steps > li').removeClass('current');
			$(this).closest('li').addClass('current');

			tab.find('.process-content').find('div.process-item').not('div.process-item:eq(' + index + ')').slideUp();

			tab.find('.process-content').find('div.process-item:eq(' + index + ')').slideDown();

			g.preventDefault();
		} );
	})(jQuery);

	//Banner principal
	$('#banner').slick({
		arrows: false,
		slide: 'div',
	    dots: true,
	    autoplay: true,
		autoplaySpeed: 5000,
	});

	//Bebidas
	$('.bebidas-refrigerante').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
			
	$('.bebidas-agua').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
			
	$('.bebidas-alcolicas').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
			
			

	//Sabores de pizza
	$('.sabores-massapan').click( function(){
		$(this).toggleClass("slider-active");

		if ($('.sabores-massapan').hasClass("slider-active")) {
			$('.pizza-slider-pan').css({display: "none"});
			$('.pizza-slider-pan').unslick();

			$('.pizza-slider-pan').css({display: "block"});

			//Sabores de pizza
			$('.pizza-slider-pan').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
		}else{
			$('.pizza-slider-pan').css({display: "none"});
			$('.pizza-slider-pan').unslick();
		}
	});

	$('.sabores-pantastica').click( function(){
		$(this).toggleClass("slider-active");

		if ($('.sabores-pantastica').hasClass("slider-active")) {
			$('.pizza-slider-pantastica').css({display: "none"});
			$('.pizza-slider-pantastica').unslick();

			$('.pizza-slider-pantastica').css({display: "block"});

			//Sabores de pizza
			$('.pizza-slider-pantastica').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
		}else{
			$('.pizza-slider-pantastica').css({display: "none"});
			$('.pizza-slider-pantastica').unslick();
		}
	});

	$('.sabores-artezanale').click( function(){
		$(this).toggleClass("slider-active");

		if ($('.sabores-artezanale').hasClass("slider-active")) {
			$('.pizza-slider-artezanale').css({display: "none"});
			$('.pizza-slider-artezanale').unslick();

			$('.pizza-slider-artezanale').css({display: "block"});

			//Sabores de pizza
			$('.pizza-slider-artezanale').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
		}else{
			$('.pizza-slider-artezanale').css({display: "none"});
			$('.pizza-slider-artezanale').unslick();
		}
	});

	$('.sabores-fininha').click( function(){
		$(this).toggleClass("slider-active");

		if ($('.sabores-fininha').hasClass("slider-active")) {
			$('.pizza-slider-fininha').css({display: "none"});
			$('.pizza-slider-fininha').unslick();

			$('.pizza-slider-fininha').css({display: "block"});

			//Sabores de pizza
			$('.pizza-slider-fininha').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
		}else{
			$('.pizza-slider-fininha').css({display: "none"});
			$('.pizza-slider-fininha').unslick();
		}
	});

	$('.sabores-cheesy').click( function(){
		$(this).toggleClass("slider-active");

		if ($('.sabores-cheesy').hasClass("slider-active")) {
			$('.pizza-slider-cheesy').css({display: "none"});
			$('.pizza-slider-cheesy').unslick();

			$('.pizza-slider-cheesy').css({display: "block"});

			//Sabores de pizza
			$('.pizza-slider-cheesy').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
		}else{
			$('.pizza-slider-cheesy').css({display: "none"});
			$('.pizza-slider-cheesy').unslick();
		}
	});

	$('.sabores-borda').click( function(){
		$(this).toggleClass("slider-active");

		if ($('.sabores-borda').hasClass("slider-active")) {
			$('.pizza-slider-borda').css({display: "none"});
			$('.pizza-slider-borda').unslick();

			$('.pizza-slider-borda').css({display: "block"});

			//Sabores de pizza
			$('.pizza-slider-borda').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  arrow: true,
			  dots: false,
			  infinite: true
			});
		}else{
			$('.pizza-slider-borda').css({display: "none"});
			$('.pizza-slider-borda').unslick();
		}
	});

	//Submit form newsletter
	$(".form-enviar").click(function() {
		$(".submit").click();
	});

	//Efeito fora do container receitas.
	$(document).ready(receitasTitle);
	$(window).resize(receitasTitle);

	function receitasTitle(){
		var largura = $('body').width();
		var larguraContainer = $(".container").width();

		var total = (largura - larguraContainer) / 2;
		
		$(".pizza-left .promo-title h1").css({'position': 'absolute','width': 276 + total + 'px', 'left':'-'+total+'px'});
		$(".cardapio-left .pizza-title h1").css({'position': 'absolute','width': 276 + total + 'px', 'left':'-'+total+'px'});
		$(".cardapio-right .pizza-title h1").css({'position': 'absolute','width': 276 + total + 'px', 'right':'-'+total+'px'});
		$(".page-left .left-title h1").css({'position': 'absolute','width': 220 + total + 'px', 'left':'-'+total+'px'});
	}
});

//Custom style form select field
(function() {
	[].slice.call( document.querySelectorAll( '.form-select' ) ).forEach( function(el) {
		new SelectFx(el);
	});
})();

//Form inputs mask
$(".data-nascimento").VanillaMask({
    masks: ['99/99/9999']
});
$(".rg").VanillaMask({
    masks: ['99.999.999-9']
});
$(".cpf").VanillaMask({
    masks: ['999.999.999-99']
});
$(".cep").VanillaMask({
    masks: ['99999-999']
});
$(".tel").VanillaMask({
    masks: ['(99) 9999-9999', '(99) 99999-9999']
});
$(".cel").VanillaMask({
    masks: ['(99) 9999-9999', '(99) 99999-9999']
});
$(".entrada").VanillaMask({
    masks: ['99/99/9999']
});
$(".saida").VanillaMask({
    masks: ['99/99/9999']
});

$(".birth").VanillaMask({
    masks: ['99/99/9999']
});

//Html5 validation fallback old browsers
function hasHtml5Validation () {
  return typeof document.createElement('input').checkValidity === 'function';
}

if (hasHtml5Validation()) {
  $('.form').submit(function (e) {
    if (!this.checkValidity()) {
      e.preventDefault();
      $(this).addClass('invalid');
    } else {
      $(this).removeClass('invalid');
    }
  });
}