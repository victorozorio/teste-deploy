
function supportsSVG() {
    return !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', "svg").createSVGRect;
}

if (supportsSVG()) {
    document.documentElement.className = "svg";
} else {
    document.documentElement.className = "";
}

$(function () {
    $('#timePicker').hide();
    $deliverySelectedPanel = $('#carryoutSelectedPanel');
    $carryoutSelectedPanel = $('#carryoutSelectedPanel');
    $useCustomerAddress = $('#useCustomerAddress');
    $deliverySelectedPanel.hide();
    $carryoutSelectedPanel.hide();
    $useCustomerAddress.hide();

    if ($('#showLocationNow').length > 0) {
        $('html').addClass('showLocationNow');
    }

    $('#editAddressPinDrag').hide();

    if ($('#pinDropPrototypeMobile').length > 0) {

        initialize();
        $('html').addClass('fullScreenMobilePinMap');
    }

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    if ($('#fullScreenEditAddressForm').length > 0) {
        $('#viewOnMapFullScreenLInk').click(function () {
            newViewMapLink = "fullscreenmap.aspx";
            newViewMapLink += "#" + $("#pinDropEditWide1").val() + "|" + $("#pinDropEditWide6").val();
            $('#viewOnMapFullScreenLInk').attr('href', newViewMapLink);
        });

        $("#pinDropEditWide1, #pinDropEditWide6").change(function () {
            newViewMapLink = "fullscreenmap.aspx";
            newViewMapLink += "#" + $("#pinDropEditWide1").val() + "|" + $("#pinDropEditWide6").val();
            $('#viewOnMapFullScreenLInk').attr('href', newViewMapLink);
        });
    }

    if ($('#pinDragFieldset').length > 0) {
        if (window.location.hash) {
            var thisHash = window.location.hash;
            thisHash = thisHash.substring(1);
            var thisHashSplit = thisHash.split("|");

            for (var i = 0; i < thisHashSplit.length; i++) {
                if (thisHashSplit[i] != "undefined") {
                    $('#pinDropEditWide' + (i + 1)).val(thisHashSplit[i]);
                }
            }
        }
    }

    $('#pinDragEditAddressButton').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        $('#editAddressPinDrag').slideToggle();
        var targetPos = $("#pinDragEditAddressButton").offset().top;
        $("html, body").animate({ scrollTop: (targetPos) + "px" }, 1200);
    });

    $('#pinDragUpdateMap').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        $('#editAddressPinDrag').slideUp();
        var targetPos = $("#mapContainer").offset().top;
        $("html, body").animate({ scrollTop: (targetPos) + "px" }, 1200);
        encodePostCode($('#pinDropEditWide6').val());
    });

    function encodePostCode(thisPostcode) {
        if ((thisPostcode != "") && (typeof thisPostcode !== "undefined")) {
            geocoder.geocode({ 'address': thisPostcode }, function postcodesearch(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var lat = results[0].geometry.location.lat();
                    var lng = results[0].geometry.location.lng();
                    newlatLng = new google.maps.LatLng(lat, lng);
                    map.setCenter(newlatLng);
                    updateMarkerPosition(newlatLng);
                    marker.setPosition(newlatLng);
                    geocodePositionNewHouseNumber(newlatLng, $('#pinDropEditWide1').val());
                }
            });
        }
    }

    $pickAStore = $('#pickAStore');
    $pickAStore.hide();
    $carryoutSelectedPanel = $('#carryoutSelectedPanel');
    $carryoutSelectedPanel.hide();
    $deliverySelectedPanel = $('#carryoutSelectedPanel');
    $deliverySelectedPanel.hide();

    if ($('#tileGrid').length > 0) {
        $('#tileTandCs').show();
    }

    $('#pindropExplantoryText, #pinDropExplanClose').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        if (viewport().width < 750) {
            $('#pindropExplantoryText').hide();
        }
    });

    $('#gotACoupon a').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        $('#couponEntryTooltip').fadeToggle();
    });

    $('.findAStoreSubmit').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        $('html, body').scrollTop(0);
        $carryoutSelectedPanel.hide();

        if ($(this).find('i').length > 0) {
            $('#geoLocationMessage').show();
        }
    });

    if ($(this).is(':checked')) {
        $('#deliveryAddressForm').hide();
    } else {
        $('#deliveryAddressForm').show();
    }

    $('.useDeliveryAddressHandler').click(function (e) {
        if ($(this).is(':checked')) {
            $('#deliveryAddressForm').hide();
        } else {
            $('#deliveryAddressForm').show();
        }
    });

    $('.linneyCustomOptions').hide();
    $('.linneyCustomDropDown').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        $(this).next().find('ul').toggle();
    });
    $('.linneyCustomOptions li').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        var thisAttr = $(this).data("summary");
        $(this).parent().parent().prev().find('span').html(thisAttr);
        $(this).parent().hide();
    });

    if ($('.linneyCustomOptions').length > 0) {
        if (viewport().width < 979) {
            var targetWidth = $('.productCard').outerWidth();

            if (viewport().width > 350) {
                if (targetWidth < 250) {
                    targetWidth = 250;
                }
            }

            $('.linneyCustomOptions').width(targetWidth);
            var targetOffset = parseInt($('.linneyCustomDropDown').offset().left) - parseInt($('.productCard').parent().css('padding-left'));
            $('.linneyCustomOptions').css('left', (0 - targetOffset) + "px");
            var targetOffset = parseInt($('.linneyCustomDropDown:eq(1)').offset().left) - parseInt($('.productCard').parent().css('padding-left'));
            $('.linneyCustomOptions.linneyCustomRight').css('left', (0 - targetOffset) + "px");
        }
    }

    function advanceStackedCarousel(whichDir) {
        if (whichDir > 0) {
            if (parseInt($('#innerCarousel').css('left')) >= 0) {
                $('#innerCarousel').css('left', maxOffset);
            }
        }

        $('#innerCarousel').stop(true).animate({
            left: "+=" + (whichDir * carouselItemWidth)
        }, 500, function () {
            var currentOffset = parseInt($('#innerCarousel').css('left'));
            console.log(currentOffset + ", " + maxOffset);

            if (currentOffset <= maxOffset) {
                $('#innerCarousel').css('left', 0);
            }
        });
    }

    carouselIsInit = false;

    if ($('#stackedCarousel').length > 0) {
        if (viewport().width < 750) {
            if (!carouselIsInit) {
                carouselItemWidth = 274;
                numberOfItems = 8;
                maxOffset = 0 - (numberOfItems * carouselItemWidth);
                ul = $('#innerCarousel');
                ul.find('li').slice(0, 2).clone(true).appendTo(ul);
                stackedCarouselAdvance = setInterval(function () { advanceStackedCarousel(-1) }, 3000);
                carouselIsInit = true;
            }
        }
    }

    $('#addLocationDetails').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        if (!validateRegisterSteps(this)) {
            return;
        }

        $('#registrationPanel2').click();
        var viewportPaddingTop = $('#viewport .inner-wrap').css("padding-top");
        var targetPos = $("#registrationPanel2").offset().top - parseInt(viewportPaddingTop);
        $("html, body").animate({ scrollTop: (targetPos) + "px" }, 1200);
    });

    $('#addSecurityDetails').click(function (e) {
        if ($('#CustomerAddressData_Complement').val().length > 20) {
            e.preventDefault();
            return;
        }

        if (e) {
            e.preventDefault();
        }

        if (!validateRegisterSteps(this)) {
            return;
        }

        $('#registrationPanel3').click();
        var viewportPaddingTop = $('#viewport .inner-wrap').css("padding-top");
        var targetPos = $("#registrationPanel3").offset().top - parseInt(viewportPaddingTop);
        $("html, body").animate({ scrollTop: (targetPos) + "px" }, 1200);
    });

    $('#locationSearchResults').hide();

    $('#locationSearch, .locationSearchLocation').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        $('#locationSearchResults').show();
    });

    function setMenuLinks() {
        $('.pickAStoreTarget').attr('href', '#\/menu\/');
    }

    $('#startYourOrderButton').click(function () {
        window.setTimeout(setMenuLinks, 1000);
        window.setTimeout(setMenuLinks, 2000);
        window.setTimeout(setMenuLinks, 3000);
        window.setTimeout(setMenuLinks, 4000);
        window.setTimeout(setMenuLinks, 5000);
    });

    $('#addExtraInformation').click(function (e) {
        if ($(this).text() == "Add extra information") {
            $('html').addClass("pindropAddInfoMode");
            $(this).text('Back to map');
        } else {
            $('html').removeClass("pindropAddInfoMode");
            $(this).text('Add extra information');
        }
        e.preventDefault();
    });

    $('.pinDropAddressEdit').click(function (e) {
        e.preventDefault();

        if ($(this).text() == "Edit") {
            var addressContent = $.trim($('#address').html());

            if (viewport().width < 750) {
                $('html').addClass("pindropEditMode");
                var addressSplit = addressContent.split(",");
            } else {
                var addressSplit = addressContent.split("<br>");
            }

            outputHtml = "";
            addressSplit[0] = addressSplit[0].substr(3);
            addressSplit[(addressSplit.length - 1)] = addressSplit[(addressSplit.length - 1)].substring(0, ((addressSplit[(addressSplit.length - 1)].length) - 4));

            for (pinDropAddressEdit = 0; pinDropAddressEdit < addressSplit.length; pinDropAddressEdit++) {
                outputHtml += '<input type="text" value="' + $.trim(addressSplit[pinDropAddressEdit]) + '">';
            }

            $('#address').html(outputHtml);
            $(this).text("Update");
        } else {
            if (viewport().width < 750) {
                $('html').removeClass("pindropEditMode");
            }

            var outputHtml = "<p>";

            if (viewport().width < 750) {
                $("#address input").each(function (index) {
                    outputHtml += $(this).val() + ", ";
                });
            } else {
                $("#address input").each(function (index) {
                    outputHtml += $.trim($(this).val()) + "<br>";
                });
            }

            if (viewport().width < 750) {
                outputHtml = outputHtml.substring(0, outputHtml.length - 2);
            } else {
                outputHtml = outputHtml.substring(0, outputHtml.length - 4);
            }

            outputHtml += "</p>";
            $('#address').html(outputHtml);
            $(this).text("Edit");
        }
    });

    if ($('#centreMapPinDropMarker').length > 0) {
        if (viewport().width < 400) {
            $('html').addClass("fullScreenMobileMap");
        }
    }

    if ($('#storeSelectorRedirect').length > 0) {
        var targetLocation = $('#storeSelectorRedirect').data('href');

        if (viewport().width > 749) {

            $('#myModal').foundation('reveal', 'open', targetLocation);

        } else {
            document.location.href = targetLocation;
        }
    }

    if ($('#splitPaymentRadios').length > 0) {
        $('#giftCard, #giftCardApplied').hide();
        $('#splitPaymentRadios').find('div').click(function (e) {
            var thisAttr = $(this).data('attr');
            var radio = $(this).children('input');

            if (thisAttr && thisAttr != "nope") {
                $('#securityCodeOneClick').attr('required', false);

                if ($(this).data('data-info').Online) {
                    $('#PaymentPanel_Online').appendTo($(this).children('.contentPaymentType'));
                }
                else if ($(this).data('data-info').MayHaveChange) {
                    $('#PaymentPanel_Offline').appendTo($(this).children('.contentPaymentType'));
                }
                else if ($(this).data('data-info').OneClick) {
                    $('#PaymentPanel_OneClick').appendTo($(this).children('.contentPaymentTypeOneClick'));
                    $('#securityCodeOneClick').attr('required', true);
                }
                else {
                    $(this).children('.contentPaymentType').empty();
                }

                $('.splitPaymentPanel').hide();
                $('#' + thisAttr).slideDown();
                $('#splitPaymentApply').show();

                $('#splitPaymentRadios').find('input').removeClass('checked');
                radio.addClass('checked');
            }

            e.stopPropagation();
        });

        $('#splitPaymentApply').click(function (e) {
            e.preventDefault();
            $('#giftCardApplied, #creditCard').slideDown();
            $(this).hide();
            e.stopPropogation();
        });
    }

    isInHalfHalfMode = false;
    $makeItHalfHalfButton = $('#makeItHalfHalfButton');
    $makeItWholeButton = $('#makeItWholeButton');
    leftHalfHeight = $('#leftHalf').height();
    rightHalfHeight = $('#rightHalf').height();
    $('#leftHalf').height(0);
    $('#rightHalf').height(0);

    function continueExpand() {
        $("#leftHalf").animate({
            height: leftHalfHeight
        }, 1000, function () {
            $('#notHalfHalf').hide();
        });

        $("#rightHalf").animate({
            height: rightHalfHeight
        }, 1000, function () {
            $("#rightHalf").height("auto");
            $("#leftHalf").height("auto");
        });
    }

    $makeItHalfHalfButton.click(function (e) {
        e.preventDefault();

        if (!isInHalfHalfMode) {
            $("#leftHalf").animate({
                height: 55
            }, 500, function () {
                setTimeout(continueExpand, 400);
            });
            $("#rightHalf").animate({
                height: 55
            }, 500);
            $('#halfHalfSummaryList').show();
            $('.wholeOnly, .summaryListAppended').hide();
        }

        isInHalfHalfMode = true;
        $(this).removeClass("secondary");
        $makeItWholeButton.addClass("secondary");
    });

    $('.heartIcon').click(function (e) {
        e.preventDefault();
        $(this).parent().fadeOut(1000, function () { $(this).remove(); });
    });

    $('.slideToggleContainer').hide();
    $('.slideToggleLink').click(function (e) {
        e.preventDefault();
        $(this).hide();
        $(this).next().slideDown();
    });

    $makeItWholeButton.click(function (e) {
        e.preventDefault();

        if (isInHalfHalfMode) {
            $('#notHalfHalf').show();
            $("#rightHalf").animate({
                height: 0
            }, 1000);
            $("#leftHalf").animate({
                height: 0
            }, 1000);
            $('#halfHalfSummaryList').hide();
            $('.wholeOnly, .summaryListAppended').show();
        }

        isInHalfHalfMode = false;
        $(this).removeClass("secondary");
        $makeItHalfHalfButton.addClass("secondary");
    });

    if ($('#hideHutLovers').length > 0) {
        $('#hutLovers').hide();
    }

    if ($('#sharedPanel').length > 0) {
        $window = $(window);
        function windowScroll() {
            windowScrollTop = $window.scrollTop(); windowScrollTop = $window.scrollTop();
            var tabletHeadHeight = $('.catHeader').position().top + $('.catHeader').outerHeight(true);

            if (windowScrollTop > tabletHeadHeight) {
                var sidePanelHeight = $("#builderColumnRight").innerHeight();
                var pizzaImageHeight = $("#builderColumnLeft").innerHeight();

                if ((windowScrollTop) > tabletHeadHeight + sidePanelHeight - pizzaImageHeight) {
                    $('html').addClass("bottomTabletDock");
                    $('html').removeClass("fixPizzaImage");
                } else {
                    $("#builderColumnLeft").width($("#builderColumnLeft").width());
                    $('html').removeClass("bottomTabletDock");
                    $('html').addClass("fixPizzaImage");
                }
            } else {
                $('html').removeClass("fixPizzaImage");
            }
        }

        $window.scroll(windowScroll);
        windowScroll();
    }

    $('.builderDropdown a, .toppingDropdown a').click(function (e) {
        e.preventDefault();
        $(this).next().slideToggle();
    });

    $('.builderDropdown li').click(function (e) {
        e.preventDefault();
        var thisTitle = $(this).find('h4').text();
        $(this).parent().prev().find('span').text(thisTitle);
        $(this).parent().slideUp();
    });

    $('.threeWayToggle li').click(function (e) {
        e.preventDefault();
        $(this).addClass('active');
        $(this).siblings().removeClass('active');

        if ($(this).parent().parent().parent().parent().parent().hasClass("toppingDropdown")) {
            isLeftHalf = false;
            isRightHalf = false;

            if ($(this).parents('#leftHalf').length > 0) {
                isLeftHalf = true;
            }

            if ($(this).parents('#rightHalf').length > 0) {
                isRightHalf = true;
            }

            var thisAmount = $(this).text();
            var thisTopping = $(this).parent().prev().text();
            var currentAddedList = $(this).parent().parent().parent().parent().prev().find('.addedList').html();
            var currentAddedListSplit = currentAddedList.split(", ");
            var numberOfAddedItems = currentAddedListSplit.length;
            var currentAddFoundIndex = -1;

            for (var i = 0; i < numberOfAddedItems; i++) {
                if (currentAddedListSplit[i].toLowerCase().indexOf(thisTopping.toLowerCase()) != -1) {
                    currentAddFoundIndex = i;
                }
            }

            if (currentAddFoundIndex == -1) {

                if (currentAddedList != "") {
                    currentAddedList += ", ";
                }

                currentAddedList += thisTopping;
                $(this).parent().parent().parent().parent().prev().find('.addedList').html(currentAddedList);
                $(this).parent().parent().parent().parent().parent().find('.fi-check').show();
            } else if (thisAmount.toLowerCase() == "none") {
                currentAddedListSplit.splice(currentAddFoundIndex, 1);
                currentAddedList = currentAddedListSplit.join(", ");
                $(this).parent().parent().parent().parent().prev().find('.addedList').html(currentAddedList);

                if (numberOfAddedItems == 1) {
                    $(this).parent().parent().parent().parent().parent().find('.fi-check').hide();
                }
            }

            var $elementToUse = $("#summaryList");

            if (isLeftHalf) {
                $elementToUse = $("#summaryListLeft");
            }

            if (isRightHalf) {
                $elementToUse = $("#summaryListRight");
            }

            var foundIndex = -1;
            $elementToUse.find("li").each(function (index) {
                if ($(this).text().toLowerCase().indexOf(thisTopping.toLowerCase()) != -1) {
                    foundIndex = index;
                }
            });

            if (foundIndex == -1) {
                var stringToAdd = '<li class="clearfix summaryListAppended"><img src="images/builder/summary-topping.gif" />' + thisTopping + ' <span>' + thisAmount + '</span></li>';
                $elementToUse.append(stringToAdd);
            } else {
                if (thisAmount.toLowerCase() == "none") {
                    $elementToUse.children().slice(foundIndex, foundIndex + 1).remove();
                } else {
                    $elementToUse.find("li:eq(" + foundIndex + ") span").html(thisAmount);
                }
            }
        }
    });

    $('#offCanvasCartTotals').hide();
    $('#offCanvasCartList').hide();
    $upSellWrapper = $('#upSellWrapper');
    $upSellWrapper.hide();
    $wideAddedToCartTooltip = $('#wideAddedToCartTooltip');
    $wideAddedToCartTooltip.hide();
    $wideAddedToCartTooltipMobile = $('#wideAddedToCartTooltipMobile');
    $wideAddedToCartTooltipMobile.hide();
    $cartCounter = $('#cartCounter');
    $miniCartCounter = $('#miniCartCounter');

    function removeAlert() {
        $('#cartProductDynamicList li').removeClass("justAdded");
    }

    function showUpsell() {
        $upSellWrapper.slideDown();
    }

    $viewport = $('#viewport');

    $('#dockedHeader .left-off-canvas-toggle').click(function (e) {
        $("html, body").animate({ scrollTop: "0px" }, 0);
    });

    $('#cartIcon').click(function (e) {
        $upSellWrapper.hide();
        $("html, body").animate({ scrollTop: "0px" }, 0);
    });

    $('.upsellViewOrder').click(function (e) {
        if (isMobile) {
            if (e) {
                e.preventDefault();
            }

            $('#cartIcon').click();
        }
    });

    $('#dismissUpSell').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        $('#upSellWrapper').slideUp();
    });

    $('.cancelCartEdit').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        window.history.go(-1);
    });

    $('#welcomeBackDismiss').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        $('#welcomePanel').fadeOut(1000);
    });

    $('#cartProductList').on('click', '.dynamicCartListDelete', function (e) {
        if (e) {
            e.preventDefault();
        }

        if (typeof upsellTimer !== "undefined") {
            clearTimeout(upsellTimer);
        }

        $(this).parents('li').fadeOut(750);
        $miniCartCounter.html(parseInt($miniCartCounter.text()) - 1);
        $('#dynamicCartInlineQuantity').html(parseInt($miniCartCounter.text()));
        $('#dynamicCartInlinePrice').html(parseInt($miniCartCounter.text()) * 14);
        $('#dynamicCartSummaryPrice').html(parseInt($miniCartCounter.text()) * 14);
        $('#dynamicCartSummaryPriceTotal').html((parseInt($miniCartCounter.text()) * 14) + 5);
    });

    $('#cartProductList').on('click', '.nameWrapper', function (e) {
        if (viewport().width < 500) {
            if (e) {
                e.preventDefault();
            }

            $(this).parent().toggleClass('toggleReveal');
        }
    });

    $('#addHalfPizzaToOrderButton').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        var button = $(this);
        putLoadingGif(button);

        var $form = button.closest('form');
        var $dform = $form.clone();

        prepareForm($form, $dform);

        var mainDiv = button.closest('#halfPizzasModal');

        var optionalsLeft = $('div.optionalsLeft');
        var optionalsRight = $('div.optionalsRight');
        var productImage = $('#pizzaRightImage > img');
        var productValue = $($dform).find('.price > span').text();
        var productName = "1/2 " + mainDiv.find('.pizzaLeft :selected').text();
        productName += ", 1/2 " + mainDiv.find('.pizzaRight :selected').text();

        var idMenuDishLeft = mainDiv.find('.pizzaLeft :selected').val();
        var idMenuDishRight = mainDiv.find('.pizzaRight :selected').val();

        fillRequestMenuDishFractionDish($dform, idMenuDishLeft, 0);
        fillRequestMenuDishFractionDish($dform, idMenuDishRight, 1);

        fillRequestMenuDishOptional($dform, optionalsLeft, 0);
        fillRequestMenuDishOptional($dform, optionalsRight, 1);

        $.post($dform.attr('action'), $dform.serialize(), function (result) {
            if (result.Success) {
                addCartCallBack(productImage, productName, optionalsRight, productValue, result, false, true);
                $('a.close-reveal-modal').click();
            } else {
                showMessage(result.ErrorMessage);
            }

            button.empty();
            button.text('ADICIONAR');
        });

    });

    $('.addToOrderButton').click(function (e) {
        if (e) {
            e.preventDefault();
        }

        if ($(this).hasClass('btn-disabled') || $(this).hasClass('disabled')) {
            return false;
        }

        var $addToOrderBtn = $(this);

        putLoadingGif($addToOrderBtn);

        try {
            var $button = $(this);
            var $thisProductCard = $(this).parents('.productCard');
            var $thisProductImage = $thisProductCard.find('div > img');
            var thisProductName = $thisProductCard.find('.large-8 > h4').text();
            var $thisProductOptions = $thisProductCard.find('.productOptions');
            var thisProductValueText = $thisProductCard.find('.price').text();
            var thisProductValue = parseInt(thisProductValueText.replace(/\D/g, '')) / 100;
            var $form = $(this).closest('form');
            var $dform = $form.clone();

            prepareForm($form, $dform);

            $.post($dform.attr('action'), $dform.serialize(), function (result) {
                if (result.Success) {
                    addCartCallBack($thisProductImage, thisProductName, $thisProductOptions, thisProductValueText, result, false, false);
                    resetProductCard($form.parent());
                    RefreshOptionalSelect($form);
                    calculateMenuDishTotalValue($form);
                    refreshPrices(result.MustUpdateDishList);
                } else {
                    showMessage(result.ErrorMessage);
                }

                $addToOrderBtn.empty();
                $addToOrderBtn.text("ADICIONAR");
            });
        } catch (err) {
            $addToOrderBtn.empty();
            $addToOrderBtn.text("ADICIONAR");
        }
    });

    $('#divSellingOut').on("click", ".addSellingOutButton", function (e) {
        if (e) {
            e.preventDefault();
        }

        var $thisProductCard = $(this).parents('.sellingOutDetails');
        var $thisProductImage = $thisProductCard.find('img');
        var thisProductName = $thisProductCard.find('.large-9 > h6.sideName').text();
        var $thisProductOptions = "";
        var thisProductValueText = $thisProductCard.find('span.uniquePrice').text();
        var thisProductValue = parseInt(thisProductValueText.replace(/\D/g, '')) / 100;
        var thisProductIdMenuDish = $thisProductCard.find("input[name='requestModel.CartMenuDish.IdMenuDish']").val();
        var $form = $(this).closest('form');
        var $dform = $form.clone();

        putLoadingGif($(this));

        $.post($dform.attr('action'), $dform.serialize(), function (result) {
            try {
                if (result.Success) {
                    addCartCallBack($thisProductImage, thisProductName, $thisProductOptions, thisProductValueText, result, true, false);
                    refreshCartDishes(result);
                    refreshSellingOutDishesSalesPromotion(result.CartData.CartData.CartMenuSellingOutDishList, result.CartData.CartData.CartMenuSalesPromotionGroupedList);
                    calculateTotal(result);
                } else {
                    showMessage(result.ErrorMessage);
                }
            } finally {
                $(this).empty();
                $(this).text("ADICIONAR");
            }
        });
    });


    function addCartCallBack($thisProductImage, thisProductName, $thisProductOptions, thisProductValueText, result, isSellingOut, isHalfPizza) {
        var currentScrollPos = $(window).scrollTop();

        if (currentScrollPos > 0) {
            currentScrollPos -= 40;
        }

        var imgElement = $thisProductImage.clone();

        if (isHalfPizza) {
            var imgFractionSrc = $(imgElement).attr("src").toString();
            imgFractionSrc = imgFractionSrc.substring(0, imgFractionSrc.indexOf("pratos/")) + "pratos/9 - Pizza Meio a Meio.png";
            $(imgElement).attr("src", imgFractionSrc);
        }

        var $cloned = $wideAddedToCartTooltip.clone();
        $wideAddedToCartTooltip.after($cloned);
        $cloned.css({ 'top': currentScrollPos + 'px' });
        $cloned.find('#addedProductImage').html($thisProductImage.clone());

        if (isHalfPizza) {
            var imgFractionSrc = $cloned.find('#addedProductImage img').attr("src").toString();
            imgFractionSrc = imgFractionSrc.substring(0, imgFractionSrc.indexOf("pratos/")) + "pratos/9 - Pizza Meio a Meio.png";
            $cloned.find('#addedProductImage img').attr("src", imgFractionSrc);
        }

        $cloned.find('#addedProductName').html(thisProductName);
        var options = "";

        if ($thisProductOptions != "") {
            $.each($thisProductOptions.find('select > option:selected'), function (key, value) {
                if ($(value).val()) {
                    options += $(value).text() + ' ';
                }
            });
        }

        $cloned.find('#addedProductOptions').text(options);
        $cloned.find('#addedProductValue').html(thisProductValueText);

        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop();
            $cloned.css({ 'top': scrollTop + 'px' });
        });

        if (isSellingOut == true) {
            $cloned.find('#addedCartButton').hide();
        }

        $cartCounter.html(result.CartQuantity);
        $miniCartCounter.html(result.CartQuantity);
        $('#cartTotal').html(result.SubTotal.toFixed(2).replace('.', ','));
        $cartCounter.show();
        $miniCartCounter.show();
        $cloned.fadeIn(500).delay(4500).fadeOut(1500, function () { $cloned.remove(); });

        var $clonedMobile = $wideAddedToCartTooltipMobile.clone();
        $wideAddedToCartTooltipMobile.after($clonedMobile);
        $clonedMobile.find('#addedProductImage').html(imgElement);
        $clonedMobile.find('#addedProductName').html(thisProductName);
        var options = "";

        if ($thisProductOptions != "") {
            $.each($thisProductOptions.find('select > option:selected'), function (key, value) {
                if ($(value).val()) {
                    options += $(value).text() + ' ';
                }
            });
        }

        $clonedMobile.find('#addedProductOptions').text(options);
        $clonedMobile.find('#addedProductValue').html(thisProductValueText);

        if (isSellingOut == true) {
            $clonedMobile.find('#addedCartButton').hide();
        }

        $clonedMobile.fadeIn(500).delay(4500).fadeOut(1500, function () { $clonedMobile.remove(); });
    }

    function calculateTotal(cartInfo) {
        $('#cartTotal, #subTotal').text(cartInfo.SubTotal.toFixed(2).replace('.', ','));
        $('#discount').text(cartInfo.Discount.toFixed(2).replace('.', ','));
        $('#deliveryFee').text(cartInfo.DeliveryFee.toFixed(2).replace('.', ','));
        $('#total').text((cartInfo.SubTotal + cartInfo.DeliveryFee - cartInfo.Discount).toFixed(2).replace('.', ','));
    }

    function viewport() {
        var e = window, a = 'inner';

        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }

        return { width: e[a + 'Width'], height: e[a + 'Height'] };
    }

    function checkViewportSize() {
        if (viewport().width > 749) {
            isMobile = false;
            $('.wideModal').attr('data-reveal-id', 'myModal').attr('data-reveal-ajax', 'true');
            $(document).foundation();
        } else {
            isMobile = true;
            $('.wideModal').removeAttr("data-reveal-id").removeAttr("data-reveal-ajax");
            $(document).foundation();
        }

        if ($('#rewardsLink').length > 0) {
            if (!isMobile) {
                $('#rewardsLink').click();
            }
        }
    }

    isMobile = true;
    $(window).resize(function () {
        checkViewportSize();
    });
    checkViewportSize();

    $(document).on('opened.fndtn.reveal', '[data-reveal]', function () {
        if (!(window.ActiveXObject) && "ActiveXObject" in window) {
            window.resizeTo($('#viewport').width(), $('#viewport').height());
        }
    });

    if (isMobile) {
        initCarousels();
        $(window).trigger('resize');
    } else {
        killCarousels();
        $('.list_carousel .prev, .list_carousel .next').hide();
        initGrid();
    }

    var t = setTimeout(function () {
        $('#Veggies, #Recipes, #Seafood, #Spices, #Cheese').hide();
    }, 1000);

    $('#carouselTabButtonGroup a').click(function (e) {
        e.preventDefault();
        $('#carouselTabButtonGroup a').removeClass('active');
        $(this).addClass('active');
        var thisId = $(this).text();
        $('#Meats, #Veggies, #Recipes, #Seafood, #Spices, #Cheese').hide();
        $('#' + thisId).show();
    });

    $('#carouselMeats').addClass('active');
    $('#carouselTooltip').hide();

    function killCarousels() {
        if ($('#chooseToppings').length > 0) {
            $("#chooseToppings").trigger("destroy", true);
            $("#chooseVeggies").trigger("destroy", true);
            $("#chooseRecipes").trigger("destroy", true);
            $("#chooseSeafood").trigger("destroy", true);
            $("#chooseCheese").trigger("destroy", true);
            $("#chooseSpices").trigger("destroy", true);
        }
    }

    function initCarousels() {
        if ($('#chooseToppings').length > 0) {
            $('#chooseToppings').carouFredSel({
                responsive: true,
                width: '100%',
                scroll: 3,
                items: {
                    width: 120,
                    visible: {
                        min: 2,
                        max: 8
                    }
                },
                swipe: {
                    onMouse: true,
                    onTouch: true
                },
                prev: '#carouselPrev',
                next: '#carouselNext',
                auto: false
            });

            $('#chooseVeggies').carouFredSel({
                responsive: true,
                width: '100%',
                scroll: 3,
                items: {
                    width: 120,
                    visible: {
                        min: 2,
                        max: 8
                    }
                },
                swipe: {
                    onMouse: true,
                    onTouch: true
                },
                prev: '#carouselPrev2',
                next: '#carouselNext2',
                auto: false
            });

            $('#chooseRecipes').carouFredSel({
                responsive: true,
                width: '100%',
                scroll: 3,
                items: {
                    width: 120,
                    visible: {
                        min: 2,
                        max: 8
                    }
                },
                swipe: {
                    onMouse: true,
                    onTouch: true
                },
                prev: '#carouselPrev3',
                next: '#carouselNext3',
                auto: false
            });

            if ($('#chooseSeafood').length > 0) {
                $('#chooseSeafood').carouFredSel({
                    responsive: true,
                    width: '100%',
                    scroll: 3,
                    items: {
                        width: 120,
                        visible: {
                            min: 2,
                            max: 8
                        }
                    },
                    swipe: {
                        onMouse: true,
                        onTouch: true
                    },
                    prev: '#carouselPrev4',
                    next: '#carouselNext4',
                    auto: false
                });
            }

            if ($('#chooseCheese').length > 0) {
                $('#chooseCheese').carouFredSel({
                    responsive: true,
                    width: '100%',
                    scroll: 3,
                    items: {
                        width: 120,
                        visible: {
                            min: 2,
                            max: 8
                        }
                    },
                    swipe: {
                        onMouse: true,
                        onTouch: true
                    },
                    prev: '#carouselPrev3',
                    next: '#carouselNext3',
                    auto: false
                });

            }

            if ($('#chooseSpices').length > 0) {
                $('#chooseSpices').carouFredSel({
                    responsive: true,
                    width: '100%',
                    scroll: 3,
                    items: {
                        width: 120,
                        visible: {
                            min: 2,
                            max: 8
                        }
                    },
                    swipe: {
                        onMouse: true,
                        onTouch: true
                    },
                    prev: '#carouselPrev4',
                    next: '#carouselNext4',
                    auto: false
                });

            }

            $('.prev, .next').click(function (e) {
                $('#carouselTooltip').hide();
            });

            $('#toppingsTable').on("click", ".removeIcon", function (e) {
                e.preventDefault();
                $(this).parent().parent().fadeOut(1000, function () { $(this).remove(); });
                e.stopPropogation();
            });

            $('.no-touch #carouselTooltip span, .no-touch #carouselTooltip a, .touch .touchAddToPizza').click(function (e) {
                e.preventDefault();
                $('#carouselTooltip').hide();

                if (($('.hiddenTable').length <= 0)) {
                    var newMarkUp = '<tr><td>Ham</td><td><div class="wholeSplit"><span class="leftOn"></span><span class="rightOn"></span></div></td><td><form class="custom" style="margin:0;"><label for="checkbox1"><input type="checkbox" id="checkbox3" style="display: none;"><span class="custom checkbox"></span> extra</label></form></td><td><a href="#" class="removeIcon"><i class="gen-enclosed foundicon-remove"></i></a></td></tr>';
                    $('#toppingsTable').append(newMarkUp);
                    $savedClickImage.attr('src', 'images/toppings-mono-tick.jpg');
                }

                e.stopPropogation();
            });

            $('.list_carousel li a').click(function (e) {
                e.preventDefault();
                $('#carouselTooltip').show();
                $('#carouselTooltip .nub').css('left', ($(this).position().left + $(this).width() / 2));
                $savedClickImage = $(this).find('img');

            });

            $('#carouselTooltip span, #carouselTooltip a, .touchAddToPizza').click(function (e) {
                e.preventDefault();
                $('#carouselTooltip').hide();

                if (($('.hiddenTable').length <= 0)) {
                    var newMarkUp = '<tr><td>Ham</td><td><div class="wholeSplit"><span class="leftOn"></span><span class="rightOn"></span></div></td><td><form class="custom" style="margin:0;"><label for="checkbox1"><input type="checkbox" id="checkbox3"><span class="custom checkbox"></span> extra</label></form></td><td><a href="#" class="removeIcon"><i class="fi-x"></i></a></td></tr>';
                    $('#toppingsTable').append(newMarkUp);
                    $savedClickImage.attr('src', 'images/toppings-mono-tick.jpg');
                }

                e.stopPropogation();
            });

            $('#carouselTooltip').hide();
        }
    }

    function initGrid() {
        $clickedParent = undefined;
        clickedId = "";
        removeLinkClicked = false;

        if ($('#chooseToppings').length > 0) {
            if ($('html').hasClass('touch')) {
                $('.list_carousel li > a').click(function (e) {
                    e.preventDefault();
                    showHoverGridToolTip($(this));
                    e.stopPropogation();
                });
            } else {
                $('.list_carousel li > a').hover(
        function () {
            showGridToolTip($(this));

        },
        function () {
            $(this).find('.gridToolTip').remove();

        }
        );
            }

            $('.list_carousel').on("click", ".removeLink", function (e) {
                removeLinkClicked = true;
                var t = setTimeout(function () {
                    removeLinkClicked = false;
                }, 1000);
                e.preventDefault();
                $(this).remove();
            });

            if (!isMobile) {
                $('.list_carousel').on("click", ".gridToolTip", function (e) {
                    if ($(this).parent().find('.removeLink').length == 0) {
                        if (!removeLinkClicked) {
                            var newHtml = '<div class="removeLink"><i class="fi-check"></i><p><a href="#">Remove</a></p></div>';
                            $(this).parent().append(newHtml);
                        }
                    }
                    e.stopPropogation();
                });
            } else {
                $('.touchAddToPizza').click(function () {
                    var newHtml = '<div class="removeLink"><i class="fi-check"></i><p><a href="#">Remove</a></p></div>';
                    $clickedParent.parent().append(newHtml);
                    $('#gridToolTip').hide();
                    e.stopPropogation();
                });
            }
        }
    }

    function showHoverGridToolTip($refElem) {

        if ($refElem.text() == clickedId) {
            $('#gridToolTip').hide();
        } else {
            $clickedParent = $refElem;
            clickedId = $refElem.text();
            $('#gridToolTip').css({
                'left': ($refElem.offset().left - $refElem.parent().parent().offset().left - $refElem.width()) + 'px',
                'top': (($refElem.parent().offset().top) - $refElem.parent().height() * 1.5) + 'px'
            });
            $('#gridToolTip').show();
        }
    }

    function showGridToolTip($refElem) {
        $('#gridToolTip').clone().removeAttr('id').appendTo($refElem).show();
        if (!isMobile) {
            $refElem.find('.gridToolTip').css('left', ($refElem.position().left - $refElem.width() * 1.5));
            $refElem.find('.gridToolTip').css('top', ($refElem.position().top + $refElem.height() - 20));
        } else {
            $refElem.find('.gridToolTip').css('left', ($refElem.position().left - $refElem.width() / 2));
            $refElem.find('.gridToolTip').css('top', ($refElem.position().top + $refElem.height()));
        }
    }

    $('.dropDownSplitLeftRight').hide();

    $('.splitLeftRightInput').click(function (e) {
        if ($(this).data('split') == "right") {
            $(this).parent().parent().next().find('.splitLeft').removeClass('disabled');
            $(this).parent().parent().next().find('.splitLeft').prev().prop('disabled', false);
            $(this).parent().parent().next().find('.splitRight').addClass('disabled');
            $(this).parent().parent().next().find('.splitRight').prev().prop('disabled', true);
        } else {
            $(this).parent().parent().next().find('.splitLeft').addClass('disabled');
            $(this).parent().parent().next().find('.splitLeft').prev().prop('disabled', true);
            $(this).parent().parent().next().find('.splitRight').removeClass('disabled');
            $(this).parent().parent().next().find('.splitRight').prev().prop('disabled', false);
        }

        $(this).parent().parent().next().show();

    });

    $('#viewport').on('click', '.f-dropdown li > a', function (e) {
        e.preventDefault();
        $(this).closest('.f-dropdown').prev().click().text($(this).find('h5').text());

        if ($('#bucketBuilder').length > 0) {
            currentSelected = 0;
            $('a.dropdown').each(function () {
                var thisQuantity = parseInt($(this).text().substring(1));
                currentSelected += thisQuantity;
            });

            if (currentSelected >= amountRequired) {
                $('#bucketContinue').show();
                $('#bucketKeepAdding').hide();
            } else {
                $('#bucketContinue').hide();
                var difference = amountRequired - currentSelected;
                var textString = $('#bucketKeepAdding').text();
                var posOfAdd = textString.indexOf("add ");
                var posOfMore = textString.indexOf("more");
                textString = (textString.substring(0, posOfAdd + 3) + " " + difference + " " + textString.substring(posOfMore));
                $('#bucketKeepAdding').text(textString);
                $('#bucketKeepAdding').show();
            }
        }

        if (($(this).find('h5').text() == "No sauce") || ($(this).find('h5').text() == "No cheese")) {
            $(this).closest('.f-dropdown').nextAll('.dropdown').hide();
        }

        if (($(this).find('h5').text() == "Sauce") || ($(this).find('h5').text() == "Cheese")) {
            $(this).closest('.f-dropdown').nextAll('.dropdown').show();
        }
    });

    $('.nestedDropDownButton').click(function (e) {
        e.preventDefault();
        $(this).next('.nestedDropDown').toggle();
    });

    $('.dropDownWrapper > a, .nestedDropDownButton').click(function (e) {
        $('#behindDropdown').show();
    });

    $('.nestedDropDown').each(function (index) {
        $(this).width($(this).prev().outerWidth());
    });

    $('a.dropdown').click(function (e) {
        e.preventDefault();

        if (isMobile) {
            targetPos = $(this).offset().top;
            $("html, body").animate({ scrollTop: (targetPos) + "px" }, 450);
        }
    });

    function validateRegisterSteps(element) {
        var $dd = $(element).closest('dd');
        var isValid = true;

        $.each($dd.find('input, select'), function (key, value) {
            if (isValid) {
                isValid = $(value).valid();
            } else {
                $(value).valid();
            }
        });

        return isValid;
    }

    function swipeLeft() {
        if ($('#viewport').hasClass('move-right')) {
            $('a.exit-off-canvas').click();
        } else {
            if ($('#stackedCarousel').length > 0) {
                clearInterval(stackedCarouselAdvance);
                advanceStackedCarousel(-1);
            }
        }
    }

    function swipeRight() {
        if ($('#viewport').hasClass('move-left')) {
            $('a.exit-off-canvas').click();
        } else {
            if ($('#stackedCarousel').length > 0) {
                clearInterval(stackedCarouselAdvance);
                advanceStackedCarousel(1);
            }
        }
    }

    if ("ontouchstart" in document.documentElement) {
        document.body.addEventListener("touchstart", function (a) {
            startPointX = a.touches[0].pageX;
            startPointY = a.touches[0].pageY;
            isScrolling = "";
            deltaX = 0
        }, false);
        document.body.addEventListener("touchmove", function (a) {
            if (a.touches.length > 1 || a.scale && a.scale !== 1) { return }

            deltaX = a.touches[0].pageX - startPointX;

            if (isScrolling == "") {
                isScrolling = (isScrolling || Math.abs(deltaX) < Math.abs(a.touches[0].pageY - startPointY))
            }

            if (!isScrolling) { a.preventDefault() }

        }, false);
        document.body.addEventListener("touchend", function (a) {
            if (!isScrolling) {
                if ((Math.abs(deltaX)) > 100) {
                    if (deltaX < 0) {
                        swipeLeft();
                    } else {
                        swipeRight();
                    }
                }
            }
        }, false);
    }

    $("#CustomerAddressData_Complement, #CustomerAddressData_Reference , #Observation , #CustomerData_Name, #PaymentList_0__CardData_CustomerAddressComplement," +
        "#PaymentList_0__CardData_CustomerAddressName, #PaymentList_0__CardData_CustomerAddressNeighborhood, #CustomerAddressData_Neighborhood," +
        "#modelRequest_AddressData_Address, #modelRequest_AddressData_Neighborhood").change(function (e) {
            e.preventDefault();
            var $text = $(this).val();
            $text = $text.replace(/\r?\n|\r/g, ' ');
            $(this).val(RemoveSpecialChar($text));
        });

    $("#CustomerAddressData_Address").change(function (e) {
        e.preventDefault();
        var $text = $(this).val();
        $text = $text.replace(/\r?\n|\r/g, ' ');
        $text = $text.replace(/<|>|\\|\"|\'/g, '')
        $(this).val($text);
    });

    $("#CustomerAddressData_Complement, #CustomerAddressData_Reference , #Observation, #CustomerData_Name, #PaymentList_0__CardData_CustomerAddressComplement, " +
        "#PaymentList_0__CardData_CustomerAddressName, #PaymentList_0__CardData_CustomerAddressNeighborhood, #CustomerAddressData_Neighborhood," +
        "#modelRequest_AddressData_Address, #modelRequest_AddressData_Neighborhood").keypress(function (e) {
            var start = this.selectionStart;
            var end = this.selectionEnd;
            var $text = $(this).val();
            var $result = RemoveSpecialChar($text);
            $(this).val(RemoveSpecialChar($text));

            if ($result != $text)
                this.setSelectionRange(start, end - 1);
            else
                this.setSelectionRange(start, end);
        });

    $("#CustomerAddressData_Address").keypress(function (e) {
            var start = this.selectionStart;
            var end = this.selectionEnd;
            var $text = $(this).val();
            var $result = $text.replace(/<|>|\\|\"|\'/g, '');
            $(this).val($text.replace(/<|>|\\|\"|\'/g, ''));

            if ($result != $text)
                this.setSelectionRange(start, end - 1);
            else
                this.setSelectionRange(start, end);
        });
});

$.validator.addMethod("cpf", function (value, element, params) {
    return validateCpf(value);
});
$.validator.classRuleSettings.cpf = { cpf: true }
jQuery.validator.unobtrusive.adapters.add("cpf", [], function (options) {
    options.rules["cpf"] = "#" + options.element.id;
    options.messages["cpf"] = options.message;
});

$.validator.addMethod("cnpj", function (value, element, params) {
    return validateCnpj(value);
});
$.validator.classRuleSettings.cnpj = { cnpj: true }
jQuery.validator.unobtrusive.adapters.add("cnpj", [], function (options) {
    options.rules["cnpj"] = "#" + options.element.id;
    options.messages["cnpj"] = options.message;
});

$.validator.addMethod("cpforcnpj", function (value, element, params) {
    return ValidateCpfOrCnpj(value);
});
$.validator.classRuleSettings.cpforcnpj = { cpforcnpj: true }
jQuery.validator.unobtrusive.adapters.add("cpforcnpj", [], function (options) {
    options.rules["cpforcnpj"] = "#" + options.element.id;
    options.messages["cpforcnpj"] = options.message;
});

$.validator.addMethod("cardname", function (value, element, params) {
    return ValidateCardName(value);
});
$.validator.classRuleSettings.cardname = { cardname: true }
jQuery.validator.unobtrusive.adapters.add("cardname", [], function (options) {
    options.rules["cardname"] = "#" + options.element.id;
    options.messages["cardname"] = options.message;
});

function ValidateCardName(value) {
    var paymentInfo = $("#splitPaymentRadios input[type=radio]:checked").parent().data("data-info");

    if (paymentInfo.Online && (value == null || value.trim() == "" || !/^[a-zA-Z ]*$/g.test(value))) {
        return false;
    }

    return true;
}

function RemoveSpecialChar(input) {
    return input.replace(/<|>|\/|\\|\"|\'/g, '');
}

function validateCpf(cpf) {
    if (cpf == '') {
        return true;
    }

    cpf = cpf.replace(/[^\d]+/g, '');

    if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" ||
    cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" ||
    cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999") {
        return false;
    }

    add = 0;

    for (i = 0; i < 9; i++) { add += parseInt(cpf.charAt(i)) * (10 - i); }

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11) {
        rev = 0;
    }

    if (rev != parseInt(cpf.charAt(9))) {
        return false;
    }

    add = 0;

    for (i = 0; i < 10; i++) { add += parseInt(cpf.charAt(i)) * (11 - i); }

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11) {
        rev = 0;
    }

    if (rev != parseInt(cpf.charAt(10))) {
        return false;
    }

    return true;
}

function validateCnpj(cnpj) {
    cnpj = cnpj.replace(/[^\d]+/g, '');

    if (cnpj == '') return false;

    if (cnpj.length != 14)
        return false;

    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;

    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0, tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;

    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }

    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

    if (resultado != digitos.charAt(0))
        return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;

    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }

    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

    if (resultado != digitos.charAt(1))
        return false;

    return true;
}

function ValidateCpfOrCnpj(value) {
    return validateCpf(value) || validateCnpj(value);
}

function showAjaxErrorMessage(message) {
    showMessageModal(message, "messageTextDiv", "alertMessageModal", true);
}

function showMessage(message) {
    showMessageModal(message, "messageTextDiv", "alertMessageModal");
}

function showMessageModal(message, textDivId, modalDivId, isAjaxErrorMessage) {
    var $div = $("#" + textDivId).first();

    $div.html("");

    var $h4 = $("<h4 class=\"text-center\"></h4>");
    $h4.html(message.replace(/(^"|"$)/g, ''));
    $div.append($h4);

    if (isAjaxErrorMessage && !$('#' + modalDivId).hasClass("ajaxErrorMessage")) {
        $('#' + modalDivId).addClass("ajaxErrorMessage");
    }

    $('#' + modalDivId).addClass("exactCenter");
    $('.alertbg').show();
    $("#" + modalDivId).show();
}

function showConfirmationMessageToDeleteDish(message, id, idSalesPromotionDoubleOtherDish) {
    $('#confirmConfirmationModal').attr('data-id', id);

    showMessageModal(message, "confirmationMessageTextDiv", "confirmationMessageModalToDeleteDish");
}

function showConfirmationMessageToIncludeOrderDeliveryFee(message, id) {
    $('#confirmYesModal').attr('data-id', id);

    showMessageModal(message, "YesNoMessageTextDiv", "confirmationMessageToIncludeOrderDeliveryFee");
}

function hideMessage(e) {
    hideMessageModal(e, 'alertMessageModal');
}

function hideConfirmationMessage(e) {
    hideMessageModal(e, 'confirmationMessageModalToDeleteDish');
}

function hideYesNoMessage(e) {
    hideMessageModal(e, 'confirmationMessageToIncludeOrderDeliveryFee');
}

function hideMessageModal(e, modalDivId) {
    e.preventDefault();

    $("#" + modalDivId).hide();
    $(".alertbg").hide();

    if ($('#' + modalDivId).hasClass("ajaxErrorMessage")) {
        $('#' + modalDivId).removeClass("ajaxErrorMessage");
        window.location.href = $("#applicationPath").val();
    }
}

function createCartDishHtml(dish) {
    var cartDishesInfoHtml = "";
    cartDishesInfoHtml += '<div class="row">';
    cartDishesInfoHtml += '       <div class="large-2 medium-2 small-3 hide-for-small columns">';

    if (dish.FractionItemList != null && dish.FractionItemList.length > 0) {
        cartDishesInfoHtml += '       <img src="' + $("#applicationPath").val() + '/Images/pizza-meio-a-meio.png" alt="" class="left hide-for-smallest" style="padding:0px 10px 20px 0px" />';
    } else {
        if (dish.ImageAddress != null) {
            cartDishesInfoHtml += '       <img src="' + dish.ImageAddress + '" alt="" class="left hide-for-smallest" style="padding:0px 10px 20px 0px" />';
        } else {
            cartDishesInfoHtml += '       <img src="' + $("#applicationPath").val() + '/Images/PH_logo_telhado.png" alt="" class="left hide-for-smallest" style="padding:0px 10px 20px 0px" />';
        }
    }

    cartDishesInfoHtml += '       </div>';
    cartDishesInfoHtml += '       <div class="large-6 medium-6 small-9 small-12 columns" style="margin:0;">';
    cartDishesInfoHtml += '           <p style="margin:0px; line-height:1em;">';
    cartDishesInfoHtml += '               <strong>' + dish.Name + '</strong>';
    cartDishesInfoHtml += '               <br />';
    cartDishesInfoHtml += '               <br />';
    cartDishesInfoHtml += '               <span style="font-size:0.7em;line-height:1.2em;display:block;">';

    if (dish.MenuDishOptionalList != null) {
        $.each(dish.MenuDishOptionalList, function (i, value) {
            cartDishesInfoHtml += i > 0 ? " - " : "";
            $.each(value.MenuDishOptionalItemList, function (i2, optional) {
                cartDishesInfoHtml += i2 == 0 ? value.Name + ": " : "";
                cartDishesInfoHtml += i2 > 0 ? ", " + optional.Name : optional.Name;
            });
        });
    }

    if (dish.FractionItemList != null) {
        $.each(dish.FractionItemList, function (i, fractionList) {
            $.each(fractionList.MenuDishOptionalList, function (i2, optionalList) {
                cartDishesInfoHtml += " - " + optionalList.Name + ": ";
                $.each(optionalList.MenuDishOptionalItemList, function (i3, optional) {
                    cartDishesInfoHtml += i3 > 0 ? ", " + optional.Name : optional.Name;
                });
            });
        });
    }
    cartDishesInfoHtml += '               </span>';
    cartDishesInfoHtml += '           </p>';
    cartDishesInfoHtml += '           <br />';
    cartDishesInfoHtml += '       </div>';
    cartDishesInfoHtml += '       <div class="large-2 medium-2 small-4 small-5 columns">';
    cartDishesInfoHtml += '           <div class="quantity-set">';

    if (!dish.IsGift) {
        cartDishesInfoHtml += '               <a class="numeral"><i class="fi-minus"></i></a>';
    }

    cartDishesInfoHtml += '               <input type="text" readonly value="' + dish.Quantity + '" />';

    if (!dish.IsGift) {
        cartDishesInfoHtml += '               <a class="numeral"><i class="fi-plus"></i></a>';
    }

    cartDishesInfoHtml += '           </div>';
    cartDishesInfoHtml += '           <span class="delete-link" data-id="' + dish.IdCartMenuDish + '"><a href="#" class="">Remover</a></span>';
    cartDishesInfoHtml += '           <br />';
    cartDishesInfoHtml += '           <br />';
    cartDishesInfoHtml += '       </div>';
    cartDishesInfoHtml += '       <div class="large-2 medium-2 small-4 small-7 columns price">';
    cartDishesInfoHtml += '           <h5>R$ <span data-default-price="' + dish.TotalValue + '">' + dish.TotalValue.toFixed(2).toString().replace('.', ',') + '</span></h5>';
    cartDishesInfoHtml += '       </div>';
    cartDishesInfoHtml += '       <br />';
    cartDishesInfoHtml += '       <hr />';
    cartDishesInfoHtml += '   </div>';

    return cartDishesInfoHtml;
}

function refreshCartMenuDishList(cartMenuDishList) {
    var cartDishesInfoHtml = "";
    $.each(cartMenuDishList, function (index, dish) {
        console.log(dish);

        cartDishesInfoHtml += createCartDishHtml(dish);
    });
    $("#divCartDishInfo").html(cartDishesInfoHtml);
}

function refreshCartDishes(result) {
    var cartMenuDishList = result.CartData.CartData.CartMenuDishList;
    refreshCartMenuDishList(cartMenuDishList);
}

function refreshSellingOutDishesSalesPromotion(cartMenuSellingOutDishList, CartMenuSalesPromotionList) {
    var sellingOutDishesInfoHtml = "";
    var salesPromotionInfoHtml = "";
    $.each(cartMenuSellingOutDishList, function (index, value) {
        sellingOutDishesInfoHtml += '<form action="' + $("#applicationPath").val() + '/Cart/AddCartMenuDish" class="menuDishForm" method="post" novalidate="novalidate">';
        sellingOutDishesInfoHtml += '   <input type="hidden" name="requestModel.IsSellingOut" value="true" />';
        sellingOutDishesInfoHtml += '   <div class="sellingOutDetails">';
        sellingOutDishesInfoHtml += '       <div class="row">';
        sellingOutDishesInfoHtml += '           <div class="large-3 tabletUp-4 mobileWide-3 columns">';
        sellingOutDishesInfoHtml += '               <img src="' + value.ImageAddress + '" alt="">';
        sellingOutDishesInfoHtml += '           </div>';
        sellingOutDishesInfoHtml += '           <div class="large-9 tabletUp-8 mobileWide-9 small-12 columns" style="margin:0;">';
        sellingOutDishesInfoHtml += '               <h6 class="sideName right">' + value.Name + '</h6>';
        sellingOutDishesInfoHtml += '               <h5 class="right">R$ <span class="uniquePrice">' + value.UnitValue.toFixed(2).toString().replace('.', ',') + '</span></h5>';
        sellingOutDishesInfoHtml += '           </div>';
        sellingOutDishesInfoHtml += '       </div>';
        sellingOutDishesInfoHtml += '       <div class="row fullWidthButtons">';
        sellingOutDishesInfoHtml += '           <div class="small-7 large-7 columns">';
        sellingOutDishesInfoHtml += '               <a href="#" class="button plainButton small addSellingOutButton" data-id="' + value.IdSellingOutGroup + '">Adicionar</a>';
        sellingOutDishesInfoHtml += '               <input id="requestModel_CartMenuDish_IdMenuDish" name="requestModel.CartMenuDish.IdMenuDish" type="hidden" value="' + value.IdMenuDish + '">';
        sellingOutDishesInfoHtml += '           </div>';
        sellingOutDishesInfoHtml += index < cartMenuSellingOutDishList.length - 1 ? '<hr />' : '';
        sellingOutDishesInfoHtml += '       </div>';
        sellingOutDishesInfoHtml += '    </div>';
        sellingOutDishesInfoHtml += '</form>';
    });

    $.each(CartMenuSalesPromotionList, function (index, value) {
        salesPromotionInfoHtml += '<form action="' + $("#applicationPath").val() + '/Cart/AddCartMenuDish" class="menuDishForm" method="post" novalidate="novalidate">';
        salesPromotionInfoHtml += '   <input type="hidden" name="requestModel.IsSellingOut" value="true" />';
        salesPromotionInfoHtml += '   <input type="hidden" name="requestModel.CartMenuDish.IdSalesPromotion" value="' + value.IdSalesPromotion.toString() + '" />'
        salesPromotionInfoHtml += '   <div class="sellingOutDetails">';
        salesPromotionInfoHtml += '       <div class="row">';
        salesPromotionInfoHtml += '           <div class="large-3 tabletUp-4 mobileWide-3 columns">';
        salesPromotionInfoHtml += '               <img src="' + value.ImageAddress + '" alt="">';
        salesPromotionInfoHtml += '           </div>';
        salesPromotionInfoHtml += '           <div class="large-9 tabletUp-8 mobileWide-9 small-12 columns" style="margin:0;">';
        salesPromotionInfoHtml += '               <h6 class="sideName right">' + value.Name + '</h6>';
        salesPromotionInfoHtml += '               <h5 class="right">R$ <span class="uniquePrice">' + value.UnitValue.toFixed(2).toString().replace('.', ',') + '</span></h5>';
        salesPromotionInfoHtml += '           </div>';
        salesPromotionInfoHtml += '       </div>';
        salesPromotionInfoHtml += '       <div class="row fullWidthButtons">';
        salesPromotionInfoHtml += '           <div class="small-7 large-7 columns">';
        salesPromotionInfoHtml += '               <a href="#" class="button plainButton small addSellingOutButton" data-id="' + value.IdSalesPromotion + '">Adicionar</a>';
        salesPromotionInfoHtml += '               <input id="requestModel_CartMenuDish_IdMenuDish" name="requestModel.CartMenuDish.IdMenuDish" type="hidden" value="' + value.IdMenuDish + '">';
        salesPromotionInfoHtml += '           </div>';
        salesPromotionInfoHtml += index < CartMenuSalesPromotionList.length - 1 ? '<hr />' : '';
        salesPromotionInfoHtml += '       </div>';
        salesPromotionInfoHtml += '    </div>';
        salesPromotionInfoHtml += '</form>';
    });

    var html = "";
    $('#cartDiv').removeClass();

    if (sellingOutDishesInfoHtml != "" || salesPromotionInfoHtml != "") {
        html += '<div class="large-3 medium-12 small-12 columns panel sideBox">';
        html += '   <h6>Adicionar acompanhamentos?</h6>';
        html += salesPromotionInfoHtml;
        html += sellingOutDishesInfoHtml;
        html += '</div>';

        $('#cartDiv').addClass("large-9 medium-12 small-12 columns");
    } else {
        $('#cartDiv').addClass("large-12 medium-12 small-12 columns");
    }

    $("#divSellingOut").html(html);
}

function RefreshAllOptionalSelect() {
    $("select[data-optionalrequired=1]").each(function (key, value) {
        $(this).prop('selectedIndex', 1);
        $(this).change();
    });
}

function RefreshOptionalSelect($form) {
    $form.find("select[data-optionalrequired=1]").each(function (event, key, value) {
        $(this).prop('selectedIndex', 1);

        menuDishOptionalChange(event, value);
    });
}

function fillRequestMenuDishFractionDish(form, idMenuDish, count) {

    form.append("<input name=\"requestModel.CartMenuDish.FractionItemList[" + count + "].IdMenuDishFractionDish\" value=\"" + idMenuDish + "\" />");
}

function fillRequestMenuDishOptional(form, mainDiv, count) {

    $(mainDiv).find('select').each(function (key, val) {

        var idMenuDishOptional = $(this).data('idmenudishoptional');
        var idMenuDishOptionalItem = $(this).val() > 0 ? $(this).val() : 0;

        form.append("<input name=\"requestModel.CartMenuDish.FractionItemList[" + count + "].MenuDishOptionalList[" + key + "].IdMenuDishOptional\" value=\"" + idMenuDishOptional + "\" />");

        if (idMenuDishOptionalItem <= 0) {
            return;
        }
        else {
            form.append("<input name=\"requestModel.CartMenuDish.FractionItemList[" + count + "].MenuDishOptionalList[" + key + "].MenuDishOptionalItemList[0].IdMenuDishOptionalItem\" value=\"" + idMenuDishOptionalItem + "\" />");
        }
    });

    $(mainDiv).find('input:checkbox:checked ').each(function (key, val) {
        if (!this.value) {
            return;
        }

        var idMenuDishOptional = $(this).data('idmenudishoptional');
        var idMenuDishOptionalItem = $(this).val();
        var countOptional = form.find('select').length;

        form.append("<input name=\"requestModel.CartMenuDish.FractionItemList[" + count + "].MenuDishOptionalList[" + countOptional + "].IdMenuDishOptional\" value=\"" + idMenuDishOptional + "\" />");
        form.append("<input name=\"requestModel.CartMenuDish.FractionItemList[" + count + "].MenuDishOptionalList[" + countOptional + "].MenuDishOptionalItemList[" + key + "].IdMenuDishOptionalItem\" value=\"" + idMenuDishOptionalItem + "\" />");
    });
}

function resetProductCard($productCard) {
    $productCard.find('select').prop('selectedIndex', 0)

    var dt = Number($productCard.find(".price > span").data("default-price"));

    $productCard.find(".price > span").text(dt.toFixed(2).replace('.', ','));
    $productCard.find("div.row").find(".extrasModal").find(".row").find("input").attr("checked", false)
    $productCard.find("div.row").find(".extrasModal").find(".row").find("input").removeAttr("checked", false)
    $productCard.find("div.row").find(".extrasModal").find(".row").find("input").prop("checked", false)
    $productCard.find("div.row").find(".extrasModal").find(".row").find("input").removeClass("checked")
    $productCard.find("div.row").find(".extrasModal").find(".row").find("input").removeClass("added")
    $productCard.find('a.extras-btn').removeClass('selected');
    $productCard.find('a.extras-btn > span:nth-child(1)').removeClass("glyphicon-ok-sign");
    $productCard.find('a.extras-btn > span:nth-child(1)').addClass("glyphicon-ok-circle");
}

function disableAddToOrderBtn($productCard) {
    var $addToOrderBtn = $productCard.find('.addToOrderButton');

    var needToEnableAddToOrderBtn = false;

    if ($addToOrderBtn.hasClass('btn-disabled') == false) {
        $addToOrderBtn.addClass('btn-disabled');

        needToEnableAddToOrderBtn = true;
    }

    return needToEnableAddToOrderBtn;
}

function disableHalfPizzaBtn($productCard) {
    var needToEnableHalfPizzaBtn = false;

    var $halfPizzaBtn = $productCard.find('.pizza-half-btn');

    if ($halfPizzaBtn.hasClass('btn-disabled') == false) {
        $halfPizzaBtn.addClass('btn-disabled');

        needToEnableHalfPizzaBtn = true;
    }

    return needToEnableHalfPizzaBtn;
}

function checkIsCalculatingPrice($productCard) {
    var $price = $productCard.find('.price');

    return $price.hasClass('calculatingPrice');
}

function calculateMenuDishTotalValue(form, isFirstTime) {
    var $productCard = $(form).closest('.productCard');
    var $price = $productCard.find('.price');

    if (checkIsCalculatingPrice($productCard) == true) {
        return;
    }

    if (typeof isFirstTime === 'undefined') {
        isFirstTime = false;
    }

    var previousValue = $price.find('span').text();
    var priceSpinnerHtml = '<img src="' + $("#applicationPath").val() + '/Images/loading.gif" width="39" height="43" alt="" style="margin-top:-10px;" class="price_spinner" />';

    $price.html(priceSpinnerHtml);
    $price.addClass('calculatingPrice');

    var needToEnableAddToOrderBtn = disableAddToOrderBtn($productCard);
    var needToEnableHalfPizzaBtn = disableHalfPizzaBtn($productCard);
    var $select = $productCard.find('select');
    var previousState = {};

    $select.each(function (index, value) {
        previousState[$(value).attr('id')] = $(value).attr('disabled');
    });

    $select.prop('disabled', true);
    
    var $dform = $(form).clone();

    $dform.find('select').removeAttr('disabled');

    prepareForm($(form), $dform);


    $.get($("#applicationPath").val() + '/MenuAsync/CalculateDishPrice', $dform.serialize(), function (result) {

        var $productCard = $(form).closest('.productCard');

        var $price = $productCard.find('.price');
        var priceSpanHtml;

        if (result.Validated) {
            priceSpanHtml = 'R$ <span data-default-price="' + result.Price + '">' + parseFloat(result.Price).toFixed(2).replace('.', ',') + '</span>';
        } else if (previousValue != '' && previousValue != undefined) {
            showMessage(result.ValidatedMessage);
            priceSpanHtml = 'R$ <span data-default-price="' + previousValue.replace(',', '.') + '">' + previousValue + '</span>';
        }

        $price.html(priceSpanHtml);

        enableControls($productCard, previousState, needToEnableAddToOrderBtn, needToEnableHalfPizzaBtn);

        $price.removeClass('calculatingPrice');
    });
}

function enableControls($productCard, previousState, needToEnableAddToOrderBtn, needToEnableHalfPizzaBtn) {
    var $addToOrderBtn = $productCard.find('.addToOrderButton');
    var $halfPizzaBtn = $productCard.find('.pizza-half-btn');
    var $select = $productCard.find('select');

    $select.each(function (index, value) {
        if (previousState[$(value).attr('id')] == '' || previousState[$(value).attr('id')] == undefined) {
            $select.prop('disabled', false);
        }
    });
    if (needToEnableAddToOrderBtn) {
        $addToOrderBtn.removeClass('btn-disabled');

        needToEnableAddToOrderBtn = false;
    }

    if (needToEnableHalfPizzaBtn) {
        $halfPizzaBtn.removeClass('btn-disabled');

        needToEnableHalfPizzaBtn = false;
    }
}

function refreshPrices(dishList) {
    $.each(dishList, function (index, value) {
        $form = $('input[name="requestModel.CartMenuDish.IdMenuDish"][value="' + value + '"]').closest('form');

        calculateMenuDishTotalValue($form)
    })
}

function prepareForm($form, $dform) {
    $.each($form.find('select'), function (key, value) {
        var $select = $dform.find('#' + $(value).attr('id'));

        if ($(value).val()) {
            $select.val($(value).val());
        } else {
            $select.detach();
        }
    });

    var idMenuDish = $dform.find('#requestModel_CartMenuDish_IdMenuDish').val();

    var $extraModal = $dform.find('#extrasModal_' + idMenuDish);

    if ($extraModal.length == 0) {
        var $externalExtraModal = $('#extrasModal_' + idMenuDish);

        var $externalExtraModalCheckboxes = $externalExtraModal.find('input:checkbox');

        $dform.append($externalExtraModalCheckboxes.clone());
    }

    var $checkboxes = $dform.find('input:checkbox:not(:checked)');

    if ($checkboxes.length > 0) {
        $checkboxes.detach();
    }

    var isFraction = ($dform.find('input[name*="FractionItemList"]').length > 0);

    if (isFraction) {
        $.each($dform.find('input[name*="FractionItemList[0]"]:checkbox'), function (key, value) {
            $(value).attr('name', $(value).attr('name').replace(/\d{1,2}\].Id/g, key + '].Id'));
        });

        $.each($dform.find('input[name*="FractionItemList[1]"]:checkbox'), function (key, value) {
            $(value).attr('name', $(value).attr('name').replace(/\d{1,2}\].Id/g, key + '].Id'));
        });
    }
    else {
        $.each($dform.find('input:checkbox'), function (key, value) {
            $(value).attr('name', $(value).attr('name').replace(/\d{1,2}\].Id/g, key + '].Id'));
        });
    }
}