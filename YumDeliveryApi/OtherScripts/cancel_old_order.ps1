param([string]$url)
$auth = "Basic cmVjZXB0b3I6c2VuaGE="

$headers = @{ Authorization = $auth }

Invoke-WebRequest -Uri $url -Method POST -Headers $headers
