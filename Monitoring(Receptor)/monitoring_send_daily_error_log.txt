

StatusCode        : 200
StatusDescription : OK
Content           : {}
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Pragma: no-cache
                    Access-Control-Allow-Origin: *
                    Access-Control-Allow-Headers: Accept, Accept-End, Authorization, Content-Type
                    Access-Control-Allow-Methods: ...
Forms             : {}
Headers           : {[Connection, keep-alive], [Pragma, no-cache], [Access-Control-Allow-Origin, *], 
                    [Access-Control-Allow-Headers, Accept, Accept-End, Authorization, Content-Type]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : System.__ComObject
RawContentLength  : 2



