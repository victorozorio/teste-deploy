$auth = "Basic cmVjZXB0b3I6c2VuaGE="

$headers = @{ Authorization = $auth }

$return = Invoke-WebRequest -Uri "http://monitoringapi.pizzahut.com.br/api/OrderReceiver/CancelOldOrder" -Method POST -Headers $headers

$return | Out-File -FilePath D:\Monitoring\cancel_old_order_log.txt
