$auth = "Basic d2luZG93c19zZXJ2aWNlOnNlbmhh"

$headers = @{ Authorization = $auth }

$return = Invoke-WebRequest -Uri "http://monitoringapi.pizzahut.com.br/api/Monitoring/SendDailySummary" -Method POST -Headers $headers

$return | Out-File -FilePath D:\Monitoring\monitoring_send_daily_summary.txt
