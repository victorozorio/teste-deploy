#Declara os arrays com o nome dos diret�rios.
$dirYumDeliveryBackup = "C:\YumDeliveryBackup"
$dirYumDeliveryRoot = "C:\WWW_ROOT"
$dirYumDeliveryTemp = "C:\YumDeliveryTemp"

$arrayDirYumDelivery = @("C:\WWW_ROOT\YumDeliveryApi", "C:\WWW_ROOT\YumDeliveryBackOffice", "C:\WWW_ROOT\YumDeliveryCallCenter", "C:\WWW_ROOT\YumDeliveryFront", "C:\WWW_ROOT\YumDeliveryMonitoringApi")
$arrayDirYumDeliveryTemp = @("C:\YumDeliveryTemp\YumDeliveryApi", "C:\YumDeliveryTemp\YumDeliveryBackOffice", "C:\YumDeliveryTemp\YumDeliveryCallCenter", "C:\YumDeliveryTemp\YumDeliveryFront", "C:\YumDeliveryTemp\YumDeliveryMonitoringApi");
$arrayDirYumDeliveryName = @("YumDeliveryApi.zip", "YumDeliveryBackOffice.zip", "YumDeliveryCallCenter.zip", "YumDeliveryFront.zip", "YumDeliveryMonitoringApi.zip")

#Lembrando que estou usando o tamanho de um array. Isso d� certo porque todos possuem 5 posi��es, caso contr�rio, deveria ser colocado o n�mero.
for ($i=0; $i -lt $arrayDirYumDelivery.length; $i++) {
    #Verifica se o diret�rio existe.
    if(Test-Path -Path $arrayDirYumDelivery[$i] ){
	    #Atribui a vari�vel archivefile o nome do diret�rio e nome do arquivo .zip
	    $archiveFile = Join-Path -Path $dirYumDeliveryBackup -ChildPath $arrayDirYumDeliveryName[$i]
   
	    #Cria o diret�rio para armazenar o arquivo .zip. Se ele j� existir, uma mensagem de erro aparece, mas eu especifico que n�o quero ver nenhum erro usando o comando EA 0 . Se a pasta n�o existir, o comando retorna um objeto DirectoryInfo , mas tamb�m n�o quero ver isso, ent�o eu adicionei o comando Out-Null. 
	    MD $dirYumDeliveryBackup -EA 0 | Out-Null
	
	    #É possível que o arquivo .zip j� exista. Caso essa condi��o seja verdadeira, eu apago o arquivo.
	    if(Test-path $archiveFile) {Remove-item $archiveFile}
	
	    #Adiciona o assembly que cont�m a classe Zipfile .NET Framework e chama o m�todo CreateFromDirectory para criar o arquivo .zip.
	    Add-Type -assembly "system.io.compression.filesystem"
	    [io.compression.zipfile]::CreateFromDirectory($arrayDirYumDelivery[$i], $archiveFile)
		
		if(Test-Path -Path $arrayDirYumDeliveryTemp[$i] ){
            #Copia os arquivos do diret�rio tempor�rio para o Root
            Copy-Item -Path $arrayDirYumDeliveryTemp[$i] -Destination $dirYumDeliveryRoot -Recurse -Force
        }	
    }
}

Remove-Item $dirYumDeliveryTemp -Recurse


    

