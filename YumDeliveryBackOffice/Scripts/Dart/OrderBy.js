﻿$(function () {
    $(".orderByLink").click(function (e) {
        e.preventDefault();

        var order = $(this).attr("rel").split(";");
        if ($("#orderByFilter").val() == order[0]) {
            if ($("#directionByFilter").val() == "ASC") {
                $("#directionByFilter").val("DESC");
            } else {
                $("#directionByFilter").val("ASC");
            }
        }
        else {
            $("#orderByFilter").val(order[0]);
            $("#directionByFilter").val("ASC");

            if (order.length > 1)
                $("#thenByFilter").val(order[1]);
            else if ($("#thenByFilter").length > 0)
                $("#thenByFilter").val("");
        }

        if (order.length > 2) {
            $("#" + order[2]).submit();
        } else {
            $("form").submit();
        }
    });

    verificaDirection();
});

function verificaDirection() {
    var rel = $("#orderByFilter").val();
    var links = $("a.orderByLink:first").attr("rel").split(";");

    if ($("#thenByFilter").length > 0) {
        if ($("#thenByFilter").val() != "") {
            rel += ";" + $("#thenByFilter").val();

            if (links.length > 2) {
                rel += ";" + links[2];
            }
        }
    }
    else if (links.length > 2) {
        rel += ";'';" + links[2];
    }

    if ($("#directionByFilter").val() == "ASC") {
        $("a.orderByLink[rel = '" + rel + "']").addClass("down-link")
    } else {
        $("a.orderByLink[rel = '" + rel + "']").addClass("up-link")
    }
}