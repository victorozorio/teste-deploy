﻿$(function () {
    showHideFilter();

    $("fieldset.filterShadow h4.cursorPointer").click(function (e) {
        e.preventDefault();

        $('.filterShadow ol').slideToggle('slow');
        var str = $(".filter h4 a").attr('class');
        if (str.indexOf("open") >= 0)
            $(".filterShadow h4 a").attr('class', "close-link")
        else
            $(".filterShadow h4 a").attr('class', "open-link")
    });

});

function showHideFilter(menu) {
    var validateFilter = false;
    $(".filterShadow ol li").find(":input").each(function () {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                if ($(this).val() != '' && $(this).val() != null) {
                    validateFilter = true;
                    return;
                }
                break;
            case 'checkbox':
            case 'radio':
                if (this.checked == true) {
                    validateFilter = true;
                    return;
                }
                break;
        }
    });

    if (!validateFilter) {
        $('.filterShadow ol').hide();
    } else {
        $(".filterShadow h4 a").attr('class', "close-link")
    }
};