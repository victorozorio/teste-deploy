﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        showLoadingOverlay();
    });

    $(document).ajaxStop(function () {
        hideLoadingOverlay();
    });

    $(document).ajaxError(function () {
        hideLoadingOverlay();
    });
    
    $(document).ajaxComplete(function () {
        hideLoadingOverlay();
    });

    $(document).ajaxSuccess(function () {
        hideLoadingOverlay();
    });

    window.onerror = function () {
        hideLoadingOverlay();
    };

    window.onended = function () {
        hideLoadingOverlay();
    };
    
    $(window).load(function () {
        hideLoadingOverlay();
    });

    $(window).error(function () {
        hideLoadingOverlay();
    });

    $("form").on("submit", function () {
        var validator = $(this).validate();
        if ($(this).valid()) {
            showLoadingOverlay();
        }
    });
});

function showLoadingOverlay() {
    if (!$("body").hasClass("disableLoadingOverlay")) {
        $("#loadingOverlay").show();
        $("html, html a, html label").attr("style", "cursor: progress !important");
    }
}

function hideLoadingOverlay() {
    $("html, html a, html label").attr("style", "cursor: ");
    $("#loadingOverlay").hide();
}

function disableLoadingOverlay() {
    $("body").addClass("disableLoadingOverlay");
}

function enableLoadingOverlay() {
    $("body").removeClass("disableLoadingOverlay");
}