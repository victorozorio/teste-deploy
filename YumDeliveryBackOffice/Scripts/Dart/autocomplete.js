﻿$(function () {
    $(":input[data-autocomplete]").each(function () {
        var input = $(this);
        input.autocomplete({
            source: $("#applicationPath").val() + "/admin/" + input.attr('data-autocomplete') + "/AutoComplete",
            minLength: 3,
            delay: 0,
            mustMatch: true,
            scrollHeight: 200,
            select: function (event, ui) {
                event.preventDefault();
                $(input).val(ui.item.label);
//              $(input).attr("readonly", "readonly");
                $("#" + input.attr('data-id')).val(ui.item.value);
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(input).val(ui.item.label);
            }
        });

        input.blur(function () {
            if ($("#" + input.attr('data-id')).val() == 0 || $("#" + input.attr('data-id')).val() == "" || $("#" + input.attr('data-id')).val() == undefined) {
                $("#" + input.attr('data-id')).val("");
                $(input).val("");
            }
        });
    });
});
