﻿$(function () {
    showHideFilter();

    $("fieldset.filter h4.cursorPointer").click(function (e) {
        e.preventDefault();
        $('.filter ol').slideToggle('slow');
        var str = $(".filter h4 a").attr('class');
        $('.filter h4 a').toggleClass("close-link");
        $('.filter h4 a').toggleClass("open-link");
    });

});

function showHideFilter(menu) {
    if ($(".filter h4 a").length > 0) {
        var validateFilter = false;
        $(".filter ol li").find(":input").each(function () {
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'textarea':
                    if ($(this).val() != '' && $(this).val() != null) {
                        validateFilter = true;
                        return;
                    }
                    break;
                case 'checkbox':
                case 'radio':
                    if (this.checked == true) {
                        validateFilter = true;
                        return;
                    }
                    break;
            }
        });
        if (!validateFilter) {
            $('.filter ol').hide();
        } else {
            $(".filter h4 a").attr('class', "close-link");
        }
    }
};