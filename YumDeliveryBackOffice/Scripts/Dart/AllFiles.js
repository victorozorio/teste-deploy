﻿$(function () {
    $(".date").datepicker().bind("change", function () { $("form").validate().element("#" + $(this).attr("id")); });
    $(".numerico").numeric({ decimal: false, negative: false });
    $(".porcent").numeric({ decimal: true, negative: false });
    $(".telefone").mask("(99)9999-9999");
    $(".datetime").datetimepicker({ timeFormat: 'HH:mm:ss' }).bind("change", function () { $("form").validate().element("#" + $(this).attr("id")); });
    $(".datetimenosec").datetimepicker({ showSecond: false, timeFormat: 'HH:mm' }).bind("change", function () { $("form").validate().element("#" + $(this).attr("id")); });
    $(".valor").priceFormat({ prefix: '', thousandsSeparator: '.', centsSeparator: ',' });
    $(".valorComNegativo").priceFormat({ allowNegative: true, prefix: '', thousandsSeparator: '.', centsSeparator: ',' });
    $(".date").mask("00/00/0000", { clearIfNotMatch: true });
    $(".monthYear").mask("00/0000", { clearIfNotMatch: true });
    $(".CPF").mask("000.000.000-00", { clearIfNotMatch: true });
    $(".CNPJ").mask("00.000.000/0000-00", { clearIfNotMatch: true });
    $(".CEP").mask("00000-000", { clearIfNotMatch: true });
    $(".placa").mask("SSS-0000", { clearIfNotMatch: true });
    $('.interval-value').mask('#.##0,00', { reverse: true });
    $('.numeric').mask('00000000000000000000', { reverse: true });
    $(".tempo").mask("99:99", { clearIfNotMatch: true });

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
        clearIfNotMatch: true,
        onKeyPress: function (val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };
    $('.phone').mask(SPMaskBehavior, spOptions);

    $(".noPost").click(function (e) {
        e.preventDefault();
    });

    $(".newDependencyEntityLink").click(function (e) {
        e.preventDefault();
        rels = $(this).attr("rel").split(";");

        $.get($(this).attr("href"), function (result) {
            $("#" + rels[0]).html(result);
            $("#" + rels[0]).dialog({
                height: 550, width: 550, modal: true,
                close: function () {

                    if (rels.length > 3) {
                        $.get($("#applicationPath").val() + "/" + rels[2] + "/" + rels[2] + "LOV", { filter: $("#" + rels[3]).val() }, function (result) {
                            $("#" + rels[1]).html(result);
                        });
                    }
                    else {
                        $.get($("#applicationPath").val() + "/" + rels[2] + "/" + rels[2] + "LOV", function (result) {
                            $("#" + rels[1]).html(result);
                        });
                    }
                }
            });
        });
    });

     $(".newAutoCompleteDependencyLink").click(function (e) {
        e.preventDefault();
        div = $(this).attr("rel");

        $.get($(this).attr("href"), function (result) {
            $("#" + div).html(result);
            $("#" + div).dialog({
                height: 550, width: 550, modal: true
            });
        });
    });
    
    Globalize.culture = Globalize.cultures['pt-BR'];
});

$.validator.methods.number = function (value, element) {

    if (value == null) {
        return true;
    }

    if (value == "" || Globalize.parseFloat(value, null, Globalize.culture)) {
        return true;
    }

    if (parseFloat(value).toString() != "NaN") {
        return true;
    }
    return false;
}

$.validator.methods.date = function (value, element) {
    if (value == "" || parseDateByFormat(value) != null) {
        return true;
    }
   
    return false;
}

function showmenu(menu) {
    document.getElementById(menu).style.display = "";
};

function hidemenu(menu) {
    document.getElementById(menu).style.display = "none";
};


function parseDateByFormat(value) {
    if (value == null || value.length < 6)
        return null;
    var culture = Globalize.culture;
    var format;
    if (value.length > 10)
        format = culture.calendar.patterns.d + " " + culture.calendar.patterns.T;
    else
        format = culture.calendar.patterns.d;
    return Globalize.parseDate(value, format, culture);
}

function formatDateToDisplay(value) {
    var culture = Globalize.culture;
    return Globalize.format(parseDateByFormat(value), culture.calendar.patterns.d, culture);
}

function replaceAll(string, token, newtoken) {
    while (string.indexOf(token) != -1) {
        string = string.replace(token, newtoken);
    }
    return string;
}

function ConverterPriceFormatToDouble(valor) {
    return replaceAll(valor, ".", "").replace(",", ".");
}

function ConverterDoubleToPriceFormat(valor) {
    return FormatarNumero(valor, 2, ",", ".");
}