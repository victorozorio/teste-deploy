﻿function sortElements(elements, sortByDataAttributes, sortType) {
    if (elements == null || sortByDataAttributes == null || !$.isArray(sortByDataAttributes) || sortByDataAttributes.length == 0) {
        return elements;
    }
    var parent = elements.parent();
    var sortDataAttr = sortByDataAttributes[0];
    var dataValues = null;
    elements.each(function () {
        var dataValue = $(this).data(sortDataAttr[0]);
        if ((dataValue != null) && (dataValues == null || $.inArray(dataValue, dataValues) < 0)) {
            if (dataValues == null) {
                dataValues = [];
            }
            switch (sortDataAttr[1]) {
                case "numeric":
                    dataValues.push([dataValue, parseFloat(dataValue)]);
                    break;
                case "date":
                    dataValues.push([dataValue, parseDateByFormat(dataValue)]);
                    break;
                default:
                    dataValues.push(dataValue);
                    break;
            }
        }
    });
    if (dataValues == null) {
        return elements;
    }
    switch (sortDataAttr[1]) {
        case "numeric":
        case "date":
            dataValues.sort(function (a, b) {
                if (a[1] > b[1])
                    return 1;
                if (a[1] < b[1])
                    return -1;
                return 0;
            });
            break;
        default:
            dataValues.sort();
            break;
    }

    switch (sortType) {
        case "toggle":
            if (parent.data("sort-type") == "asc") {
                switch (sortDataAttr[1]) {
                    case "numeric":
                    case "date":
                        dataValues.sort(function (a, b) {
                            if (b[1] > a[1])
                                return 1;
                            if (b[1] < a[1])
                                return -1;
                            return 0;
                        });
                        break;
                    default:
                        dataValues.reverse();
                        break;
                }
                parent.data("sort-type", "desc");
            }
            else {
                parent.data("sort-type", "asc");
            }
            break;
        case "desc":
            switch (sortDataAttr[1]) {
                case "numeric":
                case "date":
                    dataValues.sort(function (a, b) {
                        if (b[1] > a[1])
                            return 1;
                        if (b[1] < a[1])
                            return -1;
                        return 0;
                    });
                    break;
                default:
                    dataValues.reverse();
                    break;
            }
            break;
    }
    var nextSortByDataAttributes = null;
    if (sortByDataAttributes.length > 1) {
        nextSortByDataAttributes = sortByDataAttributes.slice(1);
    }
    for (var i = 0; i < dataValues.length; i++) {
        var filteredElements;

        if (sortDataAttr[1] == "numeric" || sortDataAttr[1] == "date") {
            filteredElements = elements.filter("[data-" + sortDataAttr[0] + "='" + dataValues[i][0] + "']");
        }
        else {
            filteredElements = elements.filter("[data-" + sortDataAttr[0] + "='" + dataValues[i] + "']");
        }

        if (nextSortByDataAttributes != null) {
            filteredElements = sortElements(filteredElements, nextSortByDataAttributes);
        }
        else {
            filteredElements.remove();
            parent.append(filteredElements);
        }
    }
}

function parseDateByFormat(value) {
    if (value == null || value.length < 6)
        return null;
    var culture = Globalize.culture();
    var format;
    if (value.length > 10)
        format = culture.calendar.patterns.d + " " + culture.calendar.patterns.T;
    else
        format = culture.calendar.patterns.d;
    return Globalize.parseDate(value, format, culture);
}