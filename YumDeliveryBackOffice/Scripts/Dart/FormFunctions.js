﻿function clearFields(idForm) {
    $("#" + idForm).find(":input").each(function () {
        if (!$(this).hasClass("NoClear")) {
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'textarea':
                case 'hidden':
                    $(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
                    break;
            }
        }
    });
}

function closeForm(idDiv) {
    $("#" + idDiv).hide();
    $("#" + idDiv).dialog('close'); 
}

function closeFormWithRefresh(idForm) {
    closeForm(idForm);
    window.location = window.location;
};

var elementChoosedCallback;
var idElementValueUpdate;
var idElementTextUpdate;

function listElementChoosed(idDiv, value, text) {
    if (idElementValueUpdate != null) {
        $("#" + idElementValueUpdate).val(value);
    }

    if (idElementTextUpdate != null) {
        $("#" + idElementTextUpdate).val(text);
    }

    if (elementChoosedCallback != undefined) {
        elementChoosedCallback(value, text);
    }

    closeForm(idDiv);
}

function submitAjaxForm(idForm, idDiv, idDropDown, lovToGet) {

    $.post($("#" + idForm).attr("action"), $("#" + idForm).serialize(), function (result) {

        if (lovToGet != null) {
            $.post(lovToGet, function (res) {
                $("#" + idDropDown).html(res);
            });
        }
    });

    closeForm(idDiv);
}

function callForm(url, idDiv, idDropDown, idToSearch, modal) {
    if (modal == null)
        modal = true;

    $.post(url, { idDropDown: idDropDown, idTosearch: idToSearch }, function (result) {
        $("#" + idDiv).html(result);
        $("#" + idDiv).dialog({ height: 550, width: 550, modal: modal });
    });
}

$.extend($.validator, {
    messages: {
        required: "Campo obrigatório.",
        email: "Email inválido",
        url: "URL inválida",
        date: "Data inválida",
        dateISO: "Data inválida (ISO).",
        number: "Campo numérico.",
        maxlength: $.validator.format("Tamanho máximo: {0} caracteres."),
        minlength: $.validator.format("Tamanho mínimo: {0} caracteres."),
        rangelength: $.validator.format("O campo deve conter entre {0} e {1} caracteres."),
        range: $.validator.format("O valor do campo deve estar entre {0} e {1}."),
        max: $.validator.format("Valor máximo: {0}."),
        min: $.validator.format("Valor mínimo: {0}.")
    }
});

function showConfirmationMessage(message, dialogTitle, typeMessage, callback) {
    $("#WarningIcon").hide();
    $("#ErrorIcon").hide();
    $("#InfoIcon").hide();

    switch (typeMessage) {
        case "error": $("#ErrorIcon").show(); break;
        case "info": $("#InfoIcon").show(); break;
        case "warning": $("#WarningIcon").show(); break;
    }

    $("#TextoMensagem").text(message);
    $("#TextoMensagem").css({ 'font-size': 15 });
    $("#Mensagem").dialog({
        title: dialogTitle,
        width: 500,
        buttons: [
                {
                    text: "Sim",
                    icons: {
                    },
                    click: function () {
                        $(this).dialog("close");
                        callback();
                    }
                },
                {
                    text: "Cancelar",
                    icons: {
                    },
                    click: function () {
                        $(this).dialog("close");
                    }
                }
        ]
    });

    $("#Mensagem").show();
}

function showMessage(message, dialogTitle, typeMessage) {
    $("#WarningIcon").hide();
    $("#ErrorIcon").hide();
    $("#InfoIcon").hide();
    switch (typeMessage) {
        case "error": $("#ErrorIcon").show(); break;
        case "info": $("#InfoIcon").show(); break;
        case "warning": $("#WarningIcon").show(); break;
    }
    $("#TextoMensagem").text(message);
    $("#Mensagem").dialog({
        title: dialogTitle,
        width: 500,
        buttons: [
                {
                    text: "OK",
                    icons: {
                    },
                    click: function () {
                        $(this).dialog("close");
                    }
                }
        ]
    });

    $("#Mensagem").show();

}