/***********************************************************************************************
*
*Os campos de upload de imagem dever�o seguir o seguinte modelo:
*
*<input id="{Nome do Campo no Banco de Dados}" type="file" name="{Nome do Campo no Banco de Dados}" class="{Largura em Pixels}/{Altura em Pixels}" onchange="{Fun��o carregaImagem({elemento}, {evento do elemento})}" />
*
*Ex:
*
*<input id="ImagemPrincipal" type="file" name="ImagemPrincipal" class="300/250" onchange="carregaImagem(this, event)" />
*
*/

$(document).ready(function () {
    $("#imagemCrop").hide();
});

function carregaImagem(element, event) {
    if (event.target.value != "") {
        $("#imagemCrop #imagem").empty();

        readFileImage($(element));

        $("#imagemCrop").dialog({
            modal: true, height: "auto", position: { my: "left top", at: "left top", of: window }, width: "auto"
        });
    }

    return false;
}

function cortarImagem(element, event) {
    event.preventDefault();
    $.post($("#applicationPath").val() + '/admin/ImageCrop/CortarImagem', { x: parseInt($("#coordX").val()), y: parseInt($("#coordY").val()), height: parseInt($("#coordHeight").val()), width: parseInt($("#coordWidth").val()), imagem: $("#imagemCrop #imagem img:first").prop("class") });
	$(element.parentElement).dialog("close");
	alert("Imagem recortada.");
}

var jaRodou = false;
var mudouLargura = false;
var mudouAltura = false;
var realWidth = 0;
var realHeight = 0;
var imageWidth = 0;
var imageHeight = 0;

function updateCoords(c) {
    if (mudouLargura) {
        var diferencaLargura = realWidth / imageWidth;
        $('#coordX').val(c.x * diferencaLargura);
        $('#coordWidth').val(c.w * diferencaLargura);
    } else {
        $('#coordX').val(c.x);
        $('#coordWidth').val(c.w);
    }
    if (mudouAltura) {
        var diferencaAltura = realHeight / imageHeight;
        $('#coordY').val(c.y * diferencaAltura);
        $('#coordHeight').val(c.h * diferencaAltura);
    } else {
        $('#coordY').val(c.y);
        $('#coordHeight').val(c.h);
    }
}

function calculaTamanhos(image, maxWidth, maxHeight, classe) {

    if (jaRodou == false) {
        jaRodou = true;
        imageWidth = image.width;
        imageHeight = image.height;
        realWidth = image.width;
        realHeight = image.height;

        if (imageWidth > window.outerWidth - 50) {
            $(image).attr('width', window.outerWidth - 50);
            imageWidth = image.width;
            mudouLargura = true;
        }
        if (imageHeight > window.outerHeight - 200) {
            $(image).attr('height', window.outerHeight - 200);
            imageHeight = image.height;
            mudouAltura = true;
        }

        if (mudouLargura || mudouAltura) {
            if (realWidth > realHeight) {
                imageHeight = (imageWidth / realWidth) * realHeight;
                $(image).attr('height', imageHeight);
                mudouAltura = true;
            } else if (realWidth <= realHeight) {
                imageWidth = (imageHeight / realHeight) * realWidth;
                $(image).attr('width', imageWidth);
                mudouLargura = true;
            } 
        }

        $(image).appendTo('#imagemCrop #imagem');
        
        var proportionalMaxWidth;
        var proportionalMaxHeigth;
        var aspectRatioImage = imageWidth / imageHeight;
        var aspectRatioRectangle = maxWidth / maxHeight;

        if ((imageWidth > imageHeight || imageWidth == imageHeight) && (maxWidth < maxHeight || maxWidth == maxHeight)) {
            proportionalMaxHeigth = imageHeight;
            proportionalMaxWidth = imageHeight * (maxWidth / maxHeight);
        } else if ((imageWidth < imageHeight || imageWidth == imageHeight) && (maxWidth > maxHeight || maxWidth == maxHeight)) {
            proportionalMaxWidth = imageWidth;
            proportionalMaxHeigth = imageWidth / (maxWidth / maxHeight);
        } else if (imageWidth > imageHeight && aspectRatioRectangle > aspectRatioImage) {
            proportionalMaxWidth = imageWidth;
            proportionalMaxHeigth = imageWidth / (maxWidth / maxHeight);
        } else if (imageWidth < imageHeight && aspectRatioRectangle > aspectRatioImage) {
            //proportionalMaxHeigth = imageHeight;
            //proportionalMaxWidth = imageHeight * (maxWidth / maxHeight);
            proportionalMaxWidth = imageWidth;
            proportionalMaxHeigth = imageWidth / (maxWidth / maxHeight);
        } else if (imageWidth > imageHeight && aspectRatioRectangle < aspectRatioImage) {
            proportionalMaxHeigth = imageHeight;
            proportionalMaxWidth = imageHeight * (maxWidth / maxHeight);
        } else if (imageWidth < imageHeight && aspectRatioRectangle < aspectRatioImage) {
            proportionalMaxWidth = imageWidth;
            proportionalMaxHeigth = imageWidth / (maxWidth / maxHeight);
        } else if (aspectRatioRectangle == aspectRatioImage) {
            proportionalMaxWidth = imageWidth;
            proportionalMaxHeigth = imageHeight;
        }

        var x = (image.width / 2) - (proportionalMaxWidth / 2);
        var y = (image.height / 2) + (proportionalMaxHeigth / 2);
        if (aspectRatioRectangle <= aspectRatioImage)
            y = (image.height / 2) - (proportionalMaxHeigth / 2);

        $("#imagemCrop #imagem img").Jcrop({
            onSelect: updateCoords, 
            onChange: updateCoords, 
            aspectRatio: maxWidth / maxHeight,
            setSelect: [x, y, proportionalMaxWidth, proportionalMaxHeigth] 
            }
        );
    }
}

function readFileImage(element) {
        var maxWidth = parseInt(element.prop("class").split("/")[0]);
        var maxHeight = parseInt(element.prop("class").split("/")[1]);
        var classe = element[0].id;

        // verifica��o se aceita ou n�o o FileReader
        if (window.FileReader) {
            var reader = new FileReader();

            reader.onloadend = function (e) {

                // make an image and append it to the div
                image = $('<img>').attr('src', e.target.result).attr('class', classe);

                jaRodou = false;

                image.attr('onload', 'calculaTamanhos(this,' + maxWidth + ',' + maxHeight + ',' + classe + ');');

            };
            
            reader.readAsDataURL(element[0].files[0]);
        } else {
            if (element.val().split("\\")[1] == "fakepath") {
                alert("Necess�rio habilitar a op��o \"Incluir caminho do diret�rio local ao carregar arquivos em um servidor\" do seu navegador.\n" +
                    "Para habilitar a op��o v� na op��o Ferramentas > Op��es da Internet. Clique na aba Seguran�a e depois no bot�o N�vel Personalizado.\n" +
                    "Na janela que abrir procure a op��o \"Incluir caminho do diret�rio local ao carregar arquivos em um servidor\" e habilite.\n" +
                    "Confirme todas as janelas e recarregue a p�gina.");
                return;
            }

            image = $('<img>').attr('src', element.val()).attr('class', classe);
            image.attr('onload', 'calculaTamanhos(this,' + maxWidth + ',' + maxHeight + ',"' + classe + '");');
        }
    }
