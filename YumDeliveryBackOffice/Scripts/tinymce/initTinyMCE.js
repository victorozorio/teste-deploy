﻿$(function() {
    tinyMCE.init({
        // General options
        mode : "specific_textareas",
        editor_selector: "RichEditor",
        theme: "advanced",
        height: "350",
        width: "590",
        verify_html: false,
        plugins: "netadvimage,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist",

        // Theme options
        theme_advanced_buttons1: "bold,italic,|,bullist,numlist,|,forecolor,|,cut,copy,paste,pasteword,|,undo,redo,|,print,|,code,preview,fullscreen",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: false,

        content_css : $("#applicationPath").val() + "/Scripts/tinymce/content.css",
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
        font_size_style_values: "10px,12px,13px,14px,16px,18px,20px",
        onchange_callback: function (editor) {
            tinyMCE.triggerSave();
            $("#" + editor.id).valid();
        }
    });

    tinyMCE.init({
        // General options
        mode : "specific_textareas",
        editor_selector: "RichEditorCrystal",
        theme: "advanced",
        height: "350",
        width: "590",
        verify_html: false,
        plugins: "pagebreak,style,layer,save,advhr,iespell,inlinepopups,preview,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist",

        // Theme options
        theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,netadvimage,cleanup,code,|,preview,|,forecolor,backcolor,|,visualchars",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",



        // Theme options
        theme_advanced_buttons1: "bold,italic,|,bullist,numlist,|,forecolor,|,cut,copy,paste,pasteword,|,undo,redo,|code,|,print,|,code,preview,fullscreen",
        theme_advanced_buttons2: null,
        theme_advanced_buttons3: null,
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: false,

        content_css: $("#applicationPath").val() + "/Scripts/tinymce/content.css",
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
        font_size_style_values: "10px,12px,13px,14px,16px,18px,20px",
        onchange_callback: function (editor) {
            tinyMCE.triggerSave();
            $("#" + editor.id).valid();
        }
    });
});