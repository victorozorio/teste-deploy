﻿
$(document).ready(function () {

    var myRadio = $('input[name=IdDiscountType]');
    var checkedValue = myRadio.filter(':checked').val();
    var label = $("label[for = DiscountValue]");
    var input = $('input[name=TextDiscountValue]');

    checkRadioButtons();
    checkInputVal();

    myRadio.on('change', function () {
        checkedValue = myRadio.filter(':checked').val();
        input.val("");
        checkRadioButtons();
    });

    function checkRadioButtons() {

        if (checkedValue == 1)
        {
            label.text("Porcentagem Desconto");
        }
        else
        {
            label.text("Valor Final");
        }
    };

    input.keyup(function(){
        checkInputVal();
    });
    
    function checkInputVal() {

        if (parseFloat(input.val()) > 100 && checkedValue == 1) {
            input.val("100,00");
            
        }

    };

});