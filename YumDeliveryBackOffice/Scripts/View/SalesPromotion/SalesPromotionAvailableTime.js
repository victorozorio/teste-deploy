﻿

$(document).ready(function () {

    $("#rangedate-validation").hide();
    $("#weekday-validation").hide();

    var $sliderCountIndex = $("#sliderCountIndex").val() - 1;

    $("#SalesPromotionForm").submit(function (event) {
        var countHR = 0;
        $(".abertura-class").find("tr").each(function (key, value) {
            $(value).find("td").find("input:checked").each(function (key1, value1) {
                countHR++;
            })

        });

        if (countHR === 0) {
            $(".aberturaValidate").removeClass("field-validation-valid").addClass("field-validation-error").html("Dia / Horários é obrigatorio, Selecione um dia").show();
            setTimeout("$(\".aberturaValidate\").hide();", 4340)

            return false;
        }

        var startDate = new Date($('input[name$="StartDate"]').val());
        var endDate = new Date($('input[name$="EndDate"]').val());

        if (endDate < startDate) {
            $(".dateValidate").removeClass("field-validation-valid").addClass("field-validation-error").html("Período inválido.").show();
            setTimeout("$(\".dateValidate\").hide();", 4340)

            return false;
        }
    });

    loadSlider('.slider-range');

    function loadSlider(sliderName) {

        $(sliderName).slider({
            range: true,
            disabled: true,
            min: 0,
            max: 1439,
            step: 30,
            values: [0, 1439],
            slide: function (event, ui) {
                for (var i = 0; i < ui.values.length; i++) {
                    if (ui.values[i] == 1440) ui.values[i] = 1439;

                    var hours = Math.floor(ui.values[i] / 60);
                    var minutes = ui.values[i] - (hours * 60);

                    if (hours.toString().length == 1) hours = '0' + hours;
                    if (minutes.toString().length == 1) minutes = '0' + minutes;

                    var time = hours + ':' + minutes;

                    $(this).parent().children(i == 0 ? '.minValue' : '.maxValue').html(time).val(time);
                }
            },
            create: function (event, ui) {
                var $slider = $(this);

                if (!$slider.parent().children('input.minValue').val()) {
                    $slider.closest('tr').find(".minValue").html('00:00');
                    $slider.closest('tr').find(".maxValue").html('23:59');
                } else {

                    $slider.closest('tr').find(".minValue").html($slider.children('input.minValue').val());
                    $slider.closest('tr').find(".maxValue").html($slider.children('input.maxValue').val());
                }

                $slider.closest('tr').find('.week-day').click(function (event) {
                    
                    if (!$(this).is(':checked'))
                    {
                        $slider.closest('tbody').find(".slider-range").slider('disable').slider("option", "values", [0, 1439]);
                        $slider.closest('tbody').find(".week-day").attr("checked", false);
                    }
                    else
                    {
                        $slider.closest('tbody').find(".slider-range").slider('enable');
                        $slider.closest('tbody').find(".week-day").attr("checked", true);
                    }

                    $slider.closest('tbody').find('.minValue').html('00:00').val('00:00');
                    $slider.closest('tbody').find('.maxValue').html('23:59').val('23:59');
                });
            }
        });

    }


    $.each($('input.minValue'), function (key, value) {

        var $slider = $(value).siblings('.slider-range'), minValue, maxValue;

        if ($(value).val()) {
            var time = $(value).val().split(':');
            minValue = (time[1] * 1) + (time[0] * 60);
        }

        if ($(value).siblings('input.maxValue').val()) {
            var time = $(value).siblings('input.maxValue').val().split(':');
            maxValue = (time[1] * 1) + (time[0] * 60);
        }

        if (minValue >= 0 && maxValue) {
            $slider.slider('enable').slider("option", "values", [minValue, maxValue]);
        }
        if ($slider.closest('tbody').find('.week-day').is(':disabled'))
        {
            $slider.slider('disable').slider("option", "values", [minValue, maxValue]);
        }
    });

    $("tbody#abertura").on("click", ".add-abertura", function (e) {

        $sliderCountIndex++;
        $weekDay = $(this).closest('tbody').data("weekday");
        $currentItem = $(this).closest("tr");
        $currentCheckbox = $currentItem.find(".week-day");

        $clone = $currentItem.clone();

        var cloneDayLabel = $clone.find(".dayLabel");
        cloneDayLabel.hide();
        cloneDayLabel.html("");
        $clone.find("#sliderAction").removeClass("add-link add-abertura").addClass("table-delete-link remove-abertura");

        $cloneCheckbox = $clone.find(".week-day");
        $cloneCheckbox.attr('name', "ListSalesPromotionAvaiableTime[" + $sliderCountIndex + "].WeekDay");
        $cloneCheckbox.hide();

        $inputMinValue = $clone.find("input.minValue");
        $inputMinValue.attr('id', "ListSalesPromotionAvaiableTime_" + $sliderCountIndex + "__StartTime");
        $inputMinValue.attr('name', "ListSalesPromotionAvaiableTime[" + $sliderCountIndex + "].StartTime");
        $inputMinValue.attr('value', "00:00");

        $inputMaxValue = $clone.find("input.maxValue");
        $inputMaxValue.attr('id', "ListSalesPromotionAvaiableTime_" + $sliderCountIndex + "__EndTime");
        $inputMaxValue.attr('name', "ListSalesPromotionAvaiableTime[" + $sliderCountIndex + "].EndTime");
        $inputMaxValue.attr('value', "23:59");

        $cloneSlider = $clone.find(".slider-range");
        $cloneSlider.find('div').first().remove();

        loadSlider($cloneSlider);

        $clone.find(".minValue").html('00:00');
        $clone.find(".maxValue").html('23:59');

        if ($currentCheckbox.is(':checked')) {
            $cloneSlider.slider('enable');
            var x = $cloneCheckbox.find(".week-day").attr("checked", true);
        }

        $clone.show();

        $(this).closest('tbody').append($clone);

    });

    $("tbody#abertura").on("click", ".remove-abertura", function (e) {

        $(this).closest('tr').remove();

        var index = 0;
        $.each($(".promotion-time-row"), function () {

            var cellWeekday = $(this).find(".weekday-promotion");
            cellWeekday.find("input.weekdayselection").attr("name", "ListSalesPromotionAvaiableTime[" + index + "].WeekDay");

            var cellTime = $(this).find(".timelist-promotion");
            cellTime.find("input.minValue").attr("id", "ListSalesPromotionAvaiableTime_" + index + "__StartTime");
            cellTime.find("input.minValue").attr("name", "ListSalesPromotionAvaiableTime[" + index + "].StartTime");
            cellTime.find("input.maxValue").attr("id", "ListSalesPromotionAvaiableTime_" + index + "__EndTime");
            cellTime.find("input.maxValue").attr("name", "ListSalesPromotionAvaiableTime[" + index + "].EndTime");
            index++;
        });

        $sliderCountIndex = index - 1;
    });

    var i = 0;
    $("tbody#abertura").children().slice(i).each(function () {

        /*var check = $(this).css("display");

        if (check === "none") {
            var x = $(this).find('input.minValue').val();
            var u = $(this).find('input.maxValue').val();

            if (x !== "" && u !== "") {
                $(this).show();
            }
        }
        i++;*/
    });

});

$.validator.addMethod("rangedate", function (value, element, params) {
    if (validateDates()) {
        $("#rangedate-validation").hide();
        return true;
    } else {
        $("#rangedate-validation").show();
        return false;
    }

}, "Período de datas inválido");

$.validator.classRuleSettings.rangedate = { rangedate: true }

function validateDates() {
    if (($("#StartDate").val().length != 0 && !isValidDate($("#StartDate").val())) ||
        ($("#EndDate").val().length != 0 && !isValidDate($("#EndDate").val()))) {
        return false;
    }

    if ($("#StartDate").val().length > 0 && $("#EndDate").val().length > 0) {

        var startParts = $("#StartDate").val().split("/");
        Start = new Date(parseInt(startParts[2], 10),
                          parseInt(startParts[1], 10) - 1,
                          parseInt(startParts[0], 10)
                          , 0, 0, 0, 0);

        var endParts = $("#EndDate").val().split("/");
        End = new Date(parseInt(endParts[2], 10),
                          parseInt(endParts[1], 10) - 1,
                          parseInt(endParts[0], 10)
                          , 0, 0, 0, 0);

        if (Start > End) {
            return false;
        } else {
            return true;
        }

    } else {
        return true;
    }

}

function isValidDate(value) {
    try {// Notice 'yy' indicates a 4-digit year value
        var testdate = $.datepicker.parseDate('dd/mm/yy', value);
        return true;
    } catch (e) {
        return false;
    }
}

$.validator.addMethod("weekdayselection", function (value, element, params) {
    if ($(".week-day[type=checkbox]:checked").length > 0) {
        $("#weekday-validation").hide();
        return true;
    } else {
        $("#weekday-validation").show();
        return false;
    }
});

$.validator.classRuleSettings.weekdayselection = { weekdayselection: true }

$.validator.addMethod("rangedatedouble", function (value, element, params) {
    if (validateDatesDouble()) {
        $("#rangedate-validation").hide();
        return true;
    } else {
        $("#rangedate-validation").show();
        return false;
    }

}, "Período de datas inválido");

$.validator.classRuleSettings.rangedatedouble = { rangedatedouble: true }

function validateDatesDouble() {
    if (($("#SalesPromotionStartDate").val().length != 0 && !isValidDate($("#SalesPromotionStartDate").val())) ||
        ($("#SalesPromotionEndDate").val().length != 0 && !isValidDate($("#SalesPromotionEndDate").val()))) {
        return false;
    }

    if ($("#SalesPromotionStartDate").val().length > 0 && $("#SalesPromotionEndDate").val().length > 0) {

        var startParts = $("#SalesPromotionStartDate").val().split("/");
        Start = new Date(parseInt(startParts[2], 10),
                          parseInt(startParts[1], 10) - 1,
                          parseInt(startParts[0], 10)
                          , 0, 0, 0, 0);

        var endParts = $("#SalesPromotionEndDate").val().split("/");
        End = new Date(parseInt(endParts[2], 10),
                          parseInt(endParts[1], 10) - 1,
                          parseInt(endParts[0], 10)
                          , 0, 0, 0, 0);

        if (Start > End) {
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }

}