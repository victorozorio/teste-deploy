﻿
var indexChanged = 0;
var optionals = {};
var hasbugs = false;

var urlGetMenuGroups = $("#ActionGetMenuGroups").data('actiongetmenugroups');
var urlGetMenuDishes = $("#ActionGetMenuDishes").data('actiongetmenudishes');
var urlGetActiveMenuDishOptionals = $("#ActionGetActiveMenuDishOptionals").data('actiongetactivemenudishoptionals');
var urlGetActiveMenuDishOptionalItems = $("#ActionGetActiveMenuDishOptionalItems").data('actiongetactivemenudishoptionalitems');

$(document).ready(function () {

    //LoadMenuGroup();

    $('#SalesPromotionForm').submit(function (event) {
        optionals = {};
        hasbugs = false;
        $("select[name*='IdItem'").each(function () {
            
            if (optionals[this.value] !== undefined) {
                $("#ValidationOptionalItens").removeClass("field-validation-valid").addClass("field-validation-error").html("Existem Campos Duplicados").show();
                setTimeout("$(\"#ValidationOptionalItens\").hide();", 5000);
                hasbugs = true;
            }
            optionals[this.value] = this;
        });
        if (hasbugs)
            return false;
    });

    $('#SalesPromotionForm').on('change', '#IdPromotionalMenu', function () {
        $('.OptionalItens div').remove();
        LoadMenuDishes();
    });

    $('#SalesPromotionForm').on('change', '#IdPromotionalDish', function () {
        $('.OptionalItens div').remove();
        AddLinkStatus();
    });

    $('#SalesPromotionForm').on('change', '#IdRestaurant', function () {
        $('.OptionalItens div').remove();
        LoadMenuGroup();
    });

    $('.OptionalItens').on('change', '.OptionalAttr', function () {
        var $div = $(this).closest('div');
        indexChanged = $('.OptionalItens div').index($div);
        var idMenuDishOptional = $(this).val();

        $("select[name='ListSalesPromotionOptional[" + indexChanged + "].IdItem'").find('option').remove().end();
        $.getJSON(urlGetActiveMenuDishOptionalItems, { idMenuDishOptional: idMenuDishOptional }, function (result) {
            $.each(result, function (key, value) {
                var i = $('.OptionalItens div').length - 1;
                var select = $("select[name='ListSalesPromotionOptional[" + indexChanged + "].IdItem'").get(0);

                if (select != null) {
                    select.options[select.options.length] = new Option(value.Name, value.IdMenuDishOptionalItem.toString());
                }
            })
        });
    });

    $('.OptionalItens').on('click', '.table-delete-link', function (e) {
        var $div = $(this).closest('div');
        var rowIndex = $('.OptionalItens div').index($div);
        $div.remove();
        $.each($('.OptionalItens div').slice(rowIndex), function (key, value) {
            $.each($(value).find('select'), function (key2, value2) {
                var i = (rowIndex + key);
                switch (key2) {
                    case 0:
                        $(value2).attr('name', 'ListSalesPromotionOptional[' + i + '].IdOptional');
                        break;
                    case 1:
                        $(value2).attr('name', 'ListSalesPromotionOptional[' + i + '].IdItem');
                        break;
                }
            });
        });
    });

    $('.OptionalItens').on('click', '.add-link', function (e) {
        e.preventDefault();

        if (!$('#IdPromotionalDish').val() || $(this).hasClass('disabled')) {
            return;
        }

        var i = $('.OptionalItens div').length;

        var $selectList = document.createElement("select");
        $selectList.name = 'ListSalesPromotionOptional[' + i + '].IdOptional';
        $selectList.style.width = "200px";
        $selectList.className = "OptionalAttr";

        LoadOptional($('#IdPromotionalDish').val());

        var $selectListItem = document.createElement("select");
        $selectListItem.name = 'ListSalesPromotionOptional[' + i + '].IdItem';
        $selectListItem.style.width = "200px";

        var $spanRemoveButton = $('<span class="row-details" style="padding-left:10px">').append($('<a href="javascript:void(0);" class="table-delete-link">'));
        var $div = $('<div style="padding-left:160px; padding-top:10px">').append($selectList).append("&nbsp;").append($selectListItem).append($spanRemoveButton);
        $('.OptionalItens').append($div);
    });
});

function LoadOptional(idMenuDish) {
    $("#addLink").css('pointer-events', 'none');
    $.getJSON(urlGetActiveMenuDishOptionals, { idMenuDish: idMenuDish }, function (result) {
        if (result.length == 0) {
            $('.OptionalItens div').remove();
            return;
        }

        $.each(result, function (key, value) {
            var i = $('.OptionalItens div').length - 1;
            var select = $("select[name='ListSalesPromotionOptional[" + i + "].IdOptional'").get(0);

            if (select != null) {
                select.options[select.options.length] = new Option(value.Name, value.IdMenuDishOptional.toString());
            }
        })

        var i = $('.OptionalItens div').length - 1;
        var select = $("select[name='ListSalesPromotionOptional[" + i + "].IdOptional'").get(0);

        if (select != null) {
            LoadOptionalItem(select.options[select.selectedIndex].value)
        }

        $("#addLink").css('pointer-events', 'auto');
    });
}

function LoadOptionalItem(idMenuDishOptional) {
    $("#addLink").css('pointer-events', 'none');
    $.getJSON(urlGetActiveMenuDishOptionalItems, { idMenuDishOptional: idMenuDishOptional }, function (result) {
        $.each(result, function (key, value) {
            var i = $('.OptionalItens div').length - 1;
            var select = $("select[name='ListSalesPromotionOptional[" + i + "].IdItem'").get(0);

            if (select != null) {
                select.options[select.options.length] = new Option(value.Name, value.IdMenuDishOptionalItem.toString());
            }
        })

        $("#addLink").css('pointer-events', 'auto');
    });
}

function AddLinkStatus() {
    
    $.getJSON(urlGetActiveMenuDishOptionals, { idMenuDish: $('#IdPromotionalDish').val() }, function (result) {
        if (result.length == 0) {
            $("#addLink").css('pointer-events', 'none');
        }
        else {
            $("#addLink").css('pointer-events', 'auto');
        }
    });
}

function LoadMenuGroup() {

    $("#IdPromotionalMenu").find('option').remove().end();

    if (!$('#IdRestaurant').val()) {
        LoadMenuDishes();
        return;
    }

    $.getJSON(urlGetMenuGroups, { idRestaurant: $('#IdRestaurant').val() }, function (result) {
        $.each(result, function (key, value) {

            select = $("#IdPromotionalMenu").get(0);

            if (select != null) {
                var disabled = "";

                if (value.Disabled) {
                    disabled = "- (Inativo)";
                }

                if (value.Description != null) {
                    select.options[select.options.length] = new Option(value.Name + ' (' + value.Description + ') ' + disabled, value.IdMenuGroup.toString());
                }
                else {
                    select.options[select.options.length] = new Option(value.Name + disabled, value.IdMenuGroup.toString());
                }
            }
        });

        LoadMenuDishes();
    });
}

function LoadMenuDishes() {

    $("#IdPromotionalDish").find('option').remove().end();

    idMenuGroup = $("#IdPromotionalMenu").val();

    if (!idMenuGroup) {
        return;
    }

    $.getJSON(urlGetMenuDishes, { idMenuGroup: idMenuGroup }, function (result) {
        if (result.length > 0) {
            $.each(result, function (key, value) {

                select = $("#IdPromotionalDish").get(0);

                if (select != null) {
                    var disabled = "";

                    if (value.Disabled) {
                        disabled = " - (Inativo)"
                    }

                    select.options[select.options.length] = new Option(value.Name + disabled, value.IdMenuDish.toString());
                }
            });

            AddLinkStatus();

        } else {
            showMessage("Menu não possui pratos selecionáveis para promoção.", "Promoções");
        }

    });
}

$.validator.addMethod("weekdayselection", function (value, element, params) {
    if ($(".week-day[type=checkbox]:checked").length > 0) {
        $("#weekday-validation").hide();
        return true;
    } else {
        $("#weekday-validation").show();
        return false;
    }
});

$.validator.classRuleSettings.weekdayselection = { weekdayselection: true }