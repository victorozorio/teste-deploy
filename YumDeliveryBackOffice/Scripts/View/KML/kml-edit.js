﻿$(document).ready(function () {
    $("#IncludeOrderDeliveryFee").click(toggleEnableSeparateDeliveryFeeMessage);
    toggleEnableSeparateDeliveryFeeMessage();

    $(".taxaSelected").click(function () {

        if ($(this).is(':checked')) {
            var b = +$(this).data("check");

            var c = $("input#RestaurantDeliveryFeeByPolygonAreaList_" + (b - 1) + "__Selected");

            if (!c.is(":checked") && c.length > 0) {
                $(this).attr("checked", false)
                alert("Selecione o Anterior")

            } else {
                $(this).parent().find("input.entrega").each(function (key, value) {
                    $(value).removeAttr('disabled')
                });
                AtualizaTaxa();
            }
        } else {

            var b = +$(this).data("check");
            var c = $("input#RestaurantDeliveryFeeByPolygonAreaList_" + (b + 1) + "__Selected");

            if (c.is(":checked") && c.length > 0) {
                $(this).prop("checked", true)
                alert("Não e possivel remover seleção, primeiro remova do checkbox abaixo")

            } else {
                $(this).parent().find("input.entrega").each(function (key, value) {
                    $(value).prop('disabled', true)
                });
                AtualizaTaxa();
            }
        }
    });
});

function AtualizaTaxa() {

    $("input#RestaurantDeliveryFeeByPolygonAreaList_1__ValueFrom").val(+$("input#RestaurantDeliveryFeeByPolygonAreaList_0__ToValue").val() + 1)

    var check = $("input#RestaurantDeliveryFeeByPolygonAreaList_0__ToValue").val().length;
    if (check !== 0) {
        $("input#RestaurantDeliveryFeeByPolygonAreaList_2__ValueFrom").val(+$("input#RestaurantDeliveryFeeByPolygonAreaList_1__ToValue").val() + 1)
    }

}

function toggleEnableSeparateDeliveryFeeMessage() {
    if (!$('#IncludeOrderDeliveryFee').is(':checked')) {
        $('#SeparateDeliveryFeeMessage').show();
        $('#alertSeparateDeliveryFeeMessage').show(); 
    }
    else {
        $('#SeparateDeliveryFeeMessage').hide();
        $('#alertSeparateDeliveryFeeMessage').hide();  
    }
}