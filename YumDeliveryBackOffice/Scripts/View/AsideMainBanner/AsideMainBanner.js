﻿$.validator.setDefaults({
    ignore: [],
});

$.validator.addMethod('specialCharFileName', function (value, element) {
    if (value) {
        var fileName = value.replace(/C:\\fakepath\\/i, '');
        return /^[a-z\u00E0-\u00FCA-Z\u00C0-\u00DC0-9().& _-]*$/.test(fileName);
    }
    else {
        return true;
    }
}, "O nome do arquivo não pode conter caracteres especiais");

$(document).ready(function () {

    $('input[name=\'FullImage\']').rules('add', { specialCharFileName: true });
    $('input[name=\'LightImage\']').rules('add', { specialCharFileName: true });

    $("#LojaMenu").on("click", ".add-ljmn", function () {

        var $form = $(this);
        var $dropDown = $(this).parent().parent().find(".Restaurant_IdRestaurant");
        var $dropDown1 = $(this).parent().parent().find(".IdMenuGroup");
        var name = $dropDown.prop('name');
        var name1 = $dropDown1.prop('name');

        if (+$dropDown.val() !== 0 && +$dropDown1.val() !== 0) {
            if ($dropDown.is('.disabled')) {
            } else {
                //IdRestaurant
                var $hiddenInput = $('<input/>', { type: 'hidden', name: name, value: $dropDown.val() });
                $form.parent().append($hiddenInput);  //append the hidden field with same name and value from the dropdown field 
                $dropDown.addClass('disabled')  //disable class
                         .prop({ 'name': name + "_1", disabled: true }); //change name and disbale 

                //IdMenuGroup
                var $hiddenInput1 = $('<input/>', { type: 'hidden', name: name1, value: $dropDown1.val() });
                $form.parent().append($hiddenInput1);  //append the hidden field with same name and value from the dropdown field 
                $dropDown1.addClass('disabled')  //disable class
                         .prop({ 'name': name1 + "_1", disabled: true }); //change name and disbale
            }

            var $clone = $(this).parent().parent().clone();
            var i = $(this).parent().parent().parent().get(0).childElementCount;
            i = i - 1;

            $.each($clone.find("select"), function (key, value) {

                $(value).prop('selectedIndex', 0);

                $(value).removeClass('disabled')
                             .prop({ disabled: false });

                if (key == 0) {
                    $(value).attr('id', 'MenuGroupList_' + i + '__IdRestaurant')
                            .attr('name', 'MenuGroupList[' + i + '].IdRestaurant');
                } else if (key == 1) {
                    $(value).attr('id', 'MenuGroupList_' + i + '__IdMenuGroup')
                            .attr('name', 'MenuGroupList[' + i + '].IdMenuGroup');
                    $(value).html("");
                    $(value).val("");
                }
            });

            $.each($clone.find("input"), function (key, value) {
                $(value).remove();
            });

            $(this).parent().parent().parent().append($clone);
            $(this).hide();
            $(this).next().show();
        } else {
            showMessage("Selecione Loja e Menu para adicionar novo.", "ATENÇÃO", "warning");
        }

    });

    $("#LojaMenu").on("click", ".rem-ljmn", function () {


        $(this).parent().parent().remove();

        var $tbody = $("#LojaMenu").find("tbody");
        var i = -1;


        $tbody.find("tr").each(function (key, value) {
            
            $(value).find("td").find("input").each(function (key1, value1) {
                if (key1 == 0) {
                    $(value1).attr('id', 'MenuGroupList_' + i + '__IdRestaurant')
                            .attr('name', 'MenuGroupList[' + i + '].IdRestaurant');
                } else if (key1 == 1) {
                    $(value1).attr('id', 'MenuGroupList_' + i + '__IdMenuGroup')
                            .attr('name', 'MenuGroupList[' + i + '].IdMenuGroup');
                } else if (key1 == 2) {
                    
                    type = $(value1).data("type");
                    if (type === "main") {
                        $(value1).attr('id', 'MenuGroupList_' + i + '__IdMainBanner_MenuGroup')
                                .attr('name', 'MenuGroupList[' + i + '].IdMainBanner_MenuGroup');
                    } else if(type === "aside"){
                        $(value1).attr('id', 'MenuGroupList_' + i + '__IdAsideBanner_MenuGroup')
                                .attr('name', 'MenuGroupList[' + i + '].IdAsideBanner_MenuGroup');
                    }
                }
            });

            $(value).find("td").find("select").each(function (key2, value2) {
                if (key2 == 0) {
                    $(value2).attr('id', 'MenuGroupList_' + i + '__IdRestaurant')
                            .attr('name', 'MenuGroupList[' + i + '].IdRestaurant');
                } else if (key2 == 1) {
                    $(value2).attr('id', 'MenuGroupList_' + i + '__IdMenuGroup')
                            .attr('name', 'MenuGroupList[' + i + '].IdMenuGroup');
                }
            });

            i++;
        });



    });

    $("#BannerForm").submit(function (event) {

        var lojamenu = new Array();
        $("#LojaMenu").find("tr").each(function (key, value) {

            var union = ""
            $(value).find("td").find("select").each(function (key1, value1) {
                union += $(value1).val()
            })
            lojamenu.push(union)

        })

        var counts = new Array();
        lojamenu.forEach(function (x) { counts[x] = (counts[x] || 0) + 1; });
        var verifica = false;
        counts.forEach(function (x) {
            if (x > 1) {
                verifica = true;
            }
        })

        if (verifica) {
            $(".LojaMenuValidate").removeClass("field-validation-valid").addClass("field-validation-error").html("Loja/Menu repetidos, verifique e tente novamente").show();
            setTimeout("$(\".LojaMenuValidate\").hide();", 2340)
            return false;
        }

    });

});