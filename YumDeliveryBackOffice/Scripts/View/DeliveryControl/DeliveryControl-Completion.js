﻿var deliveryControl = deliveryControl || {};

(function (namespace) {

    var modalConfirm = (function () {

        var $modal = $('[data-modal-confirm]');
        var $close = $modal.find('[data-close]');
        var $confirm = $modal.find('[data-confirm]');
        var confirmEvent = function () { };

        var open = function (confirmCallback) {
            confirmEvent = confirmCallback;
            $modal[0].style.display = "block";
        };

        var close = function () {
            $modal[0].style.display = "none";
        };

        // When the user clicks anywhere outside of the modal, close it
        window.addEventListener('click', function (event) {
            if (event.target == $modal[0]) {
                $modal[0].style.display = "none";
            }
        });

        $close.on('click', close);

        $confirm.on('click', function () {
            confirmEvent();
        });

        return {
            open: open,
            close: close
        };

    })();

    var modalNoOrderSelected = (function () {

        var $modal = $('[data-modal-select-order]');
        var $close = $modal.find('[data-close]');

        var open = function () {            
            $modal[0].style.display = "block";
        };

        var close = function () {
            $modal[0].style.display = "none";
        };

        // When the user clicks anywhere outside of the modal, close it
        window.addEventListener('click', function (event) {
            if (event.target == $modal[0]) {
                $modal[0].style.display = "none";
            }
        });

        $close.on('click', close);

        return {
            open: open
        };

    })();


    var Order = function (element, getCancelDeliveryUrl) {

        var $element = $(element);
        var idOrder = $element.attr('data-order');
        var $iconChecked = $element.find('[data-icon-checked]');
        var $iconUnchecked = $element.find('[data-icon-unchecked]');
        var $selectedInput = $element.find('[data-selected]');
        var $btnCancelDelivery = $element.find('[data-cancel-delivery]');      
        var $statusMessage = $element.find('[data-status-message]');
        var isDisabled = false;

        var isSelected = function () {
            return $selectedInput.val().toUpperCase() === "TRUE";
        };

        var select = function () {
            if (isDisabled) {
                return;
            }
            $iconUnchecked.hide();
            $iconChecked.show();
            $selectedInput.val('true');
            $element.addClass('card-selected');
        };

        var removeSelect = function () {
            $iconChecked.hide();
            $iconUnchecked.show();
            $selectedInput.val('false');
            $element.removeClass('card-selected');
        };

        var setDisabled = function (value) {
            isDisabled = value;
            if (isDisabled) {
                removeSelect();
                $iconUnchecked.hide();
                $element.addClass('disabled');
            } else {
                $element.removeClass('disabled');
            }
        };

        var showStatusMessage = function (text) {
            $btnCancelDelivery.hide();
            $statusMessage.text(text).show();
        };

        var showCancelButton = function () {
            $statusMessage.hide();
            $btnCancelDelivery.show();
        };

        $element.on('click', function (e) {
            // Caso seja o botão de cancelar, não podemos selecionar o card.
            if ($(e.target).is(':button')) {
                return;
            };

            if (isSelected()) {
                removeSelect();
            } else {
                select();
            }
        });



        $btnCancelDelivery.on('click', function () {
            modalConfirm.open(function () {
                setDisabled(true);
                showStatusMessage('Cancelando...');
                modalConfirm.close();

                $.post(getCancelDeliveryUrl(), { idOrder: idOrder }, function () {
                    showStatusMessage('Entrega Cancelada');
                })
                .fail(function () {
                    showStatusMessage("Erro Inesperado");
                });

            });
        });

        if (isSelected()) {
            select();
        } else {
            removeSelect();
        }

        this.isSelected = isSelected;

    };

    namespace.view = (function () {

        var config = {
            backUrl: null,
            cancelDeliveryUrl: null
        };

        var orders = [];

        var $submitBtn = $('[data-submit-form]')
        var $cancelBtn = $('[data-cancel]');
        var $form = $cancelBtn.closest('form');

        var isAnyOrderSelected = function () {
            for (var i = 0; i < orders.length; i++) {
                if (orders[i].isSelected()) {
                    return true;
                }
            }
            return false;
        };

        $cancelBtn.on('click', function () {
            window.location = config.backUrl;
        });

        $submitBtn.on('click', function (e) {
            if (isAnyOrderSelected()) {
                $form.submit();
            } else {
                modalNoOrderSelected.open();
            }
        });

        $('[data-order]').each(function () {
            var order = new Order(this, function () {
                return config.cancelDeliveryUrl;
            });
            orders.push(order);
        });

        return {
            // @param options = { backUrl, cancelDeliveryUrl }
            registerConfig: function (options) {
                config = options;
            }
        };



    })();

})(deliveryControl);