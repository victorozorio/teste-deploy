﻿var deliveryControl = deliveryControl || {};

(function (namespace) {

    var Modal

    var Order = function (element) {

        var $element = $(element);
        var $iconChecked = $element.find('[data-icon-checked]');
        var $iconUnchecked = $element.find('[data-icon-unchecked]');
        var $selectedInput = $element.find('[data-selected]');

        var isSelected = function () {
            return $selectedInput.val().toUpperCase() == "TRUE";
        };

        var select = function () {
            $iconUnchecked.hide();
            $iconChecked.show();
            $selectedInput.val('true');
            $element.addClass('card-selected');
        };

        var removeSelect = function () {
            $iconChecked.hide();
            $iconUnchecked.show();
            $selectedInput.val('false');
            $element.removeClass('card-selected');
        };

        $element.on('click', function () {
            if (isSelected()) {
                removeSelect();
            } else {
                select();
            }
        });

        if (isSelected()) {
            select();
        } else {
            removeSelect();
        }

        this.isSelected = isSelected;
    };

    var modalNoOrderSelected = (function () {

        var $modal = $('[data-modal-select-order]');
        var $close = $modal.find('[data-close]');

        var open = function () {
            $modal[0].style.display = "block";
        };

        var close = function () {
            $modal[0].style.display = "none";
        };

        // When the user clicks anywhere outside of the modal, close it
        window.addEventListener('click', function (event) {
            if (event.target == $modal[0]) {
                $modal[0].style.display = "none";
            }
        });

        $close.on('click', close);

        return {
            open: open
        };

    })();

    namespace.view = (function () {

        var config = {
            backUrl: null
        };

        var orders = [];

        var $submitBtn = $('[data-submit-form]')
        var $cancelBtn = $('[data-cancel]');
        var $form = $cancelBtn.closest('form');

        var isAnyOrderSelected = function () {
            for (var i = 0; i < orders.length; i++) {
                if (orders[i].isSelected()) {
                    return true;
                }
            }
            return false;
        };

        $cancelBtn.on('click', function () {
            window.location = config.backUrl;
        });

        $submitBtn.on('click', function (e) {
            if (isAnyOrderSelected()) {
                $form.submit();
            } else {
                modalNoOrderSelected.open();
            }
        });

        $('[data-order]').each(function () {
            orders.push(new Order(this));
        });

        return {
            // @param options = { backUrl }
            registerConfig: function (options) {
                config = options;
            }
        };

    })();        

})(deliveryControl);