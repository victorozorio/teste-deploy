﻿function clearFieldsOrderTracking(formName) {
    $('input[name^="Filter.ListRestaurant["]').remove();
    $('.table-delete-link').remove();
    $('#Filter_Origen')[0].selectedIndex = 0;

    var now = new Date();
  
    clearFields(formName);

    $('#Filter_CreatedStart').val(now.getDate() + '/' + ((now.getMonth() + 1) < 10 ? "0" + (now.getMonth() + 1) : (now.getMonth() + 1)) + '/' + now.getFullYear() + " 00:00:00");
    $('#Filter_CreatedEnd').val(now.getDate() + '/' + ((now.getMonth() + 1) < 10 ? "0" + (now.getMonth() + 1) : (now.getMonth() + 1)) + '/' + now.getFullYear() + " 23:59:59");
}

function confirmOrder(url, idOrder) {
    $.get(url + "/" + idOrder, function (result) {
        $('#linkModal').html(result);
        $('#linkModal').dialog({
            title: "Confirmação Manual",
            width: 257,
            height: 200,
            close: function () {
                $("#linkModal").empty();
            }
        });
    });
//    finalizeOrder('confirmOrder');
}

function cancelOrder(url, idOrder) {
    $.get(url + "/" + idOrder, function (result) {
        $('#linkModal').html(result);
        $('#linkModal').dialog({
            title: "Cancelamento Manual",
            width: 257,
            height: 200,
            close: function () {
                $("#linkModal").empty();
            }
        });
    });
//    finalizeOrder('cancelOrder');
}

function finalizeOrder(actionClass) {
    var iditem = $('.' + actionClass).data("id");
    var url = $('.' + actionClass).attr("href");

    $.post(url, { id: iditem }, function (result) {
        if (result.valido) {
            //$("#divDetails").dialog("close");
            page_refresh(true);
            showMessage("Status Atualizado", "ATENÇÃO", "warning")
        } else {
            showMessage("Houve Falhas ao Completar a Ação", "ATENÇÃO", "warning")
        }
        
    });
}