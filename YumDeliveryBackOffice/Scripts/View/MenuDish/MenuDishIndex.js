﻿var restaurantMenus = null;
var menuDishId = 0;

$(".clone-dish-link").on("click", function (e) {
    e.preventDefault();

    menuDishId = $(this).attr('dish-id');

    $('#divLink').dialog({
        title: "Clonar Prato",
        width: 350,
        height: 160,
        close: function () {
            $("#divLink").hide();
        }
    });
    hideLoadingOverlay();
    loadRestaurantsMenus();
});

$("#restaurants").on("change", function () {
    listMenus($(this).val());
});

function loadRestaurantsMenus() {
    $.ajax({
        url: $("#applicationPath").val() + '/MenuDish/GetRestaurantsWithMenus',
        type: 'POST',
        async: true,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            showLoadingOverlay();
        },
        complete: function () {
            hideLoadingOverlay();
        },
        success: function (result) {
            restaurantMenus = result;
            listRestaurants();
        }
    });
}

function listRestaurants() {
    $('#restaurants').find('option').remove().end();
    var selectedRestaurant = $("#Restaurant_IdRestaurant").val();
    $.each(restaurantMenus, function (i, item) {
        $("#restaurants").append($("<option/>", { value: item.restaurantId, text: item.restaurantName }));
    });
    $("#restaurants").trigger("change");
}

function listMenus(idRestaurant) {
    $('#menuGroups').find('option').remove().end();
    $.each(restaurantMenus, function () {
        if (this.restaurantId == idRestaurant) {
            $.each(this.menus, function () {
                $("#menuGroups").append($("<option/>", { value: this.menuId, text: this.menuName }));
            });
        }
    });
}

function cloneMenuDish() {

    try {

        var idMenuGroup = parseInt($("#menuGroups option:selected").val(), 10);
        if (idMenuGroup < 1) {
            showMessage("Selecione um Menu");
            return;
        }

        var idMenuDish = parseInt(menuDishId, 10);
        if (idMenuDish < 1) {
            showMessage("Prato não encontrado");
            return;
        }

        var idMenuDishes = [];
        idMenuDishes.push(idMenuDish);

        var filter = {
            IdMenuGroupTo: idMenuGroup,
            IdMenuDishes: idMenuDishes
        }

        $.ajax({
            url: $("#applicationPath").val() + '/MenuDish/CloneMenuDish',
            type: 'POST',
            data: JSON.stringify(filter),
            async: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                showLoadingOverlay();
            },
            error: function (result) {
                showMessage(result.responseText);
            },
            complete: function () {
                hideLoadingOverlay();
            },
            success: function (result) {
                showMessage("Prato clonado com sucesso");
            }
        });

    } catch (exception) {
        showMessage("Erro ao clonar prato.");
    }

}