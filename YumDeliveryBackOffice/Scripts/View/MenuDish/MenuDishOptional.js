﻿$(document).ready(function () {

    $('#MenuDishOptionalForm').on('change', '#IdMenuAttributeTypeOp', function (event) {
        LoadTipo2(event);
        $('.IdMenuAttributeTypeOpI').val($(this).children('option:selected').text()).valid();
    });

    $(".add-optionalItem").on("click", function () {

        var clone = $(".rowItem").first().clone();
        var i = $(".optionalItem").children().length;
        
        $.each(clone.find("input"), function (key, value) {
            if (key == 1) {
                $(value).attr('id', 'MenuDishOptionalItemList_' + i + '__Name')
                    .attr('name', 'MenuDishOptionalItemList[' + i + '].Name');
                $(value).val('');
            } else if (key == 2) {
                $(value).attr('id', 'MenuDishOptionalItemList_' + i + '__Order')
                    .attr('name', 'MenuDishOptionalItemList[' + i + '].Order');
                $(value).val('');
            } else if (key == 3) {
                $(value).attr('id', 'MenuDishOptionalItemList_' + i + '__Value')
                    .attr('name', 'MenuDishOptionalItemList[' + i + '].Value');
                $(value).val('');
            } else if (key == 4) {
                $(value).attr('id', 'MenuDishOptionalItemList_' + i + '__IntegrationCode')
                    .attr('name', 'MenuDishOptionalItemList[' + i + '].IntegrationCode');
                $(value).val('');
            }
        });

        clone.find("textarea").attr('id', 'MenuDishOptionalItemList_' + i + '__Description');
        clone.find("textarea").attr('name', 'MenuDishOptionalItemList[' + i + '].Description');
        var textArea = clone.find("textarea");
        textArea.html("");
        textArea.get(0).value = "";

        clone.find("select").each(function (key, value) {
            if ($(value).hasClass("IdMenuAttributeTypeOpI")) {
                $(value).attr('id', 'MenuDishOptionalItemList_' + i + '__IdMenuAttributeType');
                $(value).attr('name', 'MenuDishOptionalItemList[' + i + '].IdMenuAttributeType');
                delete $(value).get(0).dataset.selected;
                $(value).prop('selectedIndex', 0);
            } else if (value == clone.find("select[class='IdMenuDishOptionalItemProductCode']:last")[0]) {
                $(value).attr('id', "MenuDishOptionalItemList_" + i + "__MenuDishOptionalItemProductCodeList_0__IdProductCode");
                $(value).attr('name', "MenuDishOptionalItemList[" + i + "].MenuDishOptionalItemProductCodeList[0].IdProductCode");
                delete $(value).get(0).dataset.selected;
                $(value).prop('selectedIndex', 0);
            } else if (value == clone.find("select[class*='IdMenuDishOptionalItemOfRestriction']:last")[0]) {
                $(value).attr('id', 'MenuDishOptionalItemList_' + i + '__MenuDishOptionalItemRestrictionList_0__IdMenuDishOptionalItemOfRestriction');
                $(value).attr('name', 'MenuDishOptionalItemList[' + i + '].MenuDishOptionalItemRestrictionList[0].IdMenuDishOptionalItemOfRestriction');
                $(value).prop('selectedIndex', 0);
                $(value).attr("disabled", false);
                $(value).removeClass("disabled");
                delete $(value).get(0).dataset.idmenudishoptionalitem;
                delete $(value).get(0).dataset.selected;
                $(value).next().show();
                $(value).next().next().hide();
                LoadRestriction($(value));
            } else {
                $(value).parent().remove();
            }

        });

        clone.find(".custom-combobox-optional").remove();
        clone.find(".IdMenuDishOptionalItemProductCode").combobox();
        clone.show();

        $(".optionalItem").append(clone);

        MaskApply();
    })

    $(".rem-optionalItem").on("click", function () {

        var clone = $(this).parent().parent();


        $.each(clone.find("input"), function (key, value) {

            if (key == 1) {
                $(value).val('');
            } else if (key == 2) {
                $(value).val('');
            } else if (key == 3) {
                $(value).val('');
            } else if (key == 4) {
                $(value).val('');
            }
        });

        var textArea = clone.find("textarea");
        textArea.html("");
        textArea.get(0).value = "";

        clone.find("select").each(function (key, value) {

            if (key == 0) {
                delete $(value).get(0).dataset.selected;
                $(value).prop('selectedIndex', 0);
            } else if (key == 1) {
                $(value).prop('selectedIndex', 0);
                $(value).attr("disabled", false);
                $(value).removeClass("disabled");
                delete $(value).get(0).dataset.idmenudishoptionalitem;
                delete $(value).get(0).dataset.selected;

            } else {
                $(value).parent().remove();
            }

        })

        clone.hide();
    })

    $(".optionalItem").on("click", ".add-ljmn", function (e) {
        e.preventDefault();

        var $button = $(this);
        var $dropDown = $(this).parent().find(".IdMenuDishOptionalItemOfRestriction");
        var name = $dropDown.prop('name');
        var iddrop = $dropDown.prop('id');
        var val = $dropDown.val();

        var re1 = '(MenuDishOptionalItemList)';
        var re2 = '(\\[.*?\\])';
        var re3 = '(.)';

        var p = new RegExp(re1 + re2 + re3, ["i"]);
        var nome = p.exec(name);

        var re1 = '(MenuDishOptionalItemList)';
        var re2 = '(_)';
        var re3 = '(\\d+)';
        var re4 = '(_)';
        var re5 = '(_)';

        var p = new RegExp(re1 + re2 + re3 + re4 + re5, ["i"]);
        var id = p.exec(iddrop);

        var validaRepitido = 0;
        $(this).parent().parent().each(function (key, value) {

            $(value).find("select").each(function () {

                if ($(this).val() === val) {
                    validaRepitido++
                }

            })

        })
        if (validaRepitido === 1 || validaRepitido === 0) {

            if (+$dropDown.val() !== 0) {
                if (!$dropDown.is('.disabled')) {
                    var $hiddenInput = $('<input/>', { type: 'hidden', name: name, value: $dropDown.val() });
                    $button.parent().append($hiddenInput);
                    $dropDown.addClass('disabled')
                             .prop({ 'name': name + "_1", disabled: true });
                }

                var $clone = $(this).parent().clone();
                var y = $(this).parent().parent().get(0).childElementCount;
                y = y - 1;

                $.each($clone.find("select"), function (key, value) {
                    $(value).prop('selectedIndex', 0);
                    $(value).data("selected", "");
                    $(value).data("idmenudishoptionalitem", "");
                    $(value).removeClass('disabled').prop({ disabled: false });
                    $(value).attr('id', id[0] + 'MenuDishOptionalItemRestrictionList_' + y + '__IdMenuDishOptionalItemOfRestriction')
                            .attr('name', nome[0] + 'MenuDishOptionalItemRestrictionList[' + y + '].IdMenuDishOptionalItemOfRestriction');
                });

                $.each($clone.find("input"), function (key, value) {
                    $(value).remove();
                });

                $clone.css("margin-left", 160);
                $(this).parent().parent().append($clone);
                $(this).hide();
                $(this).next().show();
            } else {
                showMessage("Selecione uma restrição para adicionar nova.", "ATENÇÃO", "warning");
            }
        } else {
            showMessage("Esta restrição ja foi adiciona.", "ATENÇÃO", "warning");
            $dropDown.prop('selectedIndex', 0);
        }
    });

    $("#MenuDishOptionalForm").on("click", ".rem-ljmn", function (e) {
        e.preventDefault();

        if ($(this).parent().find(".add-ljmn:visible").length == 1)
            return;

        $(this).parent().remove();
        var $li = $(".lirestriction");
        var i = 0;
        $li.each(function (key, value) {
            var y = 0;
            $(value).find("div").each(function (key3, value3) {
                $(value3).find("input").each(function (key1, value1) {
                    $(value1).attr('id', 'MenuDishOptionalItemList_' + i + '__MenuDishOptionalItemRestrictionList_' + y + '__IdMenuDishOptionalItemOfRestriction')
                            .attr('name', 'MenuDishOptionalItemList[' + i + '].MenuDishOptionalItemRestrictionList[' + y + '].IdMenuDishOptionalItemOfRestriction');
                });
                $(value3).find("select").each(function (key2, value2) {
                    $(value2).attr('id', 'MenuDishOptionalItemList_' + i + '__MenuDishOptionalItemRestrictionList_' + y + '__IdMenuDishOptionalItemOfRestriction')
                            .attr('name', 'MenuDishOptionalItemList[' + i + '].MenuDishOptionalItemRestrictionList[' + y + '].IdMenuDishOptionalItemOfRestriction');
                });
                y++;
            });
            i++;
        });
    });

    $("#optionalProductsCode").on("click", ".add-opc", function (e) {
        e.preventDefault();
        var $dropDown = $(this).parent().find(".IdMenuDishOptionalProductCode");

        if ($dropDown.val() == 0) {
            showMessage("Selecione um produto para adicionar.", "ATENÇÃO", "warning");
            return;
        }

        var productOnList = false;
        $(this).parent().parent().find("select").each(function (key, value) {
            if ($dropDown.attr("name") != value.name) {
                if ($(value).val() == $dropDown.val()) {
                    productOnList = true;
                    return;
                }
            }
        });

        if (productOnList) {
            $dropDown.prop("selectedIndex", 0);
            showMessage("O produto selecionado já existe na lista.", "ATENÇÃO", "warning");
            return;
        }

        var $clone = $(this).parent().clone();
        var y = $(this).parent().parent().get(0).childElementCount - 1;

        $.each($clone.find("select"), function (key, value) {
            $(value).prop('selectedIndex', 0);
            $(value).data("selected", "");
            $(value).data("idproductcode", "")
            $(value).attr('id', 'MenuDishOptionalProductCodeList_' + y + '__IdProductCode')
    				.attr('name', 'MenuDishOptionalProductCodeList[' + y + '].IdProductCode');
        });

        $.each($clone.find("input"), function (key, value) {
            if (value.name == 'MenuDishOptionalProductCodeList[' + (y - 1) + '].IdMenuDishOptional') {
                $(value).attr('id', 'MenuDishOptionalProductCodeList_' + y + '__IdMenuDishOptional')
    					.attr('name', 'MenuDishOptionalProductCodeList[' + y + '].IdMenuDishOptional');
            }
        });

        $clone.css("margin-left", 160);
        $clone.css("margin-top", 5);
        $clone.find(".custom-combobox-optional").remove();
        $clone.find(".IdMenuDishOptionalProductCode").combobox();

        $(this).parent().parent().append($clone);
        $(this).next().show();
        $(this).remove();
    });

    $("#optionalProductsCode").on("click", ".rem-opc", function (e) {
        e.preventDefault();

        if ($(this).parent().find(".add-opc").length == 1) {
            return;
        }

        $(this).parent().remove();

        var y = 0;
        $("#optionalProductsCode").find("div").each(function (key, value) {
            $(value).find("select").each(function (key2, value2) {
                $(value2).attr('id', 'MenuDishOptionalProductCodeList_' + y + '__IdProductCode')
						 .attr('name', 'MenuDishOptionalProductCodeList[' + y + '].IdProductCode');
            });
            $(value).find("input").each(function (key2, value2) {
                if (value2.name == 'MenuDishOptionalProductCodeList[' + (y + 1) + '].IdMenuDishOptional') {
                    $(value2).attr('id', 'MenuDishOptionalProductCodeList_' + y + '__IdMenuDishOptional')
						 .attr('name', 'MenuDishOptionalProductCodeList[' + y + '].IdMenuDishOptional');
                }
            });
            y++;
        });
    });

    $("#MenuDishOptionalForm").on("click", ".add-oipc", function (e) {
        e.preventDefault();
        var $dropDown = $(this).parent().find(".IdMenuDishOptionalItemProductCode");

        if ($dropDown.val() == 0) {
            showMessage("Selecione um produto.", "ATENÇÃO", "warning");
            return;
        }

        var productOnList = false;
        $(this).parent().parent().find("select").each(function (key, value) {
            if ($dropDown.attr("name") != value.name) {
                if ($(value).val() == $dropDown.val()) {
                    productOnList = true;
                    return;
                }
            }
        });

        if (productOnList) {
            showMessage("O produto selecionado já existe na lista.", "ATENÇÃO", "warning");
            return;
        }

        var name = $dropDown.prop('name');
        var iddrop = $dropDown.prop('id');
        var re1 = '(MenuDishOptionalItemList)';
        var re2 = '(\\[.*?\\])';
        var re3 = '(.)';
        var p = new RegExp(re1 + re2 + re3, ["i"]);
        var nome = p.exec(name);
        var re1 = '(MenuDishOptionalItemList)';
        var re2 = '(_)';
        var re3 = '(\\d+)';
        var re4 = '(_)';
        var re5 = '(_)';
        var p = new RegExp(re1 + re2 + re3 + re4 + re5, ["i"]);
        var id = p.exec(iddrop);
        var $clone = $(this).parent().clone();
        var y = $(this).parent().parent().get(0).childElementCount - 1;

        $.each($clone.find("select"), function (key, value) {
            $(value).prop('selectedIndex', 0);
            $(value).data("selected", "");
            $(value).data("idproductcode", "")
            $(value).attr('id', id[0] + 'MenuDishOptionalItemProductCodeList_' + y + '__IdProductCode')
    				.attr('name', nome[0] + 'MenuDishOptionalItemProductCodeList[' + y + '].IdProductCode');
        });

        $.each($clone.find("input"), function (key, value) {
            if (value.name == nome[0] + 'MenuDishOptionalItemProductCodeList[' + (y - 1) + '].IdMenuDishOptionalItem') {
                $(value).attr('id', id[0] + 'MenuDishOptionalItemProductCodeList_' + y + '__IdMenuDishOptionalItem')
    					.attr('name', nome[0] + 'MenuDishOptionalItemProductCodeList[' + y + '].IdMenuDishOptionalItem');
            }
        });

        $clone.css("margin-left", 160);
        $clone.css("margin-top", 5);
        $clone.find(".custom-combobox-optional").remove();
        $clone.find(".IdMenuDishOptionalItemProductCode").combobox();

        $(this).parent().parent().append($clone);
        $(this).next().show();
        $(this).remove();
    });

    $("#MenuDishOptionalForm").on("click", ".rem-oipc", function (e) {
        e.preventDefault();

        if ($(this).parent().find(".add-oipc").length == 1) {
            return;
        }

        $(this).parent().find("select").val("0");
        $(this).parent().find(".custom-combobox-input").val("")
        $(this).parent().find(".custom-combobox-input").css("display", "none");
        $(this).parent().remove();

        var i = 0;
        $("#optionalItemProductsCode").each(function (key, value) {
            var y = 0;
            $(value).find("div").each(function (key3, value3) {
                $(value3).find("select").each(function (key2, value2) {
                    $(value2).attr('id', 'MenuDishOptionalItemList_' + i + '__MenuDishOptionalItemProductCodeList_' + y + '__IdProductCode')
                            .attr('name', 'MenuDishOptionalItemList[' + i + '].MenuDishOptionalItemProductCodeList[' + y + '].IdProductCode');
                });
                $(value3).find("input").each(function (key2, value2) {
                    if (value2.name == 'MenuDishOptionalItemList[' + i + '].MenuDishOptionalItemProductCodeList[' + (y + 1) + '].IdMenuDishOptionalItem') {
                        $(value2).attr('id', 'MenuDishOptionalItemList_' + i + '__MenuDishOptionalItemProductCodeList_' + y + '__IdMenuDishOptionalItem')
							 .attr('name', 'MenuDishOptionalItemList[' + i + '].MenuDishOptionalItemProductCodeList[' + y + '].IdMenuDishOptionalItem');
                    }
                });
                y++;
            });
            i++;
        });
    });

    $.widget("custom.combobox.optional", {
        _create: function () {
            this.wrapper = $("<span>")
				.addClass("custom-combobox-optional")
				.insertAfter(this.element);

            var attr = this.element.attr('data-disabled');
            var disabled = (typeof attr !== typeof undefined && attr !== false);

            this.element.hide();
            this._createAutocomplete(disabled);

            if (disabled) {
                this.element.attr("disabled", true)
            } else {
                this._createShowAllButton();
            }
        },
        _createAutocomplete: function (disabled) {
            var selected = this.element.children(":selected"),
				value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
				.appendTo(this.wrapper)
				.val(value)
				.attr("title", "")
                .attr("disabled", disabled)
				.addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
				.css({ width: "276px" })
				.autocomplete({
				    delay: 0,
				    minLength: 0,
				    source: $.proxy(this, "_source"),
				    disabled: disabled
				})
				.tooltip({
				    classes: {
				        "ui-tooltip": "ui-state-highlight"
				    }
				});

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });
        },
        _createShowAllButton: function () {
            var input = this.input,
				wasOpen = false;

            $("<a>")
				.attr("tabIndex", -1)
				.tooltip()
				.appendTo(this.wrapper)
				.button({
				    icons: {
				        primary: "ui-icon-triangle-1-s"
				    },
				    text: false
				})
				.removeClass("ui-corner-all")
				.addClass("custom-combobox-toggle ui-corner-right")
				.on("mousedown", function () {
				    wasOpen = input.autocomplete("widget").is(":visible");
				})
				.on("click", function () {
				    input.trigger("focus");
				    if (wasOpen) {
				        return;
				    }
				    input.autocomplete("search", "");
				});
        },
        _source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }));
        },
        _removeIfInvalid: function (event, ui) {
            if (ui.item) {
                return;
            }

            var value = this.input.val(),
				valueLowerCase = value.toLowerCase(),
				valid = false;
            this.element.children("option").each(function () {
                if ($(this).text().toLowerCase() === valueLowerCase) {
                    this.selected = valid = true;
                    return false;
                }
            });

            if (valid) {
                return;
            }

            this.input
				.val("")
				.attr("title", value + " didn't match any item")
				.tooltip("open");
            this.element.val("");
            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input.autocomplete("instance").term = "";
        },
        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });
});

function loadOptionalProductCode() {
    $('.IdMenuDishOptionalProductCode').empty();
    $('.IdMenuDishOptionalProductCode').siblings('.loading').removeClass('hidden');

    var filter = [];
    $('.IdMenuDishOptionalProductCode').each(function () {
        var selected = $(this).attr("data-selected");
        if (selected != null && selected.trim() != "" && isInt(selected)) {
            filter.push(parseInt(selected))
        }
    });

    $.ajax({
        url: $("#applicationPath").val() + '/ProductCode/GetList',
        type: 'POST',
        async: true,
        data: JSON.stringify(filter),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('.IdMenuDishOptionalProductCode').append($('<option>').val('').text(''));
            $.each(result, function (key, value) {
                $(".IdMenuDishOptionalProductCode").each(function (key1, value1) {
                    data = $(value1).data("idproductcode");
                    if (data !== value.IdProductCode) {
                        $(this).append($('<option>').val(value.IdProductCode).text(value.Description));
                    }
                });
            });

            $(".IdMenuDishOptionalProductCode").each(function (key1, value1) {
                data = $(value1).data("selected");
                $(value1).find("option").each(function (key, value) {
                    var v = +value.value;
                    if (v === data) {
                        $(value).attr("selected", true);
                    }
                });
            });

            $(".IdMenuDishOptionalProductCode").combobox();
            $('.IdMenuDishOptionalProductCode').siblings('.loading').addClass('hidden');
        }
    });

    //$.get($("#applicationPath").val() + '/ProductCode/GetList', function (result) {
    //	$('.IdMenuDishOptionalProductCode').append($('<option>').val('').text(''));
    //	$.each(result, function (key, value) {
    //		$(".IdMenuDishOptionalProductCode").each(function (key1, value1) {
    //			data = $(value1).data("idproductcode");
    //			if (data !== value.IdProductCode) {
    //				$(this).append($('<option>').val(value.IdProductCode).text(value.Description));
    //			}
    //		});
    //	});

    //	$(".IdMenuDishOptionalProductCode").each(function (key1, value1) {
    //		data = $(value1).data("selected");
    //		$(value1).find("option").each(function (key, value) {
    //			var v = +value.value;
    //			if (v === data) {
    //				$(value).attr("selected", true);
    //			}
    //		});
    //	});

    //	$(".IdMenuDishOptionalProductCode").combobox();
    //	$('.IdMenuDishOptionalProductCode').siblings('.loading').addClass('hidden');
    //});
}

function loadOptionalItemProductCode() {
    $('.IdMenuDishOptionalItemProductCode').empty();
    $('.IdMenuDishOptionalItemProductCode').siblings('.loading').removeClass('hidden');

    var filter = [];
    $('.IdMenuDishOptionalItemProductCode').each(function () {
        var selected = $(this).attr("data-selected");
        if (selected != null && selected.trim() != "" && isInt(selected)) {
            filter.push(parseInt(selected))
        }
    });

    $.ajax({
        url: $("#applicationPath").val() + '/ProductCode/GetList',
        type: 'POST',
        async: true,
        data: JSON.stringify(filter),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('.IdMenuDishOptionalItemProductCode').append($('<option>').val('').text(''));
            $.each(result, function (key, value) {
                $(".IdMenuDishOptionalItemProductCode").each(function (key1, value1) {
                    data = $(value1).data("idproductcode");
                    if (data !== value.IdProductCode) {
                        $(this).append($('<option>').val(value.IdProductCode).text(value.Description));
                    }
                });
            });

            $(".IdMenuDishOptionalItemProductCode").each(function (key1, value1) {
                data = $(value1).data("selected");
                $(value1).find("option").each(function (key, value) {
                    var v = +value.value;
                    if (v === data) {
                        $(value).attr("selected", true);
                    }
                });
            });

            $(".IdMenuDishOptionalItemProductCode").combobox();
            $('.IdMenuDishOptionalItemProductCode').siblings('.loading').addClass('hidden');
        }
    });

    //$.get($("#applicationPath").val() + '/ProductCode/GetList', function (result) {
    //	$('.IdMenuDishOptionalItemProductCode').append($('<option>').val('').text(''));
    //	$.each(result, function (key, value) {
    //		$(".IdMenuDishOptionalItemProductCode").each(function (key1, value1) {
    //			data = $(value1).data("idproductcode");
    //			if (data !== value.IdProductCode) {
    //				$(this).append($('<option>').val(value.IdProductCode).text(value.Description));
    //			}
    //		});
    //	});

    //	$(".IdMenuDishOptionalItemProductCode").each(function (key1, value1) {
    //		data = $(value1).data("selected");
    //		$(value1).find("option").each(function (key, value) {
    //			var v = +value.value;
    //			if (v === data) {
    //				$(value).attr("selected", true);
    //			}
    //		});
    //	});

    //	$(".IdMenuDishOptionalItemProductCode").combobox();
    //	$('.IdMenuDishOptionalItemProductCode').siblings('.loading').addClass('hidden');
    //});
}

function MaskApply() {
    $(".numerico").numeric({ decimal: false, negative: false });
    $('.interval-value').mask('#.##0,00', { reverse: true });
    $('.numeric').mask('00000000000000000000', { reverse: true });
};

function LoadTipo(event, callback) {
    $('#IdMenuAttributeTypeOp').empty();
    $('#IdMenuAttributeTypeOp').siblings('.loading').removeClass('hidden');
    $.get($("#applicationPath").val() + '/MenuDishOptional/GetTipo',
        { idParent: $('#IdMenuAttributeType').val() },
        function (result) {
            $('#IdMenuAttributeTypeOp').append($('<option>').val('').text(''));
            $.each(result, function (key, value) {
                $('#IdMenuAttributeTypeOp').append($('<option>').val(value.IdMenuAttributeType).text(value.Name));
            });
            $('#IdMenuAttributeTypeOp').siblings('.loading').addClass('hidden');
            if (callback) {
                callback();
            }
            setSelectedTipo();
            LoadTipo2();
        });
}


function setSelectedTipo() {

    data = $("#IdMenuAttributeTypeOp").data("selected");

    $("#IdMenuAttributeTypeOp").find("option").each(function (key, value) {

        var v = +value.value;
        if (v === data) {
            $(value).attr("selected", true)
        }

    })

}

function LoadTipo2(event, callback) {

    $('.IdMenuAttributeTypeOpI').empty();
    if (!$('#IdMenuAttributeTypeOp').val()) {
        return;
    }
    $('.IdMenuAttributeTypeOpI').siblings('.loading').removeClass('hidden');
    $.get($("#applicationPath").val() + '/MenuDishOptional/GetTipo',
            { idParent: $('#IdMenuAttributeTypeOp').val() },
        function (result) {
            $('.IdMenuAttributeTypeOpI').append($('<option>').val('').text(''));
            $.each(result, function (key, value) {

                $(".IdMenuAttributeTypeOpI").each(function (key1, value1) {
                    $(this).append($('<option>').val(value.IdMenuAttributeType).text(value.Name));
                });

            });
            $('.IdMenuAttributeTypeOpI').siblings('.loading').addClass('hidden');
            if (callback) {
                callback();
            }
            setSelectedTipo2();
        });
}

function setSelectedTipo2() {


    $(".IdMenuAttributeTypeOpI").each(function (key1, value1) {
        data = $(value1).data("selected");

        $(value1).find("option").each(function (key, value) {


            var v = +value.value;
            if (v === data) {
                $(value).attr("selected", true)
            }

        })

    })
}


function LoadRestriction(select) {
    if (select === undefined) {
        $('.IdMenuDishOptionalItemOfRestriction').empty();
        var vs = $('#IdMenuDishOp').val() !== "" ? $('#IdMenuDishOp').val() : $('#IdMenuDish').val();

        if (!vs) {
            return;
        }

        $.get($("#applicationPath").val() + '/MenuDishOptional/GetOptionalItemRestrictionList',
                { idMenuDish: vs },
            function (result) {
                $('.IdMenuDishOptionalItemOfRestriction').append($('<option>').val('').text(''));

                $.each(result, function (key, value) {
                    $(".IdMenuDishOptionalItemOfRestriction").each(function (key1, value1) {

                        data = $(value1).data("idmenudishoptionalitem");

                        if (data !== value.IdMenuDishOptionalItem) {

                            var title = value.Name;
                            if (value.Description != null && value.Description != "") {
                                title += " - " + value.Description;
                            }

                            var text = title;
                            if (text.length > 35)
                                text = text.substring(0, 35) + "...";

                            $(this).css({ width: "276px" });
                            $(this).append($('<option>').val(value.IdMenuDishOptionalItem).text(text).attr('title', title));
                        }
                    });

                });
                setSelectedRestriction();
            });
    } else {
        select.empty();

        if (!$('#IdMenuDishOp').val()) {
            return;
        }

        $.get($("#applicationPath").val() + '/MenuDishOptional/GetOptionalItemRestrictionList',
                { idMenuDish: $('#IdMenuDishOp').val() },
            function (result) {
                select.append($('<option>').val('').text(''));

                $.each(result, function (key, value) {

                    select.each(function (key1, value1) {

                        data = $(value1).data("idmenudishoptionalitem");

                        if (data !== value.IdMenuDishOptionalItem) {

                            var title = value.Name;
                            if (value.Description != null && value.Description != "") {
                                title += " - " + value.Description;
                            }

                            var text = title;
                            if (text.length > 35)
                                text = text.substring(0, 35) + "...";

                            $(this).css({ width: "276px"});
                            $(this).append($('<option>').val(value.IdMenuDishOptionalItem).text(text).attr('title', title));
                        }
                    });

                });
            });
    }
}

function setSelectedRestriction() {
    $(".IdMenuDishOptionalItemOfRestriction").each(function (key1, value1) {
        data = $(value1).data("selected");

        $(value1).find("option").each(function (key, value) {
            var v = +value.value;
            if (v === data) {
                $(value).attr("selected", true);
            }
        });
    });
}