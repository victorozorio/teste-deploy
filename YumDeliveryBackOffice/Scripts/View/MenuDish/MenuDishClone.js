﻿$(document).ready(function () {
    loadRestaurantsMenus(false);
    $("#statusColumn").html('');
});

$("#restaurants").on("change", function () {
    listMenus($(this).val(), "#menuGroups");
});

$("#restaurantsOrigin").on("change", function () {
    listMenus($(this).val(), "#menuGroupsOrigin");
});

$("#searchButton").on("click", function (e) {
    e.preventDefault();
    filterDishes();
});

$("#cloneButton").on("click", function (e) {
    e.preventDefault();
    cloneDishes();
});

$("#selectAllDishes").on("change", function () {
    if ($(this).is(":checked")) {
        $('.dishCloneCheckbox').prop('checked', true);
    } else {
        $('.dishCloneCheckbox').prop('checked', false);
    }
});

function loadRestaurantsMenus(setvalues) {
    $.ajax({
        url: $("#applicationPath").val() + '/MenuDish/GetRestaurantsWithMenusAndDishes',
        type: 'POST',
        async: true,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            showLoadingOverlay();
        },
        complete: function () {
            hideLoadingOverlay();
        },
        success: function (result) {
            var idRestaurant = $("#restaurants").val();
            var idMenu = $("#menuGroups").val();

            var idRestaurantOrigin = $("#restaurantsOrigin").val();
            var idMenuOrigin = $("#menuGroupsOrigin").val();

            restaurantMenus = result;
            listRestaurants("#restaurants");
            listRestaurants("#restaurantsOrigin");

            if (setvalues) {
                $("#restaurants").val(idRestaurant);
                $("#menuGroups").val(idMenu);
                $("#restaurantsOrigin").val(idRestaurantOrigin);
                $("#menuGroupsOrigin").val(idMenuOrigin);
            }
        }
    });
}

function cloneDishes() {
    var selectedDishes = $(".dishCloneCheckbox:checked");

    if (selectedDishes.length < 1) {
        showMessage("Selecione pelo menos um prato");
        return;
    }

    var selectedDishesIds = [];
    var idMenu = $("#menuGroups").val();

    if (idMenu == null || parseInt(idMenu) < 1) {
        showMessage("Selecione o menu de destino");
        return;
    }

    $.each(selectedDishes, function () {
        var dishId = parseInt($(this).closest("tr").attr("id"));
        selectedDishesIds.push(dishId);
    });

    var filter = {
        IdMenuGroupTo: idMenu,
        IdMenuDishes: selectedDishesIds
    }

    $.ajax({
        url: $("#applicationPath").val() + '/MenuDish/CloneMenuDish',
        type: 'POST',
        data: JSON.stringify(filter),
        async: true,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            showLoadingOverlay();
        },
        error: function (result) {
            hideLoadingOverlay();
            showMessage(result.responseText);
        },
        complete: function () {
            hideLoadingOverlay();
        },
        success: function (result) {
            loadRestaurantsMenus(true);
            clearCloneStatus();
            updateCloneStatus(result);
            showMessage("Processo de clonagem concluído. Verifique o status.");
        }
    });

    $("#statusColumn").html('Status');
    $("td:nth-child(6)").show();
}

function clearCloneStatus() {
    $(".status-column").html("");
}

function updateCloneStatus(result) {
    $.each(result, function () {

        var statusLink = $("<a/>");
        statusLink.attr("message", this.message);
        statusLink.attr("href", "#");
        statusLink.addClass("link-dish-status");
        statusLink.click(function (e) {
            e.preventDefault();
            var statusMessage = $(this).attr("message");
            showMessage(statusMessage);
        });

        var colorLink = "";
        var textLink = "";

        if (this.status == "OK") {
            colorLink = "#00B00B";
            statusLink.html("SUCESSO");
        } else {
            colorLink = "#DF0600";
            statusLink.html("ERRO");
        }

        statusLink.css({
            "color": colorLink,
            "text-weight": "bold"
        });

        var tdId = "#status_" + this.dishId;
        $(tdId).append(statusLink);

    });
}

function filterDishes() {

    var idRestaurant = $("#restaurantsOrigin").val();
    var idMenu = $("#menuGroupsOrigin").val();

    if (parseInt(idRestaurant) < 1) {
        showMessage("Selecione um restaurante");
        return;
    }

    if (parseInt(idMenu) < 1) {
        showMessage("Selecione um menu");
        return;
    }

    $('#selectAllDishes').prop('checked', false);

    var partialDishName = $("#menuDishOriginName").val();

    $("#dishesToClone").find("tr").remove().end();

    $.each(restaurantMenus, function () {
        if (this.restaurantId == idRestaurant) {

            $.each(this.menus, function () {
                if (this.menuId == idMenu) {

                    $.each(this.dishes, function () {

                        if (partialDishName.trim() == "" || String(this.dishName).indexOf(partialDishName) > -1) {
                            var row = $("<tr/>");
                            row.attr("id", this.dishId);
                            row.append("<td><input type='checkbox' id='cloneDish_" + this.dishId + "' class='dishCloneCheckbox'/></td>");
                            row.append("<td>" + this.dishId + "</td>");
                            row.append("<td>" + this.dishName + "</td>");
                            row.append("<td><input type='checkbox' id='dishDisabled_" + this.dishId + "' disabled " + (this.dishDisabled ? "checked" : "") + "/></td>");
                            row.append("<td>" + this.dishMenuName + "</td>");
                            row.append("<td>" + this.dishRestaurantName + "</td>");
                            row.append("<td id='status_" + this.dishId + "' class='status-column'></td>");
                            $("#dishesToClone").append(row);
                        }
                    });
                }
            });
        }
    });
    $("#statusColumn").html('');
    //$("td:nth-child(6)").hide();
}

function listRestaurants(comboboxId) {
    $(comboboxId).find('option').remove().end();
    $(comboboxId).append($("<option/>", { value: "0", text: "" }));
    $.each(restaurantMenus, function (i, item) {
        $(comboboxId).append($("<option/>", { value: item.restaurantId, text: item.restaurantName }));
    });
}

function listMenus(idRestaurant, comboboxId) {
    $(comboboxId).find('option').remove().end();
    $(comboboxId).append($("<option/>", { value: "0", text: "" }));
    $.each(restaurantMenus, function () {
        if (this.restaurantId == idRestaurant) {
            $.each(this.menus, function () {
                $(comboboxId).append($("<option/>", { value: this.menuId, text: this.menuName }));
            });
        }
    });
}

function showLoadingOverlay() {
    if (!$("body").hasClass("disableLoadingOverlay")) {
        $("#loadingOverlay").show();
        $("html, html a, html label").attr("style", "cursor: progress !important");
    }
}

function hideLoadingOverlay() {
    $("html, html a, html label").attr("style", "cursor: ");
    $("#loadingOverlay").hide();
}

function disableLoadingOverlay() {
    $("body").addClass("disableLoadingOverlay");
}

function enableLoadingOverlay() {
    $("body").removeClass("disableLoadingOverlay");
}