﻿var timeEntryImage = "";

$.validator.setDefaults({
    ignore: [],
    });

$.validator.addMethod('specialCharFileName', function (value, element) {
    if (value) {
        var fileName = value.replace(/C:\\fakepath\\/i, '');
        return /^[a-z\u00E0-\u00FCA-Z\u00C0-\u00DC0-9().& _-]*$/.test(fileName);
        }
else {
    return true;
}
}, "O nome do arquivo não pode conter caracteres especiais");

$(document).ready(function () {
    $('#ImageBig').rules('add', { specialCharFileName: true });
    $('#ImageSmall').rules('add', { specialCharFileName: true });

    $('#BannerForm').on('change', '#IdRestaurant', function (event) {
        console.log("aqui");
		LoadMenu();
		LoadPolygonArea();
	});

	timeEntryImage = $("#applicationPath").val() + "/Scripts/timeentry/spinnerDefault.png";
	var $sliderCountIndex = $("#sliderCountIndex").val() - 1;

	$("tbody#abertura").on("click", ".add-abertura", function (e) {
		$sliderCountIndex++;
		NewTimeEntry($(this), $sliderCountIndex, "BannerAvailability");
	});

	$("tbody#abertura").on("click", ".remove-abertura", function (e) {
		$(this).closest('tr').remove();
	});

	$(".timeentry-start").timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 0, 0, 0, 0), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });
	$(".timeentry-end").timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 23, 59, 59, 999), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });

	$(".timeentry-field").timeEntry('disable');

	$(".timeentry-field").bind("change paste keyup", function () {
		UpdateTimeEntry($(this).attr("id"), $(this).val());
	});

	$(".week-day").click(function (event) {
		UpdateChecked($(this));
	});

	$(".timeEntry-control").hide();

	$("#BannerForm").submit(function (event) {
	    var daysSelected = 0;
	    $("#abertura.abertura-class").find("tr").each(function (key, value) {
	        $(value).find("td").find("input:checked").each(function (key1, value1) {
	            daysSelected++;
	        })
	    });
        
	    if (($("#StartDate").val() != null && $("#StartDate").val() != "") && (($("#EndDate").val() != null && $("#EndDate").val() != ""))) {
	        var startDate = $("#StartDate").val().split('/');
	        var endDate = $("#EndDate").val().split('/');

	        var startDateVal = new Date(startDate[2], startDate[1] - 1, startDate[0], 0, 0, 0, 0);
	        var endDateVal = new Date(endDate[2], endDate[1] - 1, endDate[0], 0, 0, 0, 0);

	        if (endDateVal < startDateVal) {

	            $(".periodValidate").removeClass("field-validation-valid").addClass("field-validation-error").html("Data final não pode ser maior que data inicial").show();
	            setTimeout("$(\".periodValidate\").hide();", 4340);

	            hideLoadingOverlay();
	            return false;
	        }
	    }

	    if ($("input[type=file].image-select.valid").length === 1 || daysSelected === 0) {

	        if (daysSelected === 0) {
	            $(".aberturaValidate").removeClass("field-validation-valid").addClass("field-validation-error").html("Horários de Exibição é obrigatorio, Selecione um dia").show();
	            setTimeout("$(\".aberturaValidate\").hide();", 4340);
	            
	        }

	        if (!$("#ImageBig").hasClass("valid")) {
	            $(".image-big-validation").removeClass("field-validation-valid").addClass("field-validation-error").html("A imagem é obrigatória").show();
	            setTimeout("$(\".image-big-validation\").hide();", 4340);
	        }

	        if (!$("#ImageSmall").hasClass("valid")) {
	            $(".image-small-validation").removeClass("field-validation-valid").addClass("field-validation-error").html("A imagem é obrigatória").show();
	            setTimeout("$(\".image-small-validation\").hide();", 4340);
	        }

	        hideLoadingOverlay();
	        return false;
	    }

	});
	
	$(".week-day").each(function (e) {
	    UpdateChecked($(this));
	});

	LoadPolygonArea();
});

function LoadMenu() {
	$('#IdMenuGroup').empty();

	if (!$('#IdRestaurant').val()) {
		return;
	}

	$('#IdMenuGroup').siblings('.loading').removeClass('hidden');

	$.get($("#applicationPath").val() + '/MenuGroup/GetMenuGroup', { RestaurantId: $('#IdRestaurant').val() }, function (result) {
        $('#IdMenuGroup').append($('<option>').val('').text(''));

        $.each(result, function (key, value) {
        	if (value.Description == null) {
        		$('#IdMenuGroup').append($('<option>').val(value.IdMenuGroup).text(value.Name));
        	} else {
        		$('#IdMenuGroup').append($('<option>').val(value.IdMenuGroup).text(value.Name + " (" + value.Description + ")"));
        	}
        });

        $('#IdMenuGroup').siblings('.loading').addClass('hidden');
	});
}

function LoadPolygonArea() {
    console.log("aqui");
    var idSelectedArea = $('#IdPolygonArea').val();
    if (idSelectedArea == null || idSelectedArea == "") {
        idSelectedArea = 0;
    }
	$('#IdPolygonArea').empty();

	if (!$('#IdRestaurant').val()) {
		return;
	}

	$('#IdPolygonArea').siblings('.loading').removeClass('hidden');

	$.get($("#applicationPath").val() + '/KML/GetPolygonAreaByRestaurant', { selectedAreaId: idSelectedArea, idRestaurant: $('#IdRestaurant').val() }, function (result) {
		$('#IdPolygonArea').append($('<option>').val('').text('Todas'));

		$.each(result, function (key, value) {
			$('#IdPolygonArea').append($('<option>').val(value.IdPolygonArea).text(value.Name));
		});

		$('#IdPolygonArea').siblings('.loading').addClass('hidden');

		$('#IdPolygonArea').val(idSelectedArea == 0 ? '' : idSelectedArea);
	});
}

function NewTimeEntry(element, counter, list) {
	$weekDay = element.closest('tbody').data("weekday");
	$currentItem = element.closest("tr");
	$currentCheckbox = $currentItem.find(".week-day");

	$clone = $currentItem.clone();

	$cloneDayLabel = $clone.find(".dayLabel");
	$cloneDayLabel.css("display", "none");

	$clone.find("#sliderAction").removeClass("add-link add-abertura").addClass("table-delete-link remove-abertura");

	$cloneCheckbox = $clone.find(".week-day");
	$cloneCheckbox.attr('name', list + "[" + counter + "].WeekDay");
	$cloneCheckbox.hide();

	$divMinValue = $clone.find("div.inline-timeentry-start");
	$divMinValue.children().remove();

	$inputMinValue = $("<input/>");
	$inputMinValue.attr('id', list + "_StartTime_" + counter);
	$inputMinValue.attr('data-val', 'true');
	$inputMinValue.attr('data-val-required', 'O campo Hora de Início é obrigatório.');
	$inputMinValue.attr('type', 'text');
	$inputMinValue.addClass("timeentry-start timeentry-field");
	$inputMinValue.css("width", "100px");
	$inputMinValue.appendTo($divMinValue);
	$inputMinValue.timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 0, 0, 0, 0), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });
	$inputMinValue.val("00:00");

	$inputMinValue.bind("change paste keyup", function () {
		UpdateTimeEntry($(this).attr("id"), $(this).val());
	});

	$inputHiddenMinValue = $("<input/>");
	$inputHiddenMinValue.attr('id', list + "_" + counter + "__StartTime");
	$inputHiddenMinValue.attr('name', list + "[" + counter + "].StartTime");
	$inputHiddenMinValue.attr('data-val', 'true');
	$inputHiddenMinValue.attr('data-val-required', 'O campo Hora de Início é obrigatório.');
	$inputHiddenMinValue.attr('type', 'hidden');
	$inputHiddenMinValue.addClass("minValue");
	$inputHiddenMinValue.appendTo($divMinValue);
	$inputHiddenMinValue.val("00:00:00.000000");

	$divMaxValue = $clone.find("div.inline-timeentry-end");
	$divMaxValue.children().remove();

	$inputMaxValue = $("<input/>");
	$inputMaxValue.attr('id', list + "_EndTime_" + counter);
	$inputMaxValue.attr('data-val', 'true');
	$inputMaxValue.attr('data-val-required', 'O campo Hora de Término é obrigatório.');
	$inputMaxValue.attr('type', 'text');
	$inputMaxValue.addClass("timeentry-end timeentry-field");
	$inputMaxValue.css("width", "100px");
	$inputMaxValue.appendTo($divMaxValue);
	$inputMaxValue.timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 23, 59, 59, 999), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });
	$inputMaxValue.val("23:59");

	$inputMaxValue.bind("change paste keyup", function () {
		UpdateTimeEntry($(this).attr("id"), $(this).val());
	});

	$inputHiddenMaxValue = $("<input/>");
	$inputHiddenMaxValue.attr('id', list + "_" + counter + "__EndTime");
	$inputHiddenMaxValue.attr('name', list + "[" + counter + "].EndTime");
	$inputHiddenMaxValue.attr('data-val', 'true');
	$inputHiddenMaxValue.attr('data-val-required', 'O campo Hora de Término é obrigatório.');
	$inputHiddenMaxValue.attr('type', 'hidden');
	$inputHiddenMaxValue.addClass("maxValue");
	$inputHiddenMaxValue.appendTo($divMaxValue);
	$inputHiddenMaxValue.val("23:59:59.999999");

	if ($currentCheckbox.is(':checked')) {
		var x = $cloneCheckbox.find(".week-day").attr("checked", true);
		$inputMinValue.timeEntry('enable');
		$inputMaxValue.timeEntry('enable');
	} else {
		$inputMinValue.timeEntry('disable');
		$inputMaxValue.timeEntry('disable');
	}

	$clone.show();

	element.closest('tbody').append($clone);
	$(".timeEntry-control").hide();
}

function UpdateTimeEntry(fieldId, value) {
	var timeentryID = fieldId.split("_");
	var seconds = "";
	if (timeentryID[1] == "StartTime") {
		seconds = ":00.000000";
	} else {
		seconds = ":59.999999";
	}
	$("#" + timeentryID[0] + "_" + timeentryID[2] + "__" + timeentryID[1]).val(value + seconds);
}

function UpdateChecked(checkbox) {
	var $tbody = checkbox.closest('tbody');
	if (checkbox.is(':checked')) {
		$tbody.find(".timeentry-field").timeEntry('enable');
	} else {
		$tbody.find(".timeentry-field").timeEntry('disable');
	}

	var name = checkbox.attr("name");
	$tbody.find("tr").each(function (key, value) {
		$(value).find("td").find("input[type=checkbox]").each(function (key1, value1) {
			var $otherCheckbox = $(value1);
			if (name != $otherCheckbox.attr("name")) {
				$otherCheckbox.prop('checked', checkbox.is(':checked'));
			}
		})
	});
}