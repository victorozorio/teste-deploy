﻿$(function () {
    $("#todosCheckBox").change(function () {
        $("input[name='selectedGroups']").each(function () {
            $(this).prop("checked", $("#todosCheckBox").is(":checked"))
        });
    });

    $("#linkLojas").click(function (e) {
        e.preventDefault();

        rowCount = $('#Lojas li').last().attr("class");
        rowCount = parseInt(rowCount) + 1;

        $.get($('#applicationPath').val() + '/BOAccount/UserRestaurantControl', { field: rowCount }, function (result) {
            $("#Lojas").append(result)
        });
    });

    $('#CreateUserForm').submit(function (e) {
        validateSubmit(e);
    });

    $('#EditUserForm').submit(function (e) {
        validateSubmit(e);
    });

    $('#Lojas').on('change', 'select[name=selectedRestaurants]', function (e) {
        var selectedCount = 0;

        var selectedValue = $(this).val();

        $('select[name=selectedRestaurants]').each(function (index, element) {
            if ($(element).val() == selectedValue) {
                selectedCount++;
            }
        });

        if (selectedCount > 1) {
            showMessage("Loja já foi selecionada.", "ATENÇÃO", "warning");

            $(this).val('');
        }
    });
});

function validateSubmit(e) {
    var $restaurantSelectValidation = $('#restaurantSelectValidation');

    if (!checkSelectedRestaurant()) {
        e.preventDefault();

        $restaurantSelectValidation.show();
    } else {
        $restaurantSelectValidation.hide();
    }
}

function checkSelectedRestaurant() {
    var $selectList = $('select[name=selectedRestaurants]');

    var hasRestaurantSelected = false;

    $selectList.each(function (index, $select) {
        if (this.value != '') {
            hasRestaurantSelected = true;

            return false;
        }
    });

    return hasRestaurantSelected;
}