﻿var timeEntryImage = "";
$(document).ready(function () {
    $(".hiddenLabel").hide();
    timeEntryImage = $("#applicationPath").val() + "/Scripts/timeentry/spinnerDefault.png";

    $(".timeentry-start").timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 0, 0, 0, 0), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });
    $(".timeentry-end").timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 23, 59, 59, 999), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });

    $(".timeentry-field").timeEntry('disable');

    $(".timeEntry-control").hide();
});