﻿$(document).ready(function () {
    $(".taxaSelected").click(function () {

        if ($(this).is(':checked')) {
            var b = +$(this).data("check");

            var c = $("input#RestaurantDeliveryFeeRangeList_" + (b - 1) + "__Selected");

            if (!c.is(":checked") && c.length > 0) {
                $(this).attr("checked", false)
                alert("Selecione o Anterior")

            } else {
                $(this).parent().find("input.entrega").each(function (key, value) {
                    $(value).removeAttr('disabled')
                });
                AtualizaTaxa();
            }
        } else {

            var b = +$(this).data("check");
            var c = $("input#RestaurantDeliveryFeeRangeList_" + (b + 1) + "__Selected");

            if (c.is(":checked") && c.length > 0) {
                $(this).prop("checked", true)
                alert("Não e possivel remover seleção, primeiro remova do checkbox abaixo")

            } else {
                $(this).parent().find("input.entrega").each(function (key, value) {
                    $(value).prop('disabled', true)
                });
                AtualizaTaxa();
            }
        }
    });

    $(".week-day").each(function (event) {
        UpdateChecked($(this));
    });
});

function AtualizaTaxa() {

    $("input#RestaurantDeliveryFeeRangeList_1__ValueFrom").val(+$("input#RestaurantDeliveryFeeRangeList_0__ToValue").val() + 1)

    var check = $("input#RestaurantDeliveryFeeRangeList_0__ToValue").val().length;
    if (check !== 0) {
        $("input#RestaurantDeliveryFeeRangeList_2__ValueFrom").val(+$("input#RestaurantDeliveryFeeRangeList_1__ToValue").val() + 1)
    }

}