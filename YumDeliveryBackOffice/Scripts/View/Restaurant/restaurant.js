﻿var timeEntryImage = "";

$.validator.setDefaults({
    ignore: [],
});

$.validator.addMethod('unique', function (value, element) {
    var isUnique = false;
    var idrestaurant = $("#IdRestaurant").val();
    if (idrestaurant === undefined) {
        idrestaurant = 0;
    }
    $.ajax({
        url: $("#actionUniqueRestaurant").val(),
        type: 'GET',
        async: false,
        data: { uniqueName: value, idRestaurant: idrestaurant },
        success: function (result) {
            isUnique = result;
            return result;
        }
    });
    HideLabels();
    return isUnique;
}, "Já existe uma loja cadastrada com esse nome");

$.validator.addMethod('specialCharFileName', function (value, element) {
    if (value) {
        var fileName = value.replace(/C:\\fakepath\\/i, '');
        return /^[a-z\u00E0-\u00FCA-Z\u00C0-\u00DC0-9().& _-]*$/.test(fileName);
    }
    else {
        return true;
    }
}, "O nome do arquivo não pode conter caracteres especiais");

$(document).ready(function () {

    timeEntryImage = $("#applicationPath").val() + "/Scripts/timeentry/spinnerDefault.png";

    timeEntryImage = $("#applicationPath").val() + "/Scripts/timeentry/spinnerDefault.png";

    var inputImageFile = $('input[name=\'ImageFile\']');

    if (inputImageFile.length > 0) {
        inputImageFile.rules('add', { specialCharFileName: true });
    }

    if ($('#UniqueName').length) {
        $('#UniqueName').rules('add', { unique: true });
    }

    $('.interval-value').mask('#.##0,00', { reverse: true });

    checkRules();

    $('#IdIntegrationTechnology').on('change', function (event) {
        if (!$('#IdIntegrationTechnology').val()) {
            return;
        }

        if ($('#IdIntegrationTechnology').val() == 2) {
            $('#IntegrationFallbackReceptor').prop("disabled", true);
            $('#IntegrationFallbackReceptor').prop("checked", false);
        } else {
            $('#IntegrationFallbackReceptor').prop("disabled", false);
        }
    });

    if ($("#MaximumOrderChange").length) {
        $("#MaximumOrderChange").keyup(function () {

            var x = +ConverterPriceFormatToDouble($(this).val());
            if (x > 0) {
                $(this).rules('add', { required: true })
            } else {
                $(this).rules('remove', 'required')

            }
        });
    }

    if ($("#ExternalName").length) {
        $("#ExternalName").keyup(function () {

            var x = $(this).val().length
            var y = $("#ExternalLink").val().length

            if (x > 0 || y > 0) {
                $(this).rules('add', { required: true })
                $("#ExternalLink").rules('add', { required: true })
            } else {
                $(this).rules('add', { required: false })
                if (y === 0) {
                    $("#ExternalLink").rules('remove', 'required')
                }
                $(this).next().html("");
                $("#ExternalLink").next().html("");
                $(this).next().switchClass("field-validation-error", "field-validation-valid", 0);
                $("#ExternalLink").next().switchClass("field-validation-error", "field-validation-valid", 0);
            }

        });
    }

    if ($("#ExternalLink").length) {
        $("#ExternalLink").keyup(function () {

            var x = $(this).val().length
            var y = $("#ExternalName").val().length

            if (x > 0 || y > 0) {
                $(this).rules('add', { required: true })
                $("#ExternalName").rules('add', { required: true })
            } else {
                $(this).rules('remove', 'required')

                if (y === 0) {
                    $("#ExternalName").rules('remove', 'required')
                }
                $(this).next().html("");
                $("#ExternalName").next().html("");
                $(this).next().switchClass("field-validation-error", "field-validation-valid", 0);
                $("#ExternalName").next().switchClass("field-validation-error", "field-validation-valid", 0);
            }

        });
    }


    $('#RestaurantForm').on('change', '#StateCode', function (event) {
        LoadCities(event);
        $('#Address_StateCode').val($(this).children('option:selected').text()).valid();
    });

    $('#RestaurantForm').on('change', '#City', function (event) {
        LoadNeighborhoods(event);
        $('#Address_City').val($(this).children('option:selected').text()).valid();
    });

    $('#RestaurantForm').on('change', '#Neighborhood', function (event) {
        $('#Address_Neighborhood').val($(this).children('option:selected').text()).valid();
    });

    var $sliderCountIndex = $("#sliderCountIndex").val() - 1;
    var $sliderCountIndexTogo = $("#sliderCountIndexTogo").val() - 1;

    $('#RestaurantForm').on('click', '.add-link', function (event) {
        var $closestLi = $(this).closest('li'), $clone;
        if ($(this).is('.add-email')) {

            $clone = $closestLi.children('span:first').clone();
            $clone.css('margin-left', '160px');
            var i = $closestLi.find('[id$="EmailVM"]').length;
            $clone.append("<br />");
            $.each($clone.children(), function (index, value) {
                if (index == 0) {
                    $(value).attr('id', 'EmailList_' + i + '__EmailVM')
                        .attr('name', 'EmailList[' + i + '].EmailVM');
                    $(value).val('');
                } else if (index == 1) {
                    $(value).attr('id', 'EmailList_' + i + '__ReceiveReports')
                        .attr('name', 'EmailList[' + i + '].ReceiveReports')
                        .val('').attr('checked', false);
                } else if (index == 3) {
                    $(value).attr('for', 'EmailList_' + i + '__ReceiveReports')
                }
            });
        } else {
            $clone = $closestLi.children('span:first').clone();
            var i = $closestLi.find('[id$="AreaCodeVM"]').length;

            $clone.css("margin-left", 160)
            $clone.append("<br />")
            $.each($clone.children(), function (key, value) {
                if (key === 0) {
                    $(value).attr('id', 'PhoneList_' + i + '__AreaCodeVM')
                        .attr('name', 'PhoneList[' + i + '].AreaCodeVM');
                    $(value).val('');
                } else if (key === 1) {
                    $(value).attr('id', 'PhoneList_' + i + '__NumberVM')
                        .attr('name', 'PhoneList[' + i + '].NumberVM');
                    $(value).val('');
                }
            });
        }

        $closestLi.append($clone);
    });

    $(document).on('change', '.checkbox-update', function () {
        var name = $(this).attr('name');
        var isChecked = $(this).is(':checked');
        var $elements = $('[name="' + name + '"]');
        $elements.val(isChecked ? 'true' : 'false');
    });

    $("tbody#abertura").on("click", ".add-abertura", function (e) {
        $sliderCountIndex++;
        NewTimeEntry($(this), $sliderCountIndex, "RestaurantOpenTimeList");
    });

    $("tbody#abertura").on("click", ".remove-abertura", function (e) {
        $(this).closest('tr').remove();
    });

    $("tbody#aberturaTogo").on("click", ".add-abertura", function (e) {
        $sliderCountIndexTogo++;
        NewTimeEntry($(this), $sliderCountIndexTogo, "RestaurantTogoTimeList");
    });

    $("tbody#aberturaTogo").on("click", ".remove-abertura", function (e) {
        $(this).closest('tr').remove();
    });

    $(".taxa0").keyup(function () {

        $("input#RestaurantDeliveryFeeRangeList_1__ValueFrom").val(+$(this).val() + 1)

    });

    $(".taxa1").keyup(function () {
        var check = $("input#RestaurantDeliveryFeeRangeList_0__ToValue").val().length;
        if (check !== 0) {
            $("input#RestaurantDeliveryFeeRangeList_2__ValueFrom").val(+$(this).val() + 1)
        }
    });

    $("#RestaurantForm").submit(function (event) {
        $(".aberturaValidate").hide();
        $(".aberturaValidateTogo").hide();
        $(".pagamentoValidate").hide();

        var countHR = 0;
        var countTG = 0;

        $("#abertura.abertura-class").find("tr").each(function (key, value) {
            $(value).find("td").find("input:checked").each(function (key1, value1) {
                countHR++;
            })
        });

        $("#aberturaTogo.abertura-class").find("tr").each(function (key, value) {
            $(value).find("td").find("input:checked").each(function (key1, value1) {
                countTG++;
            })
        });

        if ((countHR === 0 && countTG === 0) || ($('#AcceptToGo').is(":checked") && countTG === 0)) {

            if (countHR === 0 && countTG === 0) {
                $(".aberturaValidate").removeClass("field-validation-valid").addClass("field-validation-error").html("Dia de Abertura é obrigatorio, Selecione um dia").show();
                $(".aberturaValidateTogo").removeClass("field-validation-valid").addClass("field-validation-error").html("Dia de Abertura é obrigatorio, Selecione um dia").show();
            }

            if (($('#AcceptToGo').is(":checked") && countTG === 0)) {
                $(".aberturaValidateTogo").removeClass("field-validation-valid").addClass("field-validation-error").html("Dia de Abertura é obrigatorio, Selecione um dia").show();
            }

            hideLoadingOverlay();
            HideLabels();
            return false;
        }
    });

    $('#FindAddress').click(function (event) {
        event.preventDefault();
        if (!$('#Address_ZipCode').val()) {
            return;
        }

        $.get($(this).attr('href'), { zipCode: $('#Address_ZipCode').val() }, function (result) {
            if (result) {
                $('#Address_IdCity').val(result.IdCity);
                $('#Address_City').val(result.City);
                if (result.Address) {
                    $('#Address_Address').prop('readonly', true);
                } else {
                    $('#Address_Address').prop('readonly', false);
                }
                $('#Address_Address').val(result.Address);
                if (result.Neighborhood) {
                    $('#Address_Neighborhood').prop('readonly', true);
                } else {
                    $('#Address_Neighborhood').prop('readonly', false);
                }
                $('#Address_Neighborhood').val(result.Neighborhood);
                $('#Address_StateCode').val(result.StateCode);
            }
        });
    });

    $('#AcceptToGo').change(function (event) {
        var $this = $(this);
        if ($this.is(':checked')) {
            $("#ToGoOnlinePayOnly").removeAttr('disabled');
            $("#RestaurantTogoTimeListTable .week-day").removeAttr('disabled');
        } else {
            $("#ToGoOnlinePayOnly").attr('disabled', 'disabled');
            $("#ToGoOnlinePayOnly").prop("checked", false);
            $("#RestaurantTogoTimeListTable .week-day").attr('disabled', 'disabled');
            $("#RestaurantTogoTimeListTable .week-day").each(function () {
                $checkbox = $(this);
                $checkbox.prop("checked", false);
                UpdateChecked($checkbox);
            });
        }
    });
    $("#AcceptToGo").trigger("change");

    $(".timeentry-start").timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 0, 0, 0, 0), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });
    $(".timeentry-end").timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 23, 59, 59, 999), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });

    $(".timeentry-field").timeEntry('disable');

    $(".timeentry-field").bind("change paste keyup", function () {
        UpdateTimeEntry($(this).attr("id"), $(this).val());
    });

    $(".week-day").click(function (event) {
        UpdateChecked($(this));
    });

    $(".timeEntry-control").hide();

    $("#IncludeOrderDeliveryFee").click(toggleEnableSeparateDeliveryFeeMessage);
    toggleEnableSeparateDeliveryFeeMessage();
});

function UpdateChecked(checkbox) {
    var $tbody = checkbox.closest('tbody');
    if (checkbox.is(':checked')) {
        $tbody.find(".timeentry-field").timeEntry('enable');
    } else {
        $tbody.find(".timeentry-field").timeEntry('disable');
    }

    var name = checkbox.attr("name");
    $tbody.find("tr").each(function (key, value) {
        $(value).find("td").find("input[type=checkbox]").each(function (key1, value1) {
            var $otherCheckbox = $(value1);
            if (name !== $otherCheckbox.attr("name")) {
                $otherCheckbox.prop('checked', checkbox.is(':checked'));
            }
        })
    });
}

function UpdateTimeEntry(fieldId, value) {
    var timeentryID = fieldId.split("_");
    var seconds = "";
    if (timeentryID[1] === "StartTime") {
        seconds = ":00.000000";
    } else {
        seconds = ":59.999999";
    }
    $("#" + timeentryID[0] + "_" + timeentryID[2] + "__" + timeentryID[1]).val(value + seconds);
}

function NewTimeEntry(element, counter, list) {

    $weekDay = element.closest('tbody').data("weekday");
    $currentItem = element.closest("tr");
    $currentCheckbox = $currentItem.find(".week-day");

    $clone = $currentItem.clone();

    $cloneDayLabel = $clone.find(".dayLabel");
    $cloneDayLabel.css("display", "none");

    $clone.find("#sliderAction").removeClass("add-link add-abertura").addClass("table-delete-link remove-abertura");

    $cloneCheckbox = $clone.find(".week-day");
    $cloneCheckbox.attr('name', list + "[" + counter + "].WeekDay");
    $cloneCheckbox.hide();

    $divMinValue = $clone.find("div.inline-timeentry-start");
    $divMinValue.children().remove();

    $inputMinValue = $("<input/>");
    $inputMinValue.attr('id', list + "_StartTime_" + counter);
    $inputMinValue.attr('data-val', 'true');
    $inputMinValue.attr('data-val-required', 'O campo Hora de Abertura é obrigatório.');
    $inputMinValue.attr('type', 'text');
    $inputMinValue.addClass("timeentry-start timeentry-field");
    $inputMinValue.css("width", "100px");
    $inputMinValue.appendTo($divMinValue);
    $inputMinValue.timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 0, 0, 0, 0), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });
    $inputMinValue.val("00:00");
    $inputMinValue.bind("change paste keyup", function () {
        UpdateTimeEntry($(this).attr("id"), $(this).val());
    });

    $inputHiddenMinValue = $("<input/>");
    $inputHiddenMinValue.attr('id', list + "_" + counter + "__StartTime");
    $inputHiddenMinValue.attr('name', list + "[" + counter + "].StartTime");
    $inputHiddenMinValue.attr('data-val', 'true');
    $inputHiddenMinValue.attr('data-val-required', 'O campo Hora de Abertura é obrigatório.');
    $inputHiddenMinValue.attr('type', 'hidden');
    $inputHiddenMinValue.addClass("minValue");
    $inputHiddenMinValue.appendTo($divMinValue);
    $inputHiddenMinValue.val("00:00:00.000000");

    $divMaxValue = $clone.find("div.inline-timeentry-end");
    $divMaxValue.children().remove();

    $inputMaxValue = $("<input/>");
    $inputMaxValue.attr('id', list + "_EndTime_" + counter);
    $inputMaxValue.attr('data-val', 'true');
    $inputMaxValue.attr('data-val-required', 'O campo Hora de Abertura é obrigatório.');
    $inputMaxValue.attr('type', 'text');
    $inputMaxValue.addClass("timeentry-end timeentry-field");
    $inputMaxValue.css("width", "100px");
    $inputMaxValue.appendTo($divMaxValue);
    $inputMaxValue.timeEntry({ show24Hours: true, spinnerImage: timeEntryImage, defaultTime: new Date(0, 0, 0, 23, 59, 59, 999), spinnerTexts: ['Padrão', 'Anterior', 'Próximo', 'Aumentar', 'Diminuir'] });
    $inputMaxValue.val("23:59");
    $inputMaxValue.bind("change paste keyup", function () {
        UpdateTimeEntry($(this).attr("id"), $(this).val());
    });

    $inputHiddenMaxValue = $("<input/>");
    $inputHiddenMaxValue.attr('id', list + "_" + counter + "__EndTime");
    $inputHiddenMaxValue.attr('name', list + "[" + counter + "].EndTime");
    $inputHiddenMaxValue.attr('data-val', 'true');
    $inputHiddenMaxValue.attr('data-val-required', 'O campo Hora de Abertura é obrigatório.');
    $inputHiddenMaxValue.attr('type', 'hidden');
    $inputHiddenMaxValue.addClass("maxValue");
    $inputHiddenMaxValue.appendTo($divMaxValue);
    $inputHiddenMaxValue.val("23:59:59.999999");

    if ($currentCheckbox.is(':checked')) {
        var x = $cloneCheckbox.find(".week-day").attr("checked", true);
        $inputMinValue.timeEntry('enable');
        $inputMaxValue.timeEntry('enable');
    } else {
        $inputMinValue.timeEntry('disable');
        $inputMaxValue.timeEntry('disable');
    }

    $clone.show();

    element.closest('tbody').append($clone);
    $(".timeEntry-control").hide();
}

function LoadCities(event, callback) {
    $('#City').empty();
    $('#Neighborhood').empty();
    if (!$('#StateCode').val()) {
        return;
    }
    $('#City').siblings('.loading').removeClass('hidden');
    $.get($("#actionGetCities").val(), { idState: $('#StateCode').val() }, function (result) {
        $('#City').append($('<option>').val('').text(''));
        $.each(result, function (key, value) {
            $('#City').append($('<option>').val(value.IdCity).text(value.Name));
        });
        $('#City').siblings('.loading').addClass('hidden');
        if (callback) {
            callback();
        }
    });
}

function LoadNeighborhoods(event, callback) {
    $('#Neighborhood').empty();
    if (!$('#City').val()) {
        return;
    }
    $('#Neighborhood').siblings('.loading').removeClass('hidden');
    $.get($("#actionGetNeighborhoods").val(), { idCity: $('#City').val() }, function (result) {
        $('#Neighborhood').append($('<option>').val('').text(''));
        $.each(result, function (key, value) {
            $('#Neighborhood').append($('<option>').val(value.IdNeighborhood).text(value.Name));
        });
        $('#Neighborhood').siblings('.loading').addClass('hidden');
        if (callback) {
            callback();
        }
    });
}

function checkRules() {

    var maxInput = +ConverterPriceFormatToDouble($("#MaximumOrderChange").val());
    if (maxInput > 0) {
        $("#MaximumOrderChange").rules('add', { required: true })
    }

    if ($("#ExternalName").length && $("#ExternalLink").length) {
        var externalNameInput = $("#ExternalName").val().length
        var externalLinkInput = $("#ExternalLink").val().length

        if (externalNameInput > 0 || externalLinkInput > 0) {
            $("#ExternalName").rules('add', { required: true })
            $("#ExternalLink").rules('add', { required: true })
        }
    }
}

function HideLabels() {
    $(".week-day[style*='display:none']").each(function () {
        $(this).closest("span").find(".dayLabel").hide();
    });
}

function toggleEnableSeparateDeliveryFeeMessage() {
    if (!$('#IncludeOrderDeliveryFee').is(':checked')) {
        $('#SeparateDeliveryFeeMessage').show();
        $('#alertSeparateDeliveryFeeMessage').show();   
    }
    else {
        $('#SeparateDeliveryFeeMessage').hide();
        $('#alertSeparateDeliveryFeeMessage').hide();        
    }
}