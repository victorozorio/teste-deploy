﻿$(document).ready(function () {
    $(".taxaSelected").click(function () {

        if ($(this).is(':checked')) {
            var b = +$(this).data("check");

            var c = $("input#RestaurantDeliveryFeeRangeList_" + (b - 1) + "__Selected");

            if (!c.is(":checked") && c.length > 0) {
                $(this).attr("checked", false)
                alert("Selecione o Anterior")

            } else {
                $(this).parent().find("input.entrega").each(function (key, value) {
                    $(value).removeAttr('disabled')
                });
            }
        } else {

            var b = +$(this).data("check");
            var c = $("input#RestaurantDeliveryFeeRangeList_" + (b + 1) + "__Selected");

            if (c.is(":checked") && c.length > 0) {
                $(this).prop("checked", true)
                alert("Não e possivel remover seleção, primeiro remova do checkbox abaixo")

            } else {
                $(this).parent().find("input.entrega").each(function (key, value) {
                    $(value).prop('disabled', true)
                });
            }
        }
    });

    $(".timeentry-start").val("00:00");
    $(".timeentry-end").val("23:59");
    $("#SeparateDeliveryFeeMessage").val("A taxa de entrega deste pedido é de {taxa} e deverá ser paga diretamente ao entregador ao receber o pedido, em dinheiro ou cartão de débito.");
});