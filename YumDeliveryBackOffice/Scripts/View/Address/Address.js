﻿$(document).ready(function () {
    $.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<span>")
				.addClass("custom-combobox")
				.insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
				value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
				.appendTo(this.wrapper)
				.val(value)
				.attr("title", "")
				.addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
				.css({ width: "320px" })
				.autocomplete({
				    delay: 0,
				    minLength: 0,
				    source: $.proxy(this, "_source")
				})
				.tooltip({
				    classes: {
				        "ui-tooltip": "ui-state-highlight"
				    }
				});

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function () {
            var input = this.input,
				wasOpen = false;

            $("<a>")
				.attr("tabIndex", -1)
				.tooltip()
				.appendTo(this.wrapper)
				.button({
				    icons: {
				        primary: "ui-icon-triangle-1-s"
				    },
				    text: false
				})
				.removeClass("ui-corner-all")
				.addClass("custom-combobox-toggle ui-corner-right")
				.on("mousedown", function () {
				    wasOpen = input.autocomplete("widget").is(":visible");
				})
				.on("click", function () {
				    input.trigger("focus");
				    if (wasOpen) {
				        return;
				    }
				    input.autocomplete("search", "");
				});
        },

        _source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }));
        },

        _removeIfInvalid: function (event, ui) {
            if (ui.item) {
                return;
            }

            var value = this.input.val(),
				valueLowerCase = value.toLowerCase(),
				valid = false;
            this.element.children("option").each(function () {
                if ($(this).text().toLowerCase() === valueLowerCase) {
                    this.selected = valid = true;
                    return false;
                }
            });

            if (valid) {
                return;
            }

            this.input
				.val("")
				.attr("title", value + " didn't match any item")
				.tooltip("open");
            this.element.val("");
            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input[0].autocomplete("instance").term = "";
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });

});
    
function isInt(value) {
    return !isNaN(value) && (function (x) { return (x | 0) === x; })(parseInt(value))
}

function loadRegion() {

    $('.ComboboxRegion').empty();
    $('.ComboboxRegion').siblings('.loading').removeClass('hidden');

    var filter = [];
    $('.ComboboxRegion').each(function () {
        var selected = $(this).attr("data-selected");
        if (selected != null && selected.trim() != "" && isInt(selected)) {
            filter.push(parseInt(selected))
        }
    });

    $.ajax({
        url: $("#applicationPath").val() + '/Region/GetList',
        type: 'POST',
        async: true,
        data: JSON.stringify(filter),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('.ComboboxRegion').append($('<option>').val('').text(''));
            $.each(result, function (key, value) {
                $(".ComboboxRegion").each(function (key1, value1) {
                    data = $(value1).data("IdRegion");
                    if (data !== value.IdRegion) {
                        $(this).append($('<option>').val(value.IdRegion).text(value.Name));
                    }
                });
            });

            $(".ComboboxRegion").each(function (key1, value1) {
                data = $(value1).data("selected");
                $(value1).find("option").each(function (key, value) {
                    var v = +value.value;
                    if (v === data) {
                        $(value).attr("selected", true);
                    }
                });
            });

            $(".ComboboxRegion").combobox();
            $('.ComboboxRegion').siblings('.loading').addClass('hidden');
        }
    });

}