﻿$(document).ready(function () {
	$.widget("custom.combobox", {
		_create: function () {
			this.wrapper = $("<span>")
				.addClass("custom-combobox")
				.insertAfter(this.element);

			var attr = this.element.attr('data-disabled');
			var disabled = (typeof attr !== typeof undefined && attr !== false);

			this.element.hide();
			this._createAutocomplete(disabled);

			if (disabled) {
			    this.element.attr("disabled", true);
			} else {
			    this._createShowAllButton();
			}
		},

		_createAutocomplete: function (disabled) {
			var selected = this.element.children(":selected"),
				value = selected.val() ? selected.text() : "";

			this.input = $("<input>")
				.appendTo(this.wrapper)
				.val(value)
				.attr("title", "")
                .attr("disabled", disabled)
				.addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
				.css({ width: "320px" })
				.autocomplete({
					delay: 0,
					minLength: 0,
					source: $.proxy(this, "_source"),
                    disabled: disabled
				})
				.tooltip({
					classes: {
						"ui-tooltip": "ui-state-highlight"
					}
				});

			this._on(this.input, {
				autocompleteselect: function (event, ui) {
					ui.item.option.selected = true;
					this._trigger("select", event, {
						item: ui.item.option
					});
				},

				autocompletechange: "_removeIfInvalid"
			});
		},

		_createShowAllButton: function () {
			var input = this.input,
				wasOpen = false;

			$("<a>")
				.attr("tabIndex", -1)
				.tooltip()
				.appendTo(this.wrapper)
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				})
				.removeClass("ui-corner-all")
				.addClass("custom-combobox-toggle ui-corner-right")
				.on("mousedown", function () {
					wasOpen = input.autocomplete("widget").is(":visible");
				})
				.on("click", function () {
					input.trigger("focus");
					if (wasOpen) {
						return;
					}
					input.autocomplete("search", "");
				});
		},

		_source: function (request, response) {
			var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
			response(this.element.children("option").map(function () {
				var text = $(this).text();
				if (this.value && (!request.term || matcher.test(text)))
					return {
						label: text,
						value: text,
						option: this
					};
			}));
		},

		_removeIfInvalid: function (event, ui) {
			if (ui.item) {
				return;
			}

			var value = this.input.val(),
				valueLowerCase = value.toLowerCase(),
				valid = false;
			this.element.children("option").each(function () {
				if ($(this).text().toLowerCase() === valueLowerCase) {
					this.selected = valid = true;
					return false;
				}
			});

			if (valid) {
				return;
			}

			this.input
				.val("")
				.attr("title", value + " didn't match any item")
				.tooltip("open");
			this.element.val("");
			this._delay(function () {
				this.input.tooltip("close").attr("title", "");
			}, 2500);
			this.input.autocomplete("instance").term = "";
		},

		_destroy: function () {
			this.wrapper.remove();
			this.element.show();
		}
	});

	$("#combobox").combobox();
	$("#toggle").on("click", function () {
		$("#combobox").toggle();
	});

    $('#MenuDishForm').on('change', '#Restaurant_IdRestaurant', function (event) {
        LoadMenu(event);
        $('#IdMenuGroup').val($(this).children('option:selected').text()).valid();
    });
   
    $('#MenuDishSearchForm').on('change', '#Restaurant_IdRestaurant', function (event) {
        LoadMenu(event);
        $('#IdMenuGroup').val($(this).children('option:selected').text()).valid();
    });

    $('#BannerForm').on('change', '#Restaurant_IdRestaurant', function (event) {
        LoadMenu(event);
        $('#IdMenuGroup').val($(this).children('option:selected').text()).valid();
    });

    $('#BannerForm').on('change', '.Restaurant_IdRestaurant', function (event) {
        var $MenuGroup = $(this).parent().parent().find(".IdMenuGroup");
        var value = $(this).val();
        LoadMenuParent($MenuGroup, value);
        $(this).parent().parent().find(".IdMenuGroup").val($(this).children('option:selected').text()).valid();
    });

    $('#SellingOutGroupForm').on('change', '#Restaurant_IdRestaurant', function (event) {
        LoadMenu(event);
        $('#IdMenuGroup').val($(this).children('option:selected').text()).valid();
        $("#IdMenuDish").empty();
    });

    $('#SellingOutGroupSearchForm').on('change', '#Restaurant_IdRestaurant', function (event) {
        LoadMenu(event);
        $('#IdMenuGroup').val($(this).children('option:selected').text()).valid();
    });

    $('#SalesGiftCodeForm').on('change', '#Restaurant_IdRestaurant', function (event) {
    	LoadMenu(event);
    	$('#IdMenuGroup').val($(this).children('option:selected').text()).valid();
    	$("#IdMenuDish").empty();
    });

    $('#SalesGiftCodeSearchForm').on('change', '#Restaurant_IdRestaurant', function (event) {
    	LoadMenu(event);
    	$('#IdMenuGroup').val($(this).children('option:selected').text()).valid();
    });

    $("#productsCode").on("click", ".add-pc", function (e) {
    	e.preventDefault();
    	var $dropDown = $(this).parent().find(".IdMenuDishProductCode");

    	if ($dropDown.val() == 0) {
    		showMessage("Selecione um produto.", "ATENÇÃO", "warning");
    		return;
    	}

    	var productOnList = false;
    	$(this).parent().parent().find("select").each(function (key, value) {
    		if ($dropDown.attr("name") != value.name) {
    			if ($(value).val() == $dropDown.val()) {
    				productOnList = true;
    				return;
    			}
    		}    		
    	});
		
    	if (productOnList) {
    		$dropDown.prop("selectedIndex", 0);
    		showMessage("O produto selecionado já existe na lista.", "ATENÇÃO", "warning");
    		return;
    	}

    	var $clone = $(this).parent().clone();
    	var y = $(this).parent().parent().get(0).childElementCount - 1;

    	$.each($clone.find("select"), function (key, value) {
    		$(value).prop('selectedIndex', 0);
    		$(value).data("selected", "");
    		$(value).data("idproductcode", "")
    		$(value).attr('id', 'MenuDishProductCodeList_' + y + '__IdProductCode')
    				.attr('name', 'MenuDishProductCodeList[' + y + '].IdProductCode');
    	});

    	$.each($clone.find("input"), function (key, value) {
    		if (value.name == 'MenuDishProductCodeList[' + (y - 1) + '].IdMenuDish') {
    			$(value).attr('id', 'MenuDishProductCodeList_' + y + '__IdMenuDish')
    					.attr('name', 'MenuDishProductCodeList[' + y + '].IdMenuDish');
    		}
    	});

    	$clone.css("margin-left", 160);
    	$clone.css("margin-top", 5);
    	$clone.find(".custom-combobox").remove();
    	$clone.find(".IdMenuDishProductCode").combobox();

    	$(this).parent().parent().append($clone);
    	$(this).next().show();
    	$(this).remove();
    });

    $("#productsCode").on("click", ".rem-pc", function (e) {
    	e.preventDefault();

    	if ($(this).parent().find(".add-pc").length == 1) {
    		return;
    	}

    	$(this).parent().remove();

    	var y = 0;
    	$("#productsCode").find("div").each(function (key, value) {
    		$(value).find("select").each(function (key2, value2) {
    			$(value2).attr('id', 'MenuDishProductCodeList_' + y + '__IdProductCode')
						 .attr('name', 'MenuDishProductCodeList[' + y + '].IdProductCode');
    		});

    		$(value).find("input").each(function (key2, value2) {
    			if (value2.name == 'MenuDishProductCodeList[' + (y + 1) + '].IdMenuDish') {
    				$(value2).attr('id', 'MenuDishProductCodeList_' + y + '__IdMenuDish')
						 .attr('name', 'MenuDishProductCodeList[' + y + '].IdMenuDish');
    			}
    		});
    		y++;
    	});
    });
});

function LoadMenu(event, callback) {    
    $('#IdMenuGroup').empty();
    if (!$('#Restaurant_IdRestaurant').val()) {
        return;
    }
    $('#IdMenuGroup').siblings('.loading').removeClass('hidden');
    $.get($("#applicationPath").val() + '/MenuGroup/GetMenuGroup', { RestaurantId: $('#Restaurant_IdRestaurant').val() }, function (result) {
        $('#IdMenuGroup').append($('<option>').val('').text(''));
        $.each(result, function (key, value) {
            if (value.Description==null){
                $('#IdMenuGroup').append($('<option>').val(value.IdMenuGroup).text(value.Name));
            }
            else {
                $('#IdMenuGroup').append($('<option>').val(value.IdMenuGroup).text(value.Name + " (" + value.Description +")"));
            }

        });
        $('#IdMenuGroup').siblings('.loading').addClass('hidden');
        if (callback) {
            callback();
        }
        setSelectedMenu();
    });
}

function isInt(value) {
    return !isNaN(value) && (function (x) { return (x | 0) === x; })(parseInt(value))
}

function loadProductCode() {

	$('.IdMenuDishProductCode').empty();
	$('.IdMenuDishProductCode').siblings('.loading').removeClass('hidden');

	var filter = [];
	$('.IdMenuDishProductCode').each(function () {
	    var selected = $(this).attr("data-selected");
	    if (selected != null && selected.trim() != "" && isInt(selected)) {
	        filter.push(parseInt(selected))
	    }
	});

	$.ajax({
	    url: $("#applicationPath").val() + '/ProductCode/GetList',
	    type: 'POST',
	    async: true,
	    data: JSON.stringify(filter),
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function (result) {
	        $('.IdMenuDishProductCode').append($('<option>').val('').text(''));
	        $.each(result, function (key, value) {
	            $(".IdMenuDishProductCode").each(function (key1, value1) {
	                data = $(value1).data("idproductcode");
	                if (data !== value.IdProductCode) {
	                    $(this).append($('<option>').val(value.IdProductCode).text(value.Description));
	                }
	            });
	        });

	        $(".IdMenuDishProductCode").each(function (key1, value1) {
	            data = $(value1).data("selected");
	            $(value1).find("option").each(function (key, value) {
	                var v = +value.value;
	                if (v === data) {
	                    $(value).attr("selected", true);
	                }
	            });
	        });

	        $(".IdMenuDishProductCode").combobox();
	        $('.IdMenuDishProductCode').siblings('.loading').addClass('hidden');
	    }
	});

	//$.get($("#applicationPath").val() + '/ProductCode/GetList', function (result) {
	//	$('.IdMenuDishProductCode').append($('<option>').val('').text(''));
	//	$.each(result, function (key, value) {
	//		$(".IdMenuDishProductCode").each(function (key1, value1) {
	//			data = $(value1).data("idproductcode");
	//			if (data !== value.IdProductCode) {
	//				$(this).append($('<option>').val(value.IdProductCode).text(value.Description));
	//			}
	//		});
	//	});

	//	$(".IdMenuDishProductCode").each(function (key1, value1) {
	//		data = $(value1).data("selected");
	//		$(value1).find("option").each(function (key, value) {
	//			var v = +value.value;
	//			if (v === data) {
	//				$(value).attr("selected", true);
	//			}
	//		});
	//	});

	//	$(".IdMenuDishProductCode").combobox();
	//	$('.IdMenuDishProductCode').siblings('.loading').addClass('hidden');
    //});
}

function setSelectedMenu() {    
    data = $("#IdMenuGroup").data("selected");
    $("#IdMenuGroup").find("option").each(function (key, value) {
    	var v = +value.value;
    	if (v === data) {
    		$(value).attr("selected", true)
    	}
    });
}


function setSelectedLoja() {    
    data = $("#Restaurant_IdRestaurant").data("selected");
    $("#Restaurant_IdRestaurant").find("option").each(function (key, value) {
    	var v = +value.value;
    	if (v === data) {
    		$(value).attr("selected", true)
    	}
    });
}

function setSelectedTipo() {
    data = $("#IdMenuAttributeType").data("selected");
    $("#IdMenuAttributeType").find("option").each(function (key, value) {
    	var v = +value.value;
    	if (v === data) {
    		$(value).attr("selected", true)
    	}
    });
}

function LoadMenuParent($MenuGroup, value, event, callback) {
    $MenuGroup.empty();
    $MenuGroup.siblings('.loading').removeClass('hidden');
    $.get($("#applicationPath").val() + '/MenuGroup/GetMenuGroup', { RestaurantId: value }, function (result) {
        $MenuGroup.append($('<option>').val('').text(''));
        $.each(result, function (key, value) {
            if (value.Description == null) {
                $MenuGroup.append($('<option>').val(value.IdMenuGroup).text(value.Name));
            }
            else {
                $MenuGroup.append($('<option>').val(value.IdMenuGroup).text(value.Name + " (" + value.Description + ")"));
            }
        });
        $MenuGroup.siblings('.loading').addClass('hidden');
        if (callback) {
            callback();
        }
        setSelectedMenuClass();
    });
}

function LoadMenuParentClass(event, callback) {
    $(".Restaurant_IdRestaurant").each(function () {
        var $MenuGroup = $(this).parent().parent().find(".IdMenuGroup");
        var val = $(this).val();
        $MenuGroup.empty();
        if (!val) {
            return;
        }
        $MenuGroup.siblings('.loading').removeClass('hidden');
        $.get($("#applicationPath").val() + '/MenuGroup/GetMenuGroup', { RestaurantId: val }, function (result) {
            $MenuGroup.append($('<option>').val('').text(''));
            $.each(result, function (key, value) {
                if (value.Description == null) {
                    $MenuGroup.append($('<option>').val(value.IdMenuGroup).text(value.Name));
                }
                else {
                    $MenuGroup.append($('<option>').val(value.IdMenuGroup).text(value.Name + " (" + value.Description + ")"));
                }
            });
            $MenuGroup.siblings('.loading').addClass('hidden');
            if (callback) {
                callback();
            }
            setSelectedMenuClass();
        });
    });
}

function setSelectedMenuClass() {
    $(".IdMenuGroup").each(function () {
        data = $(this).data("selected");
        $(this).find("option").each(function (key, value) {
        	var v = +value.value;
        	if (v === data) {
        		$(value).attr("selected", true)
        	}
        });
    });
}

function setSelectedLojaClass() {
    $(".Restaurant_IdRestaurant").each(function () {
        data = $(this).data("selected");
        $(this).find("option").each(function (key, value) {
        	var v = +value.value;
        	if (v === data) {
        		$(value).attr("selected", true)
        	}
        });
    });
}