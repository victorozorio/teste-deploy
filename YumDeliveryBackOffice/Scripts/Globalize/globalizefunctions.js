﻿//PARA ALTERAR A CULTURA DA PÁGINA, MODIFICAR NO ARQUIVO ALLFILES AS SEGUINTES LINHAS DE COMANDO.

//    Globalize.culture("@ViewBag.CultureName");
//    personalizeCulturePatterns();
//    $.validator.methods.number = function (value, element) {
//        if (value == "" || value == 0 || !isNaN(parseFloat(value, Globalize.culture()))) {
//            return true;
//        }
//        return false;
//    }
//    $.validator.methods.date = function (value, element) {
//        if (value == "" || parseDateByFormat(value) != null) {
//            return true;
//        }
//        return false;
//    }


//    function personalizeCulturePatterns() {
//        personalizeDateTimePatterns("d", "@ViewBag.Culture.DateTimeFormat.ShortDatePattern");
//        personalizeDateTimePatterns("t", "@ViewBag.Culture.DateTimeFormat.ShortTimePattern");
//        personalizeDateTimePatterns("D", "@ViewBag.Culture.DateTimeFormat.LongDatePattern");
//        personalizeDateTimePatterns("T", "@ViewBag.Culture.DateTimeFormat.LongTimePattern");
//        personalizeDateTimePatterns("F", "@ViewBag.Culture.DateTimeFormat.FullDateTimePattern");
//        personalizeDateTimePatterns("M", "@ViewBag.Culture.DateTimeFormat.MonthDayPattern");
//        personalizeDateTimePatterns("Y", "@ViewBag.Culture.DateTimeFormat.YearMonthPattern");
//    }


function parseDateByFormat(value) {
    if (value == null || value.length < 6)
        return null;
    var culture = Globalize.culture();
    var format;
    if (value.length > 10)
        format = culture.calendar.patterns.d + " " + culture.calendar.patterns.T;
    else
        format = culture.calendar.patterns.d;
    return Globalize.parseDate(value, format, culture);
}

function formatDateToDisplay(value) {
    var culture = Globalize.culture();
    return Globalize.format(parseDateByFormat(value), culture.calendar.patterns.d, culture);
}